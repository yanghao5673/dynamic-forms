# dynamicForms

### 演示地址：http://dynamicformsdemo.shiningon.top/admin

演示地址为本项目的Demo服务，通过rest接口进行实际应用的界面化演示和部分对接接口的演示。

dynamicFormsDemo 项目地址：https://gitee.com/yanghao5673/dynamicFormsDemo

#### 介绍

简化审核表单的创建和维护功能。

用于小型规模的自定义表单创建，审核回复流程追溯和服务。

![](./doc/overview.jpg)

#### 软件架构

| 组件名称              | 功能      | 版本 | 组件名称 | 功能 | 版本 |
|-------------------|---------|----|----|----|----|
| SpringBoot        |         | 2.7.1 | alibaba druid | 数据库连接池 |  |
| thymeleaf         | 视图框架    |    | mybatis | ORM框架 |    |
| jQueryEsayUI      | 前端渲染    |    | caffeine | 内存缓存 |    |
| logBack           | 日志框架    |    | knife4j | 接口文档 |    |
| lombok            |         |    | hutool |  |    |
| Apache httpClient | Http连接池 |    | hutool |  |    |

#### 安装教程

环境依赖：JDK1.8、MySql5.7+、推荐1G以上内存

1、下载项目到本地，以Maven项目引入开发工具（以IntelliJ IDEA（2022.2.1）为例）

2、更新Maven依赖

3、本地调试默认使用dev环境，检查resources/application-dev.yml文件，检查相关组件的链接地址。（配置中的内容请进行调整，避免演示服务崩溃）

4、Maven打包脚本，指定test环境，或自行新增对应环境的配置文件进行指定

<code>
clean package -DskipTests -Ptest
</code>

3、启动项目。默认端口8100

#### 模块列表：

- 单据查询

  	单据列表

- 单据流程配置

  	单据流程配置

- 单据配置

  	单据配置列表

#### 参与贡献

目前作者自行根据认知进行维护。

请愿意参与后续发展的码农邮件联系：yanghao5673@163.com，附上项目名：dynamicForms

