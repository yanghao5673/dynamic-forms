FROM java:8
## 设置所属时区
ENV TZ=Asia/Shanghai
## 创建本地和容器的连接
RUN ln -snf /usr/share/zoneinfo/$TZ /etc/localtime && echo $TZ > /etc/timezone
VOLUME /home/docker/dynamicForms/
ENTRYPOINT ["java", "-Xms1g", "-Xmx1g", "-XX:+HeapDumpOnOutOfMemoryError", "-XX:HeapDumpPath=/project/javalog/jvm.dump", "-XX:+PrintGCDetails", "-XX:+PrintGCDateStamps", "-XX:+UseGCLogFileRotation", "-XX:+PrintHeapAtGC", "-XX:NumberOfGCLogFiles=5", "-XX:GCLogFileSize=50M", "-Xloggc:/project/javalog/emps-gc-%t.log", "-XX:+HeapDumpOnOutOfMemoryError", "-XX:HeapDumpPath=/project/javalog/emps-heap.dump", "-jar", "/project/dynamicForms-1.0-SNAPSHOT.jar", "--spring.profiles.active=test"]



