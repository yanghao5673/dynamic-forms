package top.shiningon.dynamic.forms.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.shiningon.dynamic.forms.entity.BillDetail;

@Mapper
public interface BillDetailDao extends BaseMapper<BillDetail> {

}