package top.shiningon.dynamic.forms.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.github.yulichang.base.MPJBaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.shiningon.dynamic.forms.entity.BillReplyReady;

@Mapper
public interface BillReplyReadyDao extends BaseMapper<BillReplyReady>, MPJBaseMapper<BillReplyReady> {

}