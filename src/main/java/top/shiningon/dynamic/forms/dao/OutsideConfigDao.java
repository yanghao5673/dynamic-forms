package top.shiningon.dynamic.forms.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import top.shiningon.dynamic.forms.entity.OutsideConfig;

@Mapper
public interface OutsideConfigDao extends BaseMapper<OutsideConfig> {
    
}