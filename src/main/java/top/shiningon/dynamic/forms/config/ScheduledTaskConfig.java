package top.shiningon.dynamic.forms.config;

import cn.hutool.core.util.StrUtil;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.Trigger;
import org.springframework.scheduling.config.TriggerTask;
import top.shiningon.dynamic.forms.job.CustomScheduledTask;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

@Log4j2
@Configuration
public class ScheduledTaskConfig implements DisposableBean {

    @Autowired
    private TaskScheduler taskScheduler;

    private final Map<String,CustomScheduledTask> scheduledTasks = new ConcurrentHashMap(16);

    public void addTriggerTask(String key,Runnable task, Trigger trigger) {
        if (task == null || trigger == null || StrUtil.isBlank(key)){
            log.error("任务key和任务线程以及触发器不能为空");
            return;
        }
        CustomScheduledTask customScheduledTask1 = scheduledTasks.get(key);
        if (customScheduledTask1 != null){
            log.error("{}---对应的任务已存在，请勿重复创建，如需重复创建，请先执行删除后在尝试新建任务",key);
            return;
        }
        // addTriggerTask(new TriggerTask(task, trigger));
        CustomScheduledTask customScheduledTask = scheduleTriggerTask(new TriggerTask(task, trigger));
        customScheduledTask.future = this.taskScheduler.schedule(task, trigger);
        scheduledTasks.put(key,customScheduledTask);
    }

    public void removeTriggerTask(String key) {
        if (StrUtil.isBlank(key)){
            log.error("key不能为空");
            return;
        }
        CustomScheduledTask scheduledTask = scheduledTasks.get(key);
        if (scheduledTask == null){
            log.error("{}对应的任务不存在，请勿重复删除",key);
        }else {
            scheduledTask.cancel();
            scheduledTasks.remove(key);
        }
    }

    private CustomScheduledTask scheduleTriggerTask(TriggerTask task) {
        return  new CustomScheduledTask(task);
    }

    @Override
    public void destroy() throws Exception {
        for (CustomScheduledTask task : this.scheduledTasks.values()) {
            task.cancel();
        }
    }

}
