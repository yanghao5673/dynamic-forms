package top.shiningon.dynamic.forms.config;

import lombok.extern.log4j.Log4j2;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.annotation.SchedulingConfigurer;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;
import org.springframework.util.ReflectionUtils;

@Log4j2
@Configuration
public class ScheduledTaskRegistrar implements SchedulingConfigurer {

    @Override
    public void configureTasks(org.springframework.scheduling.config.ScheduledTaskRegistrar taskRegistrar) {
        ThreadPoolTaskScheduler taskScheduler = (ThreadPoolTaskScheduler)taskScheduler();
    }

    @Bean
    public TaskScheduler taskScheduler(){
        ThreadPoolTaskScheduler threadPoolTaskScheduler = new ThreadPoolTaskScheduler();
        threadPoolTaskScheduler.initialize();
        threadPoolTaskScheduler.setPoolSize(2);
        threadPoolTaskScheduler.setThreadNamePrefix("sched-pool-");
        threadPoolTaskScheduler.setErrorHandler((e) -> {
            log.error("定时任务执行异常", e);
            ReflectionUtils.rethrowRuntimeException(e);
        });
        return threadPoolTaskScheduler;
    }

}
