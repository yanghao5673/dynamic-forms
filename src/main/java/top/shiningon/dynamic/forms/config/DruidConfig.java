package top.shiningon.dynamic.forms.config;

import com.alibaba.druid.pool.DruidDataSource;
import com.alibaba.druid.support.http.StatViewServlet;
import com.alibaba.druid.support.http.WebStatFilter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import javax.sql.DataSource;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

/**
 * druid 连接池配置
 */
@Configuration
public class DruidConfig {

    @ConfigurationProperties(prefix = "spring.datasource.druid")
    @Bean
    public DataSource druid() {
        return new DruidDataSource();
    }

    @Value("${spring.datasource.druid.stat-view-servlet.enabled}")
    private Boolean enabled;
    @Value("${spring.datasource.druid.stat-view-servlet.url-mappings}")
    private String urlMappings;
    @Value("${spring.datasource.druid.stat-view-servlet.reset-enable}")
    private String resetEnable;
    @Value("${spring.datasource.druid.stat-view-servlet.login-username}")
    private String loginUsername;
    @Value("${spring.datasource.druid.stat-view-servlet.login-password}")
    private String loginPassword;

    //内置 Servlet 容器时没有web.xml文件，所以使用 Spring Boot 的注册 Servlet 方式
    @Bean
    public ServletRegistrationBean statViewServlet(){
        ServletRegistrationBean bean = new ServletRegistrationBean(new StatViewServlet(), urlMappings);
        // 这些参数可以在 com.alibaba.druid.support.http.StatViewServlet 的父类 com.alibaba.druid.support.http.ResourceServlet 中找到
        Map<String,String> initParams = new HashMap<>();
        initParams.put("loginUsername",loginUsername);
        initParams.put("loginPassword",loginPassword);
//        initParams.put("allow",""); //默认就是允许所有访问
        //deny：Druid 后台拒绝谁访问，表示禁止此ip访问
        // initParams.put("deny","192.168.10.132");
        initParams.put("resetEnable",resetEnable); //默认就是允许所有访问
        bean.setInitParameters(initParams);
        return bean;
    }

    @Value("${spring.datasource.druid.web-stat-filter.url-pattern}")
    private String urlPattern;
    @Value("${spring.datasource.druid.web-stat-filter.exclusions}")
    private String exclusions;

    //2、配置一个web监控的filter
    @Bean
    public FilterRegistrationBean webStatFilter(){
        FilterRegistrationBean bean = new FilterRegistrationBean();
        if(enabled) {
            bean.setFilter(new WebStatFilter());
            Map<String, String> initParams = new HashMap<>();
            initParams.put("exclusions", exclusions);
            bean.setInitParameters(initParams);
            bean.setUrlPatterns(Arrays.asList(urlPattern));
        }
        return  bean;
    }
}