package top.shiningon.dynamic.forms.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import top.shiningon.dynamic.forms.rest.interceptor.AdminContextInterceptor;
import top.shiningon.dynamic.forms.rest.interceptor.GlobalContextInterceptor;

/**
 * spring MVC的全局拦截器
 * @author YH
 *
 */
@Configuration
public class InterceptorConfiguration implements WebMvcConfigurer {

	@Autowired
	private GlobalContextInterceptor globalContextInterceptor;
	@Autowired
	private AdminContextInterceptor adminContextInterceptor;

	@Override
	public void addResourceHandlers(ResourceHandlerRegistry registry) {
		registry.addResourceHandler("favicon.ico").addResourceLocations("classpath:/static/favicon.ico");
		registry.addResourceHandler("/static/**").addResourceLocations("classpath:/static/");
		registry.addResourceHandler("doc.html").addResourceLocations("classpath:/META-INF/resources/");
		registry.addResourceHandler("/webjars/**").addResourceLocations("classpath:/META-INF/resources/webjars/");
	}

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		registry.addInterceptor(globalContextInterceptor)
				.addPathPatterns("/process/**", "/config/**");

		registry.addInterceptor(adminContextInterceptor)
				.addPathPatterns("/admin/**")
				.excludePathPatterns("/admin", "/admin/index", "/admin/login");
    }

}
