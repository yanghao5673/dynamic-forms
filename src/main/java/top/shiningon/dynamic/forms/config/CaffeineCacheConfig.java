package top.shiningon.dynamic.forms.config;

import com.github.benmanes.caffeine.cache.Cache;
import com.github.benmanes.caffeine.cache.Caffeine;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.util.concurrent.TimeUnit;

@Configuration
public class CaffeineCacheConfig {

    /**
     * 默认缓存配置,key为string，value为object，如果value想自定义为目标对象,可新建一个@Bean
     */
    @Bean(name = "caffeineCache10M")
    public Cache<String, Object> caffeineCache10M() {
        return Caffeine.newBuilder()
                // 设置最后一次写入或访问后经过固定时间过期
                .expireAfterWrite(10, TimeUnit.MINUTES)//10分钟
                // 初始的缓存空间大小
                .initialCapacity(50)
                // 缓存的最大条数
                .maximumSize(500)
                .build();
    }

    @Bean(name = "caffeineCache1H")
    public Cache<String, Object> caffeineCache1H() {
        return Caffeine.newBuilder()
                // 设置最后一次写入或访问后经过固定时间过期
                .expireAfterWrite(1, TimeUnit.HOURS)//10分钟
                // 初始的缓存空间大小
                .initialCapacity(50)
                // 缓存的最大条数
                .maximumSize(500)
                .build();
    }

}
