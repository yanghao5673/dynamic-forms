package top.shiningon.dynamic.forms.job;

import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.support.CronTrigger;
import top.shiningon.dynamic.forms.config.ScheduledTaskConfig;
import top.shiningon.dynamic.forms.constant.MemberSyncModeEnum;
import top.shiningon.dynamic.forms.entity.OutsideConfig;
import top.shiningon.dynamic.forms.service.OutsideConfigService;

import javax.annotation.PostConstruct;

@Log4j2
@Configuration
public class MemberSyncTask implements Runnable{

    public static int SYNC_MODE = 0;//0=不同步 1=三方推送同步 2=主动访问三方同步

    public static boolean SYNC_STATUS = false;

    public static final String MEMBER_SYNC_TASK_NAME = "memberSyncTask";

    @Autowired
    OutsideConfigService outsideConfigService;

    @Autowired
    ScheduledTaskConfig scheduledTaskConfig;

    @PostConstruct
    private void startSyncTask(){
        OutsideConfig config = outsideConfigService.getConfig();
        if(null != config){
            SYNC_MODE = MemberSyncModeEnum.不同步.getKey();
            if(MemberSyncModeEnum.不同步.equals(config.getMemberSyncMode())){
                log.info("员工信息关闭同步");
            }
            if(MemberSyncModeEnum.被动同步.equals(config.getMemberSyncMode())){
                log.info("开启员工信息【被动】同步");
            }
            if(MemberSyncModeEnum.主动同步_暂不支持.equals(config.getMemberSyncMode())){
                log.info("开启员工信息【主动】同步");
                scheduledTaskConfig.addTriggerTask(MEMBER_SYNC_TASK_NAME,
                        this::run,
                        triggerContext -> new CronTrigger( this.outsideConfigService.getMemberTaskCron() ).nextExecutionTime(triggerContext)
                );
            }
        }
    }

    @Override
    public void run() {
        if(SYNC_STATUS){
            //已经在执行，直接结束。避免多线程阻塞
            return;
        }
        SYNC_STATUS = true;
        log.info("key:{},员工信息主动同步定时任务暂不支持，请关闭。", "MemberSyncTask");

        OutsideConfig config = outsideConfigService.getConfig();
        //同步模式不同，判断情况
        if(config.getMemberSyncMode().getKey() != SYNC_MODE){
            SYNC_MODE = config.getMemberSyncMode().getKey();
            if(!MemberSyncModeEnum.主动同步_暂不支持.equals(config.getMemberSyncMode())) {
                //关闭任务
                scheduledTaskConfig.removeTriggerTask(MEMBER_SYNC_TASK_NAME);
            }
        }
        SYNC_STATUS = false;
    }

}
