package top.shiningon.dynamic.forms.job;

import org.springframework.scheduling.config.Task;

import javax.annotation.Nullable;
import java.util.concurrent.ScheduledFuture;

public class CustomScheduledTask {

    private final Task task;

    @Nullable
    public volatile ScheduledFuture<?> future;


    public CustomScheduledTask(Task task) {
        this.task = task;
    }


    public Task getTask() {
        return this.task;
    }

    public void cancel() {
        cancel(true);
    }

    public void cancel(boolean mayInterruptIfRunning) {
        ScheduledFuture<?> future = this.future;
        if (future != null) {
            future.cancel(mayInterruptIfRunning);
        }
    }

    @Override
    public String toString() {
        return this.task.toString();
    }

}
