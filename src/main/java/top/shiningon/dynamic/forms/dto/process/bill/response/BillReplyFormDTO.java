package top.shiningon.dynamic.forms.dto.process.bill.response;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.BillReplyStatusEnum;
import top.shiningon.dynamic.forms.entity.Member;

import java.util.Date;

@Data
public class BillReplyFormDTO {

    private Long id;

    private Long billListId;

    private String billTitle;

    private String billSubTitle;

    private Long flowStepId;

    private String flowStepName;

    private BillReplyStatusEnum replyStatus;

    private Integer replyStatusCode;

    @TableField("`index`")
    private Integer index;

    private String remark;

    private String createBy;

    private Member createMember;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String updateBy;

    private Member updateMember;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    public Integer getReplyStatus(){
        if(null != replyStatus){
            return replyStatus.getKey();
        }
        return null;
    }

}
