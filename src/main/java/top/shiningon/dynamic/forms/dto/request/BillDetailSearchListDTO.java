package top.shiningon.dynamic.forms.dto.request;

import lombok.Data;

import java.util.List;

@Data
public class BillDetailSearchListDTO {

    private List<Long> billListIdList;

    private Boolean delflag = false;

}
