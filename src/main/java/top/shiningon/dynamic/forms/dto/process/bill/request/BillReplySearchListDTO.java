package top.shiningon.dynamic.forms.dto.process.bill.request;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.BillReplyStatusEnum;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;

@Data
public class BillReplySearchListDTO extends PageFormDTO {

    private Long billListId;

    private Long flowStepId;

    private BillReplyStatusEnum replyStatus;

    private String createBy;

    private Boolean delflag = false;

}
