package top.shiningon.dynamic.forms.dto.admin;

import lombok.Data;
import top.shiningon.dynamic.forms.dto.admin.detail.BillDetailBillListDTO;
import top.shiningon.dynamic.forms.dto.admin.detail.BillDetailDetailDTO;
import top.shiningon.dynamic.forms.dto.admin.detail.BillDetailReplyDTO;
import top.shiningon.dynamic.forms.dto.admin.detail.BillDetailReplyReadyDTO;

import java.util.List;

@Data
public class BillDetailDTO {

    private BillDetailBillListDTO bill;

    private List<BillDetailDetailDTO> formInputList;

    private List<BillDetailReplyDTO> replyList;

    private List<BillDetailReplyReadyDTO> replyReadyList;

}
