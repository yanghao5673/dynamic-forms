package top.shiningon.dynamic.forms.dto.admin.detail;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.BillStatusEnum;

import java.util.Date;

@Data
public class BillDetailBillListDTO {

    private Long id;

    private String title;

    private String subTitle;

    private String nowFlowStepName;

    private BillStatusEnum status;

    private String createBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
