package top.shiningon.dynamic.forms.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import top.shiningon.dynamic.forms.constant.BillStatusEnum;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;

import java.util.Date;
import java.util.List;

@Data
public class BillListSearchListDTO extends PageFormDTO {

    private List<Long> billConfigIdList;

    private String title;

    private BillStatusEnum status;

    private String createBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginCreateTime;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endCreateTime;

    private String updateBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date beginUpdateTime;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd")
    private Date endUpdateTime;

    private Boolean delflag = false;

}
