package top.shiningon.dynamic.forms.dto.config;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import top.shiningon.dynamic.forms.constant.InputTypeEnum;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class BillFormFieldConfigFormDTO {

    private Long id;

    @NotNull
    private Long billConfigId;

    @NotNull
    private String inputName;

    @NotNull
    private InputTypeEnum inputType;

    @NotNull
    private Integer vaildType;

    @NotNull
    private Integer index;

    private String vaildMax;

    private String vaildMin;

    @NotNull
    private Boolean isRequired;

    private String placeholder;

    private String defVal;

    @NotNull
    private String operBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date operTime;

    private Boolean delflag = false;

}
