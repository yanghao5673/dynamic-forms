package top.shiningon.dynamic.forms.dto.process.bill.response.detail;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.InputTypeEnum;

@Data
@ApiModel(value="单据详情对象-单据字段信息")
public class BillDetailDetailDTO {

    @ApiModelProperty(position=1, name = "id", required = true, notes="id", example = "0")
    private Long id;

    @ApiModelProperty(position=3, name = "index", notes="字段顺序", example = "1")
    private Integer index;

    @ApiModelProperty(position=3, name = "inputName", notes="字段名称", example = "姓名")
    private String inputName;

    @ApiModelProperty(position=3, name = "inputType", notes="字段输入类型", example = "文本框")
    private InputTypeEnum inputType;

    @ApiModelProperty(position=3, name = "inputVal", notes="字段值", example = "张三")
    private String inputVal;

}
