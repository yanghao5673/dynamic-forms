package top.shiningon.dynamic.forms.dto.process.bill.response;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowStepReplyTypeEnum;
import top.shiningon.dynamic.forms.constant.FlowStepReplyUserTypeEnum;
import top.shiningon.dynamic.forms.entity.FlowConfig;
import top.shiningon.dynamic.forms.entity.FlowStep;

@Data
public class DetailBillFormFlowStepDTO {

    private Long id;

    private Long flowConfigId;

    private String flowConfigName;

    private String name;

    private Integer index;

    private FlowStepReplyTypeEnum replyType;

    private String replyTypeDesc;

    private FlowStepReplyUserTypeEnum replyUserType;

    private String replyUserTypeDesc;

    private String userTypeIds;

    public void initBy(String flowConfigName, FlowStep flowStep){
        this.initPart(flowStep);
        this.flowConfigName = flowConfigName;
    }

    public void initBy(FlowConfig flowConfig, FlowStep flowStep){
        this.initPart(flowStep);
        this.flowConfigName = flowConfig.getName();
    }

    private void initPart(FlowStep flowStep){
        this.id = flowStep.getId();
        this.flowConfigId = flowStep.getFlowConfigId();
        this.name = flowStep.getName();
        this.index = flowStep.getIndex();
        this.replyType = flowStep.getReplyType();
        this.replyTypeDesc = flowStep.getReplyType().getVal();
        this.replyUserType = flowStep.getReplyUserType();
        this.replyUserTypeDesc = flowStep.getReplyUserType().getVal();
        this.userTypeIds = flowStep.getUserTypeIds();
    }

}
