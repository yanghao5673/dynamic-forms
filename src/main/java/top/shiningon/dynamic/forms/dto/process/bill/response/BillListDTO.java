package top.shiningon.dynamic.forms.dto.process.bill.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.BillStatusEnum;
import top.shiningon.dynamic.forms.entity.Member;

import java.util.Date;

@Data
@ApiModel(value="单据对象")
public class BillListDTO {

    @ApiModelProperty(position=1, name = "id", required = true, notes="id", example = "0")
    private Long id;

    @ApiModelProperty(position=2, name = "billConfigId", notes="单据配置id", example = "0")
    private Long billConfigId;

    @ApiModelProperty(position=3, name = "billConfigName", notes="单据配置名称", example = "请假单")
    private String billConfigName;

    @ApiModelProperty(position=4, name = "title", notes="单据标题", example = "0")
    private String title;

    @ApiModelProperty(position=5, name = "subTitle", notes="单据子标题", example = "0")
    private String subTitle;

    @ApiModelProperty(position=6, name = "nowFlowStepId", notes="当前流程步骤Id", example = "0")
    private Long nowFlowStepId;

    @ApiModelProperty(position=7, name = "nowFlowStepName", notes="当前流程步骤名称", example = "主管审核")
    private String nowFlowStepName;

    @ApiModelProperty(position=8, name = "status", notes="单据状态", example = "审批中")
    private BillStatusEnum status;

    @ApiModelProperty(position=9, name = "statusCode", notes="单据状态Code", example = "2")
    private Integer statusCode;

    @ApiModelProperty(position=10, name = "createBy", notes="子字段创建人", example = "用户A")
    private String createBy;

    @ApiModelProperty(position=11, name = "createBy", notes="单据创建人", example = "用户A")
    private Member createMember;

    @ApiModelProperty(position=12, name = "createTime", notes="子字段创建时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(position=13, name = "updateBy", notes="子字段更新人", example = "用户A")
    private String updateBy;

    @ApiModelProperty(position=14, name = "createBy", notes="单据更新人", example = "用户A")
    private Member updateMember;

    @ApiModelProperty(position=15, name = "updateTime", notes="子字段更新时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

}
