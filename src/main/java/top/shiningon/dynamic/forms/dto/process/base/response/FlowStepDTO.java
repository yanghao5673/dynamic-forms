package top.shiningon.dynamic.forms.dto.process.base.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowStepReplyTypeEnum;
import top.shiningon.dynamic.forms.constant.FlowStepReplyUserTypeEnum;
import top.shiningon.dynamic.forms.entity.Member;

import java.util.Date;

@Data
@ApiModel(value="流程步骤配置对象")
public class FlowStepDTO {

    @ApiModelProperty(position=1, name = "id", required = true, notes="id", example = "0")
    private Long id;

    @ApiModelProperty(position=2, name = "flowConfigId", notes="流程配置id", example = "0")
    private Long flowConfigId;

    @ApiModelProperty(position=3, name = "name", notes="流程步骤名称", example = "主管审核")
    private String name;

    @ApiModelProperty(position=4, name = "index", notes="流程步骤顺序", example = "1")
    private Integer index;

    @ApiModelProperty(position=5, name = "replyType", notes="流程步骤审核类型", example = "一票通过")
    private FlowStepReplyTypeEnum replyType;

    @ApiModelProperty(position=6, name = "replyUserType", notes="流程步骤审核人类型", example = "部门内直属上级")
    private FlowStepReplyUserTypeEnum replyUserType;

    @ApiModelProperty(position=7, name = "userTypeIds", notes="部分审核人类型需要指定的外部系统岗位角色id", example = "0")
    private String userTypeIds;

    @ApiModelProperty(position=8, name = "createBy", notes="子字段创建人", example = "用户A")
    private String createBy;

    @ApiModelProperty(position=9, name = "createBy", notes="单据创建人", example = "用户A")
    private Member createMember;

    @ApiModelProperty(position=10, name = "createTime", notes="子字段创建时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(position=11, name = "updateBy", notes="子字段更新人", example = "用户A")
    private String updateBy;

    @ApiModelProperty(position=12, name = "createBy", notes="单据更新人", example = "用户A")
    private Member updateMember;

    @ApiModelProperty(position=13, name = "updateTime", notes="子字段更新时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @ApiModelProperty(position=14, name = "delflag", notes="T or F", example = "F")
    private Boolean delflag;

}
