package top.shiningon.dynamic.forms.dto.callback.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.BillReplyStatusEnum;
import top.shiningon.dynamic.forms.dto.process.bill.response.detail.BillDetailDetailDTO;
import top.shiningon.dynamic.forms.entity.Member;

import java.util.Date;
import java.util.List;

@Data
public class ReplyNotifyFormDTO {

    private Long id;

    private String title;

    private String subTitle;

    private String billConfigCode;

    private List<BillDetailDetailDTO> formInputList;

    private BillReplyStatusEnum replyStatus;

    private Integer replyStatusCode;

    private Boolean billIsOver;

    private String createBy;

    private Member createMember;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String operBy;

    private Member operMember;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date operTime;

    public Integer getReplyStatusCode(){
        if(null != replyStatus){
            return replyStatus.getKey();
        }
        return null;
    }

}
