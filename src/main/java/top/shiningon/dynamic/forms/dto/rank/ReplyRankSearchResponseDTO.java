package top.shiningon.dynamic.forms.dto.rank;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowStepReplyUserTypeEnum;

import java.util.List;

@Data
public class ReplyRankSearchResponseDTO {

    private Long id;

    private String userId;

    private FlowStepReplyUserTypeEnum replyUserType;

    private List<String> replyUserId;

}
