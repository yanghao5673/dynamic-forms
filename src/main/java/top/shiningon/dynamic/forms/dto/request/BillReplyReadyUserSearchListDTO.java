package top.shiningon.dynamic.forms.dto.request;

import lombok.Data;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;

import java.util.List;

@Data
public class BillReplyReadyUserSearchListDTO extends PageFormDTO {

    private Long replyReadyId;

    private List<Long> replyReadyIdList;

    private Long userid;

    private Boolean delflag = false;

}
