package top.shiningon.dynamic.forms.dto.process.bill.request;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.BillReplyStatusEnum;

import javax.validation.constraints.NotNull;

@Data
public class SubmitReplyFormDTO {

    @NotNull
    private Long billListId;

    @NotNull
    private Long flowStepId;

    @NotNull
    private BillReplyStatusEnum replyStatus;

    private String remark;

    @NotNull
    private String operBy;

}
