package top.shiningon.dynamic.forms.dto.process;

public class PageFormDTO {

    private Integer pageNum;

    private Integer pageSize;

    public PageFormDTO(){

    }

    public PageFormDTO(Integer pageNum, Integer pageSize){
        this.pageNum = pageNum;
        this.pageSize = pageSize;
    }
    public boolean isPageSearch(){
        return null != pageNum && null != pageSize;
    }

    public Integer getPageNum(){
        if(null == pageNum || pageNum <= 0) {
            return 1;
        }
        return pageNum;
    }

    public void setPageNum(Integer pageNum) {
        this.pageNum = pageNum;
    }


    public Integer getPageSize() {
        return pageSize;
    }

    public void setPageSize(Integer pageSize) {
        this.pageSize = pageSize;
    }

}
