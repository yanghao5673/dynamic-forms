package top.shiningon.dynamic.forms.dto.process.bill.response;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.dto.process.bill.response.detail.BillDetailBillListDTO;
import top.shiningon.dynamic.forms.dto.process.bill.response.detail.BillDetailDetailDTO;
import top.shiningon.dynamic.forms.dto.process.bill.response.detail.BillDetailReplyDTO;
import top.shiningon.dynamic.forms.dto.process.bill.response.detail.BillDetailReplyReadyDTO;

import java.util.List;

/**
 * 用户查询表单详情时返回的对象
 */
@Data
@ApiModel(value="单据详情对象")
public class DetailBillFormDTO {

    @ApiModelProperty(position=1, name = "bill", notes="单据主体", example = "对象")
    private BillDetailBillListDTO bill;

    @ApiModelProperty(position=2, name = "formInputList", notes="单据字段信息", example = "列表")
    private List<BillDetailDetailDTO> formInputList;

    @ApiModelProperty(position=3, name = "replyList", notes="单据审核信息", example = "列表")
    private List<BillDetailReplyDTO> replyList;

    @ApiModelProperty(position=4, name = "replyReadyList", notes="单据审核流程暂存信息", example = "列表")
    private List<BillDetailReplyReadyDTO> replyReadyList;

}
