package top.shiningon.dynamic.forms.dto.process.base.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowConfigStatusEnum;
import top.shiningon.dynamic.forms.entity.Member;

import java.util.Date;
import java.util.List;

@Data
@ApiModel(value="流程配置对象")
public class FlowConfigDTO {

    @ApiModelProperty(position=1, name = "id", required = true, notes="id", example = "0")
    private Long id;

    @ApiModelProperty(position=2, name = "flowStepList", notes="流程步骤配置id", example = "0")
    private List<FlowStepDTO> flowStepList;

    @ApiModelProperty(position=3, name = "name", notes="流程名称", example = "请假单")
    private String name;

    @ApiModelProperty(position=4, name = "status", notes="流程是否可用", example = "可用")
    private FlowConfigStatusEnum status;

    @ApiModelProperty(position=5, name = "createBy", notes="子字段创建人", example = "用户A")
    private String createBy;

    @ApiModelProperty(position=6, name = "createBy", notes="单据创建人", example = "用户A")
    private Member createMember;

    @ApiModelProperty(position=7, name = "createTime", notes="子字段创建时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(position=8, name = "updateBy", notes="子字段更新人", example = "用户A")
    private String updateBy;

    @ApiModelProperty(position=9, name = "createBy", notes="单据更新人", example = "用户A")
    private Member updateMember;

    @ApiModelProperty(position=10, name = "updateTime", notes="子字段更新时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @ApiModelProperty(position=11, name = "delflag", notes="T or F", example = "F")
    private Boolean delflag;

}
