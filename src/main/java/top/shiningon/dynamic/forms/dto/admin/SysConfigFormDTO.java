package top.shiningon.dynamic.forms.dto.admin;

import lombok.Data;

@Data
public class SysConfigFormDTO {

    private Boolean loginAble;

    private String loginName;

    private String loginPassword;

}
