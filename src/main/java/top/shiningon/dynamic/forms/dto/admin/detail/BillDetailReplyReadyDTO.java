package top.shiningon.dynamic.forms.dto.admin.detail;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowStepReplyTypeEnum;

import java.util.List;

@Data
public class BillDetailReplyReadyDTO {

    private Long id;

    private String flowStepName;

    private FlowStepReplyTypeEnum replyType;

    private Integer replyTypeCode;

    private Integer index;

    private Boolean isNow;

    private List<String> userList;

    public Integer getreplyTypeCode(){
        if(null != replyType){
            return replyType.getKey();
        }
        return null;
    }

}
