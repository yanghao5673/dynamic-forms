package top.shiningon.dynamic.forms.dto.admin.base;

import lombok.Data;

import java.io.Serializable;

/**
 * @author yanghao
 * 2019年5月15日
 */
@Data
public class PageSupportParamDTO implements Serializable{

	private Integer page = 1;
	private Integer rows = 10;

	public PageSupportParamDTO() {

	}

	public PageSupportParamDTO(Integer page, Integer rows) {
		this.page = page;
		this.rows = rows;
	}

}
