package top.shiningon.dynamic.forms.dto.sync.request;

import lombok.Data;

@Data
public class MemberSyncDTO {

    private String id;

    private String name;

    private String position;

    private String department;

}
