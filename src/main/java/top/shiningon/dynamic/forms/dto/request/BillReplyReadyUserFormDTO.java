package top.shiningon.dynamic.forms.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class BillReplyReadyUserFormDTO {

    private Long id;

    @NotNull
    private Long replyReadyId;

    @NotNull
    private String userId;

    @NotNull
    private String operBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date operTime;

    private Boolean delflag = false;

}
