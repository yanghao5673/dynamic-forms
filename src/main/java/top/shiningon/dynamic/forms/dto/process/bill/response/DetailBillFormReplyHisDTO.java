package top.shiningon.dynamic.forms.dto.process.bill.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.BillReplyStatusEnum;
import top.shiningon.dynamic.forms.entity.Member;

import java.util.Date;

@Data
public class DetailBillFormReplyHisDTO {

    private Long id;

    private Long billListId;

    private Long flowConfigId;

    private Long flowStepId;

    private BillReplyStatusEnum replyStatus;

    private Integer replyStatusCode;

    private String replyStatusDesc;

    private Integer index;

    private String remark;

    private String createBy;

    private Member createMember;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    public Integer getReplyStatusCode(){
        if(null != replyStatus){
            return replyStatus.getKey();
        }
        return null;
    }

}
