package top.shiningon.dynamic.forms.dto.admin;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.MemberSyncModeEnum;

@Data
public class OutsideMemberSyncFormDTO {

    private MemberSyncModeEnum memberSyncMode;

    private String memberSyncCron;

    private String memberSyncUrl;

}
