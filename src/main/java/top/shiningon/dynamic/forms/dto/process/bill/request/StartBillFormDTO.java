package top.shiningon.dynamic.forms.dto.process.bill.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

/**
 * 用户发起单据，进入流转
 */
@Data
public class StartBillFormDTO {

    @NotNull
    private Long billId;

    @NotNull
    private String operBy;

}
