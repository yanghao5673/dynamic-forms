package top.shiningon.dynamic.forms.dto.admin;

import lombok.Data;
import top.shiningon.dynamic.forms.dto.admin.base.PageSupportParamDTO;

@Data
public class BillConfigSearchDTO extends PageSupportParamDTO {

    private Long flowConfigId;

    private String code;

    private String title;

}
