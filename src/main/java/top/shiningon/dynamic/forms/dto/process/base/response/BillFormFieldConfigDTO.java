package top.shiningon.dynamic.forms.dto.process.base.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.InputTypeEnum;
import top.shiningon.dynamic.forms.entity.Member;

import java.util.Date;
import java.util.List;

@Data
@ApiModel(value="单据字段对象")
public class BillFormFieldConfigDTO {

    @ApiModelProperty(position=1, name = "id", required = true, notes="id", example = "0")
    private Long id;

    @ApiModelProperty(position=2, name = "billConfigId", notes="单据配置id", example = "0")
    private Long billConfigId;

    @ApiModelProperty(position=3, name = "fieldSubConfigList", notes="子字段配置id", example = "0")
    private List<BillFormFieldSubConfigDTO> fieldSubConfigList;

    @ApiModelProperty(position=4, name = "inputName", notes="字段名称", example = "姓名")
    private String inputName;

    @ApiModelProperty(position=5, name = "inputType", notes="字段输入类型", example = "文本框")
    private InputTypeEnum inputType;

    @ApiModelProperty(position=6, name = "vaildType", notes="校验类型（暂无用）", example = "0")
    private Integer vaildType;

    @ApiModelProperty(position=7, name = "index", notes="字段顺序", example = "1")
    private Integer index;

    @ApiModelProperty(position=8, name = "vaildMax", notes="字段最大值（文本和日期表示范围）", example = "0")
    private String vaildMax;

    @ApiModelProperty(position=9, name = "vaildMin", notes="字段最小值（文本和日期表示范围）", example = "1")
    private String vaildMin;

    @ApiModelProperty(position=10, name = "isRequired", notes="是否必填项", example = "F")
    private Boolean isRequired;

    @ApiModelProperty(position=11, name = "placeholder", notes="默认提示信息", example = "请输入")
    private String placeholder;

    @ApiModelProperty(position=12, name = "defVal", notes="默认值", example = "张三")
    private String defVal;

    @ApiModelProperty(position=13, name = "createBy", notes="字段创建人", example = "用户A")
    private String createBy;

    @ApiModelProperty(position=14, name = "createBy", notes="单据创建人", example = "用户A")
    private Member createMember;

    @ApiModelProperty(position=15, name = "createTime", notes="字段创建时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(position=16, name = "updateBy", notes="字段更新人", example = "用户A")
    private String updateBy;

    @ApiModelProperty(position=17, name = "createBy", notes="单据更新人", example = "用户A")
    private Member updateMember;

    @ApiModelProperty(position=18, name = "updateTime", notes="字段更新时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @ApiModelProperty(position=19, name = "delflag", notes="T or F", example = "F")
    private Boolean delflag;

}
