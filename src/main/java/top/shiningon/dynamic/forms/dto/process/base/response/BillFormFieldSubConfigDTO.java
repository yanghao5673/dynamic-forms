package top.shiningon.dynamic.forms.dto.process.base.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.entity.Member;

import java.util.Date;

@Data
@ApiModel(value="单据子字段对象")
public class BillFormFieldSubConfigDTO {

    @ApiModelProperty(position=1, name = "id", required = true, notes="id", example = "0")
    private Long id;

    @ApiModelProperty(position=2, name = "formFieldId", notes="表单字段配置id", example = "0")
    private Long formFieldId;

    @ApiModelProperty(position=3, name = "inputVal", notes="子字段值", example = "事假")
    private String inputVal;

    @ApiModelProperty(position=4, name = "index", notes="子字段顺序", example = "1")
    private Integer index;

    @ApiModelProperty(position=5, name = "defSelect", notes="是否默认选中", example = "F")
    private Boolean defSelect;

    @ApiModelProperty(position=6, name = "createBy", notes="子字段创建人", example = "用户A")
    private String createBy;

    @ApiModelProperty(position=7, name = "createBy", notes="单据创建人", example = "用户A")
    private Member createMember;

    @ApiModelProperty(position=8, name = "createTime", notes="子字段创建时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(position=9, name = "updateBy", notes="子字段更新人", example = "用户A")
    private String updateBy;

    @ApiModelProperty(position=10, name = "createBy", notes="单据更新人", example = "用户A")
    private Member updateMember;

    @ApiModelProperty(position=11, name = "updateTime", notes="子字段更新时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @ApiModelProperty(position=12, name = "delflag", notes="T or F", example = "F")
    private Boolean delflag;

}
