package top.shiningon.dynamic.forms.dto.admin;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowConfigStatusEnum;

@Data
public class FlowConfigFormDTO {

    private Long id;

    private String name;

    private FlowConfigStatusEnum status;

}
