package top.shiningon.dynamic.forms.dto.admin;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowStepReplyTypeEnum;
import top.shiningon.dynamic.forms.constant.FlowStepReplyUserTypeEnum;

@Data
public class FlowStepDTO {

    private Long id;

    private Long flowConfigId;

    private String flowConfigName;

    private String name;

    private Integer index;

    private FlowStepReplyTypeEnum replyType;

    private FlowStepReplyUserTypeEnum replyUserType;

    private String userTypeIds;

    public Integer getReplyTypeCode(){
        if(null != replyType) {
            this.replyType.getKey();
        }
        return null;
    }

    public Integer getReplyUserTypeCode(){
        if(null != replyUserType) {
            this.replyUserType.getKey();
        }
        return null;
    }

}
