package top.shiningon.dynamic.forms.dto.process.base.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.MemberSyncModeEnum;

import java.util.Date;

@Data
public class OutsideConfigDTO {

    private Long id;

    private String urlExpression;

    private MemberSyncModeEnum memberSyncMode;

    private String memberSyncCron;

    private String memberSyncUrl;

    private String createBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String updateBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    private Boolean delflag;

}
