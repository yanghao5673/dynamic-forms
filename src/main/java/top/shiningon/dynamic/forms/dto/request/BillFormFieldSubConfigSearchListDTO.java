package top.shiningon.dynamic.forms.dto.request;

import lombok.Data;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;

import java.util.List;

@Data
public class BillFormFieldSubConfigSearchListDTO extends PageFormDTO {

    private List<Long> formFieldIdList;

    private Boolean delflag = false;

}
