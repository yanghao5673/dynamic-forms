package top.shiningon.dynamic.forms.dto.admin;

import lombok.Data;

@Data
public class BillConfigDTO {

    private Long id;

    private Long flowConfigId;

    private String flowConfigName;

    private String code;

    private String title;

    private String subTitle;

}
