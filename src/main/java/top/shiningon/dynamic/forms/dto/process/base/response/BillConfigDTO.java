package top.shiningon.dynamic.forms.dto.process.base.response;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.entity.Member;

import java.util.Date;
import java.util.List;

@Data
@ApiModel(value="单据配置对象")
public class BillConfigDTO {

    @ApiModelProperty(position=1, name = "id", required = true, notes="id", example = "0")
    private Long id;

    @ApiModelProperty(position=2, name = "flowConfigId", notes="流程配置id", example = "0")
    private Long flowConfigId;

    @ApiModelProperty(position=3, name = "flowConfigName", notes="流程配置名称", example = "名称")
    private String flowConfigName;

    @ApiModelProperty(position=4, name = "fieldConfigList", notes="字段配置列表", example = "字段列表")
    private List<BillFormFieldConfigDTO> fieldConfigList;

    @ApiModelProperty(position=5, name = "code", notes="单据code", example = "code")
    private String code;

    @ApiModelProperty(position=6, name = "title", notes="单据标题", example = "标题")
    private String title;

    @ApiModelProperty(position=7, name = "subTitle", notes="单据子标题", example = "子标题")
    private String subTitle;

    @ApiModelProperty(position=8, name = "createBy", notes="单据创建人", example = "用户A")
    private String createBy;

    @ApiModelProperty(position=9, name = "createBy", notes="单据创建人", example = "用户A")
    private Member createMember;

    @ApiModelProperty(position=10, name = "createTime", notes="单据创建时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(position=11, name = "updateBy", notes="单据更新人", example = "用户A")
    private String updateBy;

    @ApiModelProperty(position=12, name = "createBy", notes="单据更新人", example = "用户A")
    private Member updateMember;

    @ApiModelProperty(position=13, name = "updateTime", notes="单据更新时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    @ApiModelProperty(position=14, name = "delflag", notes="T or F", example = "F")
    private Boolean delflag;

}
