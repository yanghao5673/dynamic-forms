package top.shiningon.dynamic.forms.dto.process.base.request;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowConfigStatusEnum;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;

@Data
public class FlowConfigSearchListDTO extends PageFormDTO {

    private String name;

    private FlowConfigStatusEnum status;

    private Boolean delflag = false;

}
