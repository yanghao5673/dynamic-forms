package top.shiningon.dynamic.forms.dto.process.bill.response.detail;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.BillStatusEnum;
import top.shiningon.dynamic.forms.entity.Member;

import java.util.Date;

@Data
@ApiModel(value="单据详情对象-单据主体")
public class BillDetailBillListDTO {

    @ApiModelProperty(position=1, name = "id", required = true, notes="id", example = "0")
    private Long id;

    @ApiModelProperty(position=2, name = "title", notes="单据标题", example = "请假单")
    private String title;

    @ApiModelProperty(position=3, name = "subTitle", notes="单据子标题", example = "一般员工")
    private String subTitle;

    @ApiModelProperty(position=4, name = "nowFlowStepName", notes="单据当前流程步骤名称", example = "主管审批")
    private String nowFlowStepName;

    @ApiModelProperty(position=5, name = "status", notes="单据状态", example = "审批中")
    private BillStatusEnum status;

    @ApiModelProperty(position=6, name = "createBy", notes="子字段创建人", example = "用户A")
    private String createBy;

    @ApiModelProperty(position=7, name = "createBy", notes="单据创建人", example = "用户A")
    private Member createMember;

    @ApiModelProperty(position=8, name = "createTime", notes="子字段创建时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
