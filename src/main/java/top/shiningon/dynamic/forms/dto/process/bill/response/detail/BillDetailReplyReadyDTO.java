package top.shiningon.dynamic.forms.dto.process.bill.response.detail;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowStepReplyTypeEnum;

import java.util.List;

@Data
@ApiModel(value="单据详情对象-单据主体")
public class BillDetailReplyReadyDTO {

    @ApiModelProperty(position=1, name = "id", required = true, notes="id", example = "0")
    private Long id;

    @ApiModelProperty(position=2, name = "flowStepId", notes="流程步骤Id", example = "0")
    private Long flowStepId;

    @ApiModelProperty(position=3, name = "flowStepName", notes="流程步骤名称", example = "主管审核")
    private String flowStepName;

    @ApiModelProperty(position=4, name = "replyType", notes="流程步骤的审核类型", example = "一票通过")
    private FlowStepReplyTypeEnum replyType;

    @ApiModelProperty(position=5, name = "replyTypeCode", notes="流程步骤的审核类型Code", example = "0")
    private Integer replyTypeCode;

    @ApiModelProperty(position=6, name = "index", notes="单据审核顺序", example = "1")
    private Integer index;

    @ApiModelProperty(position=7, name = "isNow", notes="是否是当前单据审核步骤", example = "F")
    private Boolean isNow;

    @ApiModelProperty(position=8, name = "userList", notes="单据审核步骤涉及的相关用户", example = "用户Id1,用户Id2")
    private List<String> userList;

    public Integer getreplyTypeCode(){
        if(null != replyType){
            return replyType.getKey();
        }
        return null;
    }

}
