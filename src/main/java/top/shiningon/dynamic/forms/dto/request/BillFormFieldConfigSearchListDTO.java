package top.shiningon.dynamic.forms.dto.request;

import lombok.Data;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;

@Data
public class BillFormFieldConfigSearchListDTO extends PageFormDTO {

    private Long billConfigId;

    private String inputName;

    private Boolean delflag = false;

}
