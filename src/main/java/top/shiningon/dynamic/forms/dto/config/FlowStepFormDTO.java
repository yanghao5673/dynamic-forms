package top.shiningon.dynamic.forms.dto.config;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import top.shiningon.dynamic.forms.constant.FlowStepReplyTypeEnum;
import top.shiningon.dynamic.forms.constant.FlowStepReplyUserTypeEnum;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class FlowStepFormDTO {

    private Long id;

    @NotNull
    private Long flowConfigId;

    @NotNull
    private String name;

    @NotNull
    private Integer index;

    @NotNull
    private FlowStepReplyTypeEnum replyType;

    @NotNull
    private FlowStepReplyUserTypeEnum replyUserType;

    @NotNull
    private String userTypeIds;

    @NotNull
    private String operBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date operTime;

    private Boolean delflag = false;

}
