package top.shiningon.dynamic.forms.dto.process.bill.response.detail;

import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.BillReplyStatusEnum;
import top.shiningon.dynamic.forms.entity.Member;

import java.util.Date;

@Data
@ApiModel(value="单据详情对象-单据审核信息")
public class BillDetailReplyDTO {

    @ApiModelProperty(position=1, name = "id", required = true, notes="id", example = "0")
    private Long id;

    @ApiModelProperty(position=2, name = "flowStepName", notes="流程步骤名称", example = "主管审核")
    private String flowStepName;

    @ApiModelProperty(position=3, name = "replyStatus", notes="审核状态", example = "审批通过")
    private BillReplyStatusEnum replyStatus;

    @ApiModelProperty(position=4, name = "replyStatusCode", notes="审核状态Code", example = "1")
    private Integer replyStatusCode;

    @ApiModelProperty(position=5, name = "index", notes="审核顺序", example = "1")
    private Integer index;

    @ApiModelProperty(position=6, name = "remark", notes="备注", example = "")
    private String remark;

    @ApiModelProperty(position=7, name = "createBy", notes="子字段创建人", example = "用户A")
    private String createBy;

    @ApiModelProperty(position=8, name = "createBy", notes="单据创建人", example = "用户A")
    private Member createMember;

    @ApiModelProperty(position=9, name = "createTime", notes="子字段创建时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    public Integer getReplyStatusCode(){
        return this.replyStatus.getKey();
    }

}
