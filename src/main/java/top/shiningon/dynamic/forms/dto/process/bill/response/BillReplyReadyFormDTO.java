package top.shiningon.dynamic.forms.dto.process.bill.response;

import com.baomidou.mybatisplus.annotation.TableField;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowStepReplyTypeEnum;
import top.shiningon.dynamic.forms.entity.Member;

import java.util.Date;
import java.util.List;

@Data
@ApiModel(value="单据审核信息暂存对象")
public class BillReplyReadyFormDTO {

    @ApiModelProperty(position=1, name = "id", required = true, notes="id", example = "0")
    private Long id;

    @ApiModelProperty(position=2, name = "billListId", notes="单据Id", example = "0")
    private Long billListId;

    @ApiModelProperty(position=3, name = "billTitle", notes="单据标题", example = "请假单")
    private String billTitle;

    @ApiModelProperty(position=4, name = "billSubTitle", notes="单据子标题", example = "一般员工")
    private String billSubTitle;

    @ApiModelProperty(position=5, name = "flowStepId", notes="流程步骤名称", example = "主管审核")
    private Long flowStepId;

    @ApiModelProperty(position=6, name = "flowStepName", notes="流程步骤名称", example = "主管审核")
    private String flowStepName;

    @ApiModelProperty(position=7, name = "replyType", notes="流程步骤的审核类型", example = "一票通过")
    private FlowStepReplyTypeEnum replyType;

    @ApiModelProperty(position=8, name = "replyTypeCode", notes="流程步骤的审核类型Code", example = "0")
    private Integer replyTypeCode;

    @ApiModelProperty(position=9, name = "index", notes="单据审核顺序", example = "1")
    private Integer index;

    @ApiModelProperty(position=10, name = "isNow", notes="是否是当前单据审核步骤", example = "F")
    private Boolean isNow;

    @ApiModelProperty(position=11, name = "userList", notes="单据审核步骤涉及的相关用户", example = "用户Id1,用户Id2")
    private List<String> userIdList;

    @ApiModelProperty(position=12, name = "createBy", notes="子字段创建人", example = "用户A")
    private String createBy;

    @ApiModelProperty(position=13, name = "createBy", notes="单据创建人", example = "用户A")
    private Member createMember;

    @ApiModelProperty(position=14, name = "createTime", notes="子字段创建时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    @ApiModelProperty(position=15, name = "updateBy", notes="子字段更新人", example = "用户A")
    private String updateBy;

    @ApiModelProperty(position=16, name = "createBy", notes="单据更新人", example = "用户A")
    private Member updateMember;

    @ApiModelProperty(position=17, name = "updateTime", notes="子字段更新时间", example = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    public Integer getReplyTypeCode(){
        if(null != replyType){
            return replyType.getKey();
        }
        return null;
    }

}
