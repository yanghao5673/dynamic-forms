package top.shiningon.dynamic.forms.dto.admin;

import lombok.Data;

@Data
public class BillFieldSubFormDTO {

    private Long id;

    private Long formFieldId;

    private String inputVal;

    private Integer index;

    private Boolean defSelect;

}
