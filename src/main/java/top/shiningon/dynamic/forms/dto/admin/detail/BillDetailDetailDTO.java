package top.shiningon.dynamic.forms.dto.admin.detail;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.InputTypeEnum;

@Data
public class BillDetailDetailDTO {

    private Long id;

    private Integer index;

    private String inputName;

    private InputTypeEnum inputType;

    private String inputVal;

}
