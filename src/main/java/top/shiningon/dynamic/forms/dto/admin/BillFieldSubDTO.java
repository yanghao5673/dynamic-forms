package top.shiningon.dynamic.forms.dto.admin;

import lombok.Data;

@Data
public class BillFieldSubDTO {

    private Long id;

    private Long formFieldId;

    private String formFieldName;

    private String inputVal;

    private Integer index;

    private Boolean defSelect;

}
