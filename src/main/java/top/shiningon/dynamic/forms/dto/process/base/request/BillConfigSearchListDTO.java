package top.shiningon.dynamic.forms.dto.process.base.request;

import lombok.Data;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;

import java.util.List;

@Data
public class BillConfigSearchListDTO extends PageFormDTO {

    private List<Long> workFlowIdList;

    private String code;

    private String title;

    private Boolean delflag = false;

}
