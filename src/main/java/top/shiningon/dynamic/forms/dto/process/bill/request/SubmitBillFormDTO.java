package top.shiningon.dynamic.forms.dto.process.bill.request;

import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * 用户提交表单对象，提交对象
 */
@Data
public class SubmitBillFormDTO {

    @NotNull
    private Long billConfigId;

    @NotNull
    private String title;

    private String subTitle;

    @NotNull
    private String operBy;

    @NotNull
    private List<BillDetailDTO> inputList;

}
