package top.shiningon.dynamic.forms.dto.config;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import top.shiningon.dynamic.forms.constant.MemberSyncModeEnum;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class OutsideConfigMemberSyncFormDTO {

    private MemberSyncModeEnum memberSyncMode;

    private String memberSyncCron;

    private String memberSyncUrl;

    @NotNull
    private String operBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date operTime;

}
