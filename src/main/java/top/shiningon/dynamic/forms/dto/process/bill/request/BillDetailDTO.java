package top.shiningon.dynamic.forms.dto.process.bill.request;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class BillDetailDTO {

    @NotNull
    private Long fieldId;

    private String inputVal;

}
