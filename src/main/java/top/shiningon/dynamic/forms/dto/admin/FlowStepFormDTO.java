package top.shiningon.dynamic.forms.dto.admin;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowStepReplyTypeEnum;
import top.shiningon.dynamic.forms.constant.FlowStepReplyUserTypeEnum;

@Data
public class FlowStepFormDTO {

    private Long id;

    private Long flowConfigId;

    private String name;

    private Integer index;

    private FlowStepReplyTypeEnum replyType;

    private FlowStepReplyUserTypeEnum replyUserType;

    private String userTypeIds;

}
