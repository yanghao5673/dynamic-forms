package top.shiningon.dynamic.forms.dto.admin.base;

import lombok.Data;

@Data
public class ComboBoxOptionDTO {

    private String id;

    private String name;

    private String text;

    private String desc;

    private String extend1;

    private String extend2;

}
