package top.shiningon.dynamic.forms.dto.request;

import lombok.Data;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;

import java.util.List;

@Data
public class FlowStepSearchListDTO extends PageFormDTO {

    private List<Long> flowConfigIdList;

    private String name;

    private Boolean delflag = false;

    private String orderCol;

    private String orderType;

}
