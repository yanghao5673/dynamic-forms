package top.shiningon.dynamic.forms.dto.process.bill.response;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowStepReplyTypeEnum;

import java.util.List;

@Data
public class DetailBillFormReplyReadyDTO {

    private Long id;

    private Long billListId;

    private Long flowConfigId;

    private Long flowStepId;

    private FlowStepReplyTypeEnum replyType;

    private Integer index;

    private Boolean isNow;

    private List<String> userIdList;

}
