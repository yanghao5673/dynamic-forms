package top.shiningon.dynamic.forms.dto.admin;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowConfigStatusEnum;

@Data
public class FlowConfigDTO {

    private Long id;

    private String name;

    private FlowConfigStatusEnum status;

    private Integer statusCode;

    public Integer getStatusCode(){
        if(null != status) {
            this.status.getKey();
        }
        return null;
    }

}
