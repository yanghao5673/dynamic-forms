package top.shiningon.dynamic.forms.dto.request;

import lombok.Data;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;

@Data
public class BillReplyReadySearchListDTO extends PageFormDTO {

    private Long billListId;

    private Long flowStepId;

    private Boolean isNow;

    private Boolean delflag = false;

}
