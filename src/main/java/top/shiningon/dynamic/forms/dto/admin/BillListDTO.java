package top.shiningon.dynamic.forms.dto.admin;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.BillStatusEnum;

import java.util.Date;

@Data
public class BillListDTO {

    private Long id;

    private Long billConfigId;

    private String billConfigName;

    private String title;

    private String subTitle;

    private Long nowFlowStepId;

    private String nowFlowStepName;

    private BillStatusEnum status;

    private Integer statusCode;

    private String createBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String updateBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

}
