package top.shiningon.dynamic.forms.dto.admin.detail;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.BillReplyStatusEnum;

import java.util.Date;

@Data
public class BillDetailReplyDTO {

    private Long id;

    private String flowStepName;

    private BillReplyStatusEnum replyStatus;

    private Integer replyStatusCode;

    private Integer index;

    private String remark;

    private String createBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    public Integer getReplyStatusCode(){
        return this.replyStatus.getKey();
    }

}
