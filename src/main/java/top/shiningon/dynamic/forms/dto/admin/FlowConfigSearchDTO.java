package top.shiningon.dynamic.forms.dto.admin;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowConfigStatusEnum;
import top.shiningon.dynamic.forms.dto.admin.base.PageSupportParamDTO;

@Data
public class FlowConfigSearchDTO extends PageSupportParamDTO {

    private String name;

    private FlowConfigStatusEnum status;

}
