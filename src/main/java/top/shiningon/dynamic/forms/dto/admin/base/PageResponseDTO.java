package top.shiningon.dynamic.forms.dto.admin.base;

import com.baomidou.mybatisplus.core.metadata.IPage;
import lombok.Data;

import java.io.Serializable;
import java.util.List;

/**
 * @author yanghao
 * 2019年5月15日
 */
@Data
public class PageResponseDTO<T> implements Serializable{

	private static final long serialVersionUID = -882889493375262196L;

	private long total;
	private List<T> rows;

	public PageResponseDTO() {

	}

	public PageResponseDTO(IPage<T> page) {
		this.total = page.getTotal();
		this.rows = page.getRecords();
	}

	public PageResponseDTO(long total, List<T> rows) {
		this.total = total;
		this.rows = rows;
	}
	
}
