package top.shiningon.dynamic.forms.dto.rank;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowStepReplyUserTypeEnum;

@Data
public class ReplyRankSearchDTO {

    private Long id;

    private String userId;

    private FlowStepReplyUserTypeEnum replyUserType;

    private String replyId;

}
