package top.shiningon.dynamic.forms.dto.request;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import top.shiningon.dynamic.forms.constant.BillReplyStatusEnum;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class BillReplyFormDTO {

    private Long id;

    @NotNull
    private Long billListId;

    @NotNull
    private Long flowConfigId;

    @NotNull
    private Long flowStepId;

    @NotNull
    private BillReplyStatusEnum replyStatus;

    @NotNull
    private Integer index;

    private String remark;

    @NotNull
    private String operBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date operTime;

    private Boolean delflag = false;

}
