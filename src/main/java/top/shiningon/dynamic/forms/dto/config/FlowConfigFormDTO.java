package top.shiningon.dynamic.forms.dto.config;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import org.springframework.format.annotation.DateTimeFormat;
import top.shiningon.dynamic.forms.constant.FlowConfigStatusEnum;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Data
public class FlowConfigFormDTO {

    private Long id;

    @NotNull
    private String name;

    @NotNull
    private FlowConfigStatusEnum status;

    @NotNull
    private String operBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date operTime;

    private Boolean delflag = false;

}
