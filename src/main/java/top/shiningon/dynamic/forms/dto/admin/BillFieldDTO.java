package top.shiningon.dynamic.forms.dto.admin;

import lombok.Data;
import top.shiningon.dynamic.forms.constant.InputTypeEnum;

@Data
public class BillFieldDTO {

    private Long id;

    private Long billConfigId;

    private String billConfigName;

    private String inputName;

    private InputTypeEnum inputType;

    private String inputTypeCode;

    private Integer vaildType;

    private Integer index;

    private String vaildMax;

    private String vaildMin;

    private Boolean isRequired;

    private String placeholder;

    private String defVal;

}
