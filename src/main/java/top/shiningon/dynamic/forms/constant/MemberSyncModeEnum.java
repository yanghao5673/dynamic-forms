package top.shiningon.dynamic.forms.constant;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum MemberSyncModeEnum {

    不同步(0, "不同步"), 被动同步(1, "被动同步"), 主动同步_暂不支持(2, "主动同步-暂不支持");

    MemberSyncModeEnum(Integer key, String val){
        this.key = key;
        this.val = val;
    }

    @EnumValue
    private Integer key;

    @JsonValue
    private String val;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }

}
