package top.shiningon.dynamic.forms.constant;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum BillStatusEnum {

    未提交(0, "未提交"), 已提交(1, "已提交"), 审批中(2, "审批中"),
    审批通过(3, "审批通过"), 审批驳回(4, "审批驳回"), 完成(5, "完成");

    BillStatusEnum(Integer key, String val){
        this.key = key;
        this.val = val;
    }

    @EnumValue
    private Integer key;

    @JsonValue
    private String val;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
