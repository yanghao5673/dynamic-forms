package top.shiningon.dynamic.forms.constant;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum FlowStepReplyUserTypeEnum {

    本部门同级别(0, "本部门同级别"),
    部门内直属上级(1, "部门内直属上级"),
    本部门部门主管(2, "本部门部门主管"),
    其他部门指定岗位(3, "其他部门指定岗位"),
    其他部门主管(4, "其他部门主管"),
    指定人员(5, "指定人员");

    FlowStepReplyUserTypeEnum(Integer key, String val){
        this.key = key;
        this.val = val;
    }

    @EnumValue
    private Integer key;

    @JsonValue
    private String val;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
