package top.shiningon.dynamic.forms.constant;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum FlowStepReplyTypeEnum {

    一票通过(0, "一票通过"), 会签通过(1, "会签通过"), 知会(2, "知会");

    FlowStepReplyTypeEnum(Integer key, String val){
        this.key = key;
        this.val = val;
    }

    @EnumValue
    private Integer key;

    @JsonValue
    private String val;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
