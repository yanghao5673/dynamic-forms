package top.shiningon.dynamic.forms.constant;

public class Cache1HKey {

    public static final String SYS_CONFIG = "sysConfig";

    public static final String FLOW_STEP_BY_ID = "flowStepById:";

    public static final String NEXT_FLOW_STEP_BY_FLOW_CONFIG = "nextFlowStepByFlowConfig:";

    public static final String FLOW_STEP_LIST_BY_FLOW_CONFIG_ID = "flowStepListByFlowConfigId:";

    public static final String FLOW_CONFIG_BY_ID = "flowConfigById:";

    public static final String FLOW_CONFIG_FULL_BY_ID = "flowConfigFullById:";

    public static final String BILL_CONFIG_BY_ID = "billConfigById:";

    public static final String BILL_DETAIL_BY_ID = "billDetailById:";

    public static final String BILL_FORM_FIELD_CONFIG_BY_BILL_CONFIG_ID = "billFormFieldConfigByBillConfigId:";

    public static final String BILL_FORM_FIELD_CONFIG_BY_ID = "billFormFieldConfigById:";

    public static final String BILL_FORM_FIELD_SUB_CONFIG_BY_ID = "billFormFieldSubConfigById:";

    public static final String BILL_FORM_FIELD_SUB_CONFIG_LIST_BY_FIELD_CONFIG_ID = "billFormFieldSubConfigListByFieldConfigId:";

    public static final String MEMBER_BY_ID = "memberById:";

}
