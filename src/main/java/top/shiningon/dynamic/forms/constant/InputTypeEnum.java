package top.shiningon.dynamic.forms.constant;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum InputTypeEnum {

    文本框("text", "文本框"), 数字框("num", "数字框"), 选择框("select", "选择框"),
    单选框("radio", "单选框"), 多选框("checkbox", "多选框"), 文本域("textarea", "文本域"),
    图片("picture", "图片"), 文件("file", "文件");

    InputTypeEnum(String key, String val){
        this.key = key;
        this.val = val;
    }

    @EnumValue
    private String key;

    @JsonValue
    private String val;

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
