package top.shiningon.dynamic.forms.constant;

public enum ErrorCodeEnum {

    GLOBAL_UNKNOW_REQUEST(404, "未知请求"),
    GLOBAL_UNKNOW(501, "未知异常"),
    GLOBAL_UNLOGIN(-1, "未登录"),
    GLOBAL_ERROR(500, "失败"),
    GLOBAL_SUCCESS(200, "成功");

    private int code;
    private String msg;

    ErrorCodeEnum(int code, String msg){
        this.code = code;
        this.msg = msg;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
