package top.shiningon.dynamic.forms.constant;

import com.baomidou.mybatisplus.annotation.EnumValue;
import com.fasterxml.jackson.annotation.JsonValue;

public enum FlowConfigStatusEnum {

    不可用(0, "不可用"), 可用(1, "可用");

    FlowConfigStatusEnum(Integer key, String val){
        this.key = key;
        this.val = val;
    }

    @EnumValue
    private Integer key;

    @JsonValue
    private String val;

    public Integer getKey() {
        return key;
    }

    public void setKey(Integer key) {
        this.key = key;
    }

    public String getVal() {
        return val;
    }

    public void setVal(String val) {
        this.val = val;
    }
}
