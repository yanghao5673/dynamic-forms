package top.shiningon.dynamic.forms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowStepReplyTypeEnum;

import java.util.Date;

@Data
@TableName("df_bill_reply_ready")
public class BillReplyReady {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long billListId;

    private Long flowStepId;

    private FlowStepReplyTypeEnum replyType;

    @TableField("`index`")
    private Integer index;

    private Boolean isNow;

    private String createBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String updateBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    private Boolean delflag;

    public void initBy(Long billListId, FlowStep step){
        this.billListId = billListId;
        this.flowStepId = step.getId();
        this.replyType = step.getReplyType();
        this.index = step.getIndex();
        this.isNow = false;
        this.createBy = "-1";
        this.createTime = new Date();
        this.delflag = false;
    }

}