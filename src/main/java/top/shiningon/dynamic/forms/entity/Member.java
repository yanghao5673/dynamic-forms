package top.shiningon.dynamic.forms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@TableName("df_member")
public class Member {

    @TableId(type = IdType.INPUT)
    private String id;

    private String name;

    private String position;

    private String department;

}