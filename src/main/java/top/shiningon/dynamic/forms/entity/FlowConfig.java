package top.shiningon.dynamic.forms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.FlowConfigStatusEnum;

import java.util.Date;

@Data
@TableName("df_flow_config")
public class FlowConfig {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String name;

    private FlowConfigStatusEnum status;

    private String createBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String updateBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    private Boolean delflag;

}