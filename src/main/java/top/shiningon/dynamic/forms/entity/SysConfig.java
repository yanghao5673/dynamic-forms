package top.shiningon.dynamic.forms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.Data;

@Data
@TableName("df_sys_config")
public class SysConfig {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Boolean loginAble;

    private String loginName;

    private String loginPassword;

}