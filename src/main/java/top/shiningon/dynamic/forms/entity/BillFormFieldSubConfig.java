package top.shiningon.dynamic.forms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import java.util.Date;

@Data
@TableName("df_bill_form_field_sub_config")
public class BillFormFieldSubConfig {

    @TableId(type = IdType.AUTO)
    private Long id;

    private Long formFieldId;

    private String inputVal;

    @TableField("`index`")
    private Integer index;

    private Boolean defSelect;

    private String createBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String updateBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    private Boolean delflag;

}