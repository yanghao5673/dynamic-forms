package top.shiningon.dynamic.forms.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import top.shiningon.dynamic.forms.constant.MemberSyncModeEnum;

import java.util.Date;

@Data
@TableName("df_outside_config")
public class OutsideConfig {

    @TableId(type = IdType.AUTO)
    private Long id;

    private String urlExpression;

    private String replyCallBackUrl;

    private MemberSyncModeEnum memberSyncMode;

    private String memberSyncCron;

    private String memberSyncUrl;

    private String createBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date createTime;

    private String updateBy;

    @JsonFormat(locale="zh", timezone="GMT+8", pattern="yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    private Boolean delflag;

}