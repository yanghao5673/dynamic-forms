package top.shiningon.dynamic.forms.rest.config;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dto.config.FlowConfigFormDTO;
import top.shiningon.dynamic.forms.service.FlowConfigService;

@Api(tags = "流程配置-更新操作")
@ApiSupport(order = 1)
@Controller("top.shiningon.dynamic.forms.rest.bill.flow.FlowConfigController")
@RequestMapping(value = "/config/flow/config")
public class FlowConfigController {

    @Autowired
    FlowConfigService flowConfigService;
    
    /**
     * 更新单个流程配置
     */
    @ApiOperation(value = "新增、更新单个流程配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 1)
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam update(@RequestBody FlowConfigFormDTO formDTO) {
        //判断新增还是更新
        return flowConfigService.saveFlowConfigForm(formDTO);
    }

    /**
     * 删除单个流程配置
     */
    @ApiOperation(value = "删除单个流程配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 2)
    @RequestMapping(value = "del/{id}/{operId}", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam del(@PathVariable(name = "id") Long id, @PathVariable(name = "operId") String operId) {
        if(flowConfigService.delFlowConfig(id,  operId)){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR);
    }

}
