package top.shiningon.dynamic.forms.rest.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.constant.MemberSyncModeEnum;
import top.shiningon.dynamic.forms.dto.admin.OutsideMemberSyncFormDTO;
import top.shiningon.dynamic.forms.service.OutsideConfigService;

import javax.servlet.http.HttpServletRequest;

@Controller("top.shiningon.dynamic.forms.rest.admin.OutsideConfigController")
@RequestMapping(value = "/admin/manager/outsideConfig")
public class OutsideConfigController {

    @Autowired
    OutsideConfigService outsideConfigService;

    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("entity", this.outsideConfigService.getConfig());
        model.addAttribute("memberSyncModeEnum", MemberSyncModeEnum.values());
        return "admin/manager/outsideConfig/index";
    }

    @RequestMapping(value = "saveUrlExpression", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam saveUrlExpression(HttpServletRequest request) {
        String urlExpression = request.getParameter("urlExpression");
        String replyCallBackUrl = request.getParameter("replyCallBackUrl");
        return this.outsideConfigService.updateUrlExpression(urlExpression, replyCallBackUrl, request.getSession().getAttribute("user").toString());
    }

    @RequestMapping(value = "saveMemberSyncConfig", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam saveMemberSyncConfig(OutsideMemberSyncFormDTO formDTO, HttpServletRequest request) {
        return this.outsideConfigService.updateMemberSync(formDTO, request.getSession().getAttribute("user").toString());
    }

}
