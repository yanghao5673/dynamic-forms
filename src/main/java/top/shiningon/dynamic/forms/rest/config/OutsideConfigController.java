package top.shiningon.dynamic.forms.rest.config;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dto.admin.OutsideMemberSyncFormDTO;
import top.shiningon.dynamic.forms.dto.config.FlowStepFormDTO;
import top.shiningon.dynamic.forms.dto.config.OutsideConfigMemberSyncFormDTO;
import top.shiningon.dynamic.forms.dto.config.OutsideConfigUrlFormDTO;
import top.shiningon.dynamic.forms.service.FlowStepService;
import top.shiningon.dynamic.forms.service.OutsideConfigService;

@Api(tags = "外部系统的审核角色-更新操作")
@ApiSupport(order = 6)
@Controller("top.shiningon.dynamic.forms.rest.config.OutsideConfigController")
@RequestMapping(value = "/config/outsideConfig")
public class OutsideConfigController {

    @Autowired
    OutsideConfigService outsideConfigService;
    
    /**
     * 新增、更新外部系统的审核角色信息
     */
    @ApiOperation(value = "新增、更新外部系统的审核角色信息", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 1)
    @RequestMapping(value = "updateUrl", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam updateUrl(@RequestBody @Validated OutsideConfigUrlFormDTO formDTO) {
        return outsideConfigService.updateUrlExpression(formDTO.getUrlExpression(), formDTO.getReplyCallBackUrl(), formDTO.getOperBy());
    }

    /**
     * 新增、更新外部系统的员工同步配置
     */
    @ApiOperation(value = "新增、更新外部系统的员工同步配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 2)
    @RequestMapping(value = "updateMemberSync", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam updateMemberSync(@RequestBody @Validated OutsideConfigMemberSyncFormDTO formDTO) {
        OutsideMemberSyncFormDTO dto = new OutsideMemberSyncFormDTO();
        dto.setMemberSyncMode(formDTO.getMemberSyncMode());
        dto.setMemberSyncCron(formDTO.getMemberSyncCron());
        dto.setMemberSyncUrl(formDTO.getMemberSyncUrl());
        return this.outsideConfigService.updateMemberSync(dto, formDTO.getOperBy());
    }

}
