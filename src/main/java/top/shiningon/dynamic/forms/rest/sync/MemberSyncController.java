package top.shiningon.dynamic.forms.rest.sync;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.constant.MemberSyncModeEnum;
import top.shiningon.dynamic.forms.dto.sync.request.MemberSyncDTO;
import top.shiningon.dynamic.forms.job.MemberSyncTask;
import top.shiningon.dynamic.forms.service.MemberService;

import java.util.List;

@Api(tags = "单据配置-更新操作")
@ApiSupport(order = 1)
@Controller("top.shiningon.dynamic.forms.rest.sync.MemberSyncController")
@RequestMapping(value = "/sync/member")
public class MemberSyncController {

    @Autowired
    MemberService memberService;

    /**
     * 更新单个员工
     */
    @ApiOperation(value = "新增、更新单个员工", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 1)
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam update(@RequestBody @Validated MemberSyncDTO formDTO) {
        if(MemberSyncModeEnum.被动同步.getKey() == MemberSyncTask.SYNC_MODE){
            return memberService.saveMember(formDTO);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("未开启被动同步员工信息");
    }

    /**
     * 更新批量员工
     */
    @ApiOperation(value = "新增、更新批量员工", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 2)
    @RequestMapping(value = "updateBatch", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam updateBatch(@RequestBody @Validated List<MemberSyncDTO> formDTOList) {
        if(MemberSyncModeEnum.被动同步.getKey() == MemberSyncTask.SYNC_MODE){
            return memberService.saveBatchMember(formDTOList);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("未开启被动同步员工信息");
    }

    /**
     * 删除单个员工
     */
    @ApiOperation(value = "删除单个员工", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 3)
    @RequestMapping(value = "del/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam del(@PathVariable(name = "id") String id) {
        if(MemberSyncModeEnum.被动同步.getKey() == MemberSyncTask.SYNC_MODE){
            return memberService.delMember(id);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("未开启被动同步员工信息");
    }

    /**
     * 删除批量员工
     */
    @ApiOperation(value = "删除批量员工", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 4)
    @RequestMapping(value = "delBatch", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam delBatch(@RequestBody @Validated List<String> idList) {
        if(MemberSyncModeEnum.被动同步.getKey() == MemberSyncTask.SYNC_MODE){
            return memberService.delBatchMember(idList);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("未开启被动同步员工信息");
    }

}
