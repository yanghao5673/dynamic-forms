package top.shiningon.dynamic.forms.rest.process;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.constant.MemberSyncModeEnum;
import top.shiningon.dynamic.forms.dto.process.base.request.BillConfigSearchListDTO;
import top.shiningon.dynamic.forms.dto.process.base.request.FlowConfigSearchListDTO;
import top.shiningon.dynamic.forms.dto.process.base.response.*;
import top.shiningon.dynamic.forms.dto.request.BillReplyReadySearchListDTO;
import top.shiningon.dynamic.forms.dto.request.BillReplyReadyUserSearchListDTO;
import top.shiningon.dynamic.forms.dto.request.FlowStepSearchListDTO;
import top.shiningon.dynamic.forms.entity.*;
import top.shiningon.dynamic.forms.service.*;

import java.util.*;
import java.util.stream.Collectors;

@Api(tags = "基础数据获取")
@ApiSupport(order = 6)
@Controller("top.shiningon.dynamic.forms.rest.pub.BaseController")
@RequestMapping(value = "/process/base")
public class BaseController {

    @Autowired
    private BillConfigService billConfigService;

    @Autowired
    private BillFormFieldConfigService billFormFieldConfigService;

    @Autowired
    private BillFormFieldSubConfigService billFormFieldSubConfigService;

    @Autowired
    private BillReplyReadyService billReplyReadyService;
    @Autowired
    private BillReplyReadyUserService billReplyReadyUserService;

    @Autowired
    private FlowConfigService flowConfigService;
    @Autowired
    private FlowStepService flowStepService;
    @Autowired
    private MemberService memberService;
    @Autowired
    private OutsideConfigService outsideConfigService;

    /**
     * 获取单个单据配置
     */
    @ApiOperation(value = "获取单个单据配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 1)
    @RequestMapping(value = "getBillConfigById/{billConfigId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<BillConfigDTO> getBillConfigById(@PathVariable(name = "billConfigId") Long billConfigId) {
        BillConfig config = billConfigService.getById(billConfigId);
        BillConfigDTO dto = new BillConfigDTO();
        BeanUtil.copyProperties(config, dto);
        dto.setCreateMember(memberService.getById(dto.getCreateBy()));
        dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
        FlowConfig fc = this.flowConfigService.getById(config.getFlowConfigId());
        if(null != fc) {
            dto.setFlowConfigName(fc.getName());
        }
        return new ReturnParam<BillConfigDTO>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(dto);
    }

    /**
     * 获取单个单据字段配置
     */
    @ApiOperation(value = "获取单个单据字段配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 2)
    @RequestMapping(value = "getBillFormFieldById/{formFieldId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<BillFormFieldConfigDTO> getBillFormFieldById(@PathVariable(name = "formFieldId") Long formFieldId) {
        BillFormFieldConfig config = billFormFieldConfigService.getById(formFieldId);
        BillFormFieldConfigDTO dto = new BillFormFieldConfigDTO();
        BeanUtil.copyProperties(config, dto);
        dto.setCreateMember(memberService.getById(dto.getCreateBy()));
        dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
        return new ReturnParam<BillFormFieldConfigDTO>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(dto);
    }

    /**
     * 获取单个单据字段子项配置
     */
    @ApiOperation(value = "获取单个单据字段子项配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 3)
    @RequestMapping(value = "getBillFormFieldSubById/{formFieldSubId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<BillFormFieldSubConfigDTO> getBillFormFieldSubById(@PathVariable(name = "formFieldSubId") Long formFieldSubId) {
        BillFormFieldSubConfig config = billFormFieldSubConfigService.getById(formFieldSubId);
        BillFormFieldSubConfigDTO dto = new BillFormFieldSubConfigDTO();
        BeanUtil.copyProperties(config, dto);
        dto.setCreateMember(memberService.getById(dto.getCreateBy()));
        dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
        return new ReturnParam<BillFormFieldSubConfigDTO>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(dto);
    }

    /**
     * 获取单个流程配置
     */
    @ApiOperation(value = "获取单个流程配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 4)
    @RequestMapping(value = "getFlowConfigById/{flowConfigId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<FlowConfigDTO> getFlowConfigById(@PathVariable(name = "flowConfigId") Long flowConfigId) {
        FlowConfig config = flowConfigService.getById(flowConfigId);
        FlowConfigDTO dto = new FlowConfigDTO();
        BeanUtil.copyProperties(config, dto);
        dto.setCreateMember(memberService.getById(dto.getCreateBy()));
        dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
        return new ReturnParam<FlowConfigDTO>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(dto);
    }

    /**
     * 获取单个流程步骤配置
     */
    @ApiOperation(value = "获取单个流程步骤配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 5)
    @RequestMapping(value = "getFlowStepById/{flowStepId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<FlowStepDTO> getFlowStepById(@PathVariable(name = "flowStepId") Long flowStepId) {
        FlowStep config = flowStepService.getById(flowStepId);
        FlowStepDTO dto = new FlowStepDTO();
        BeanUtil.copyProperties(config, dto);
        dto.setCreateMember(memberService.getById(dto.getCreateBy()));
        dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
        return new ReturnParam<FlowStepDTO>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(dto);
    }

    /**
     * 获取单据配置
     */
    @ApiOperation(value = "获取单据配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 6)
    @RequestMapping(value = "getBillConfig", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<SearchResoultDTO<BillConfigDTO>> getBillConfig(BillConfigSearchListDTO formDTO) {
        SearchResoultDTO<BillConfig> billConfigSearchDTO = billConfigService.searchBillConfigList(formDTO);
        SearchResoultDTO<BillConfigDTO> resoultDTO = new SearchResoultDTO<BillConfigDTO>();
        resoultDTO.setPage(billConfigSearchDTO.isPage());
        resoultDTO.setPageNum(billConfigSearchDTO.getPageNum());
        resoultDTO.setPageSize(billConfigSearchDTO.getPageSize());
        resoultDTO.setRowCount(billConfigSearchDTO.getRowCount());
        resoultDTO.setList(new ArrayList<>(billConfigSearchDTO.getList().size()));
        if(billConfigSearchDTO.getList().size() > 0) {
            List<FlowConfig> flowConfigList = this.flowConfigService.listByIds(billConfigSearchDTO.getList().stream().map(BillConfig::getFlowConfigId).collect(Collectors.toList()));
            for (BillConfig bc : billConfigSearchDTO.getList()) {
                BillConfigDTO dto = new BillConfigDTO();
                BeanUtil.copyProperties(bc, dto);
                dto.setCreateMember(memberService.getById(dto.getCreateBy()));
                dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
                Optional<FlowConfig> opt = flowConfigList.stream().filter(obj -> obj.getId().equals(bc.getFlowConfigId())).findAny();
                opt.ifPresent(obj -> dto.setFlowConfigName(obj.getName()));
                resoultDTO.getList().add(dto);
            }
        }
        return new ReturnParam<SearchResoultDTO<BillConfigDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(resoultDTO);
    }

    /**
     * 获取单据配置的表单字段和子项信息
     */
    @ApiOperation(value = "获取单据配置的表单字段和子项信息", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 7)
    @RequestMapping(value = "getBillFormField/{billConfigId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<List<BillFormFieldConfigDTO>> getBillFormField(@PathVariable(name = "billConfigId") Long billConfigId) {
        SearchResoultDTO<BillFormFieldConfig> formFieldConfigSearchList = billFormFieldConfigService.searchBillFormFieldConfigList(billConfigId);
        SearchResoultDTO<BillFormFieldSubConfig> subConfigSearchList = this.billFormFieldSubConfigService.searchBillFormFieldSubConfigList(formFieldConfigSearchList.getList().stream().map(BillFormFieldConfig::getId).collect(Collectors.toList()));
        Map<Long, List<BillFormFieldSubConfig>> subConfigMap = new HashMap<>();
        if(subConfigSearchList.getList().size() > 0){
            for(BillFormFieldSubConfig subConfig : subConfigSearchList.getList()){
                if(!subConfigMap.containsKey(subConfig.getFormFieldId())){
                    subConfigMap.put(subConfig.getFormFieldId(), new ArrayList<>());
                }
                subConfigMap.get(subConfig.getFormFieldId()).add(subConfig);
            }
        }
        List<BillFormFieldConfigDTO> resList = new ArrayList<>(formFieldConfigSearchList.getList().size());
        for(BillFormFieldConfig config : formFieldConfigSearchList.getList()){
            BillFormFieldConfigDTO dto = new BillFormFieldConfigDTO();
            BeanUtil.copyProperties(config, dto);
            dto.setCreateMember(memberService.getById(dto.getCreateBy()));
            dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
            if(subConfigMap.containsKey(config.getId())) {
                dto.setFieldSubConfigList(new ArrayList<>());
                List<BillFormFieldSubConfig> subList = subConfigMap.get(config.getId());
                for(BillFormFieldSubConfig sub : subList) {
                    BillFormFieldSubConfigDTO subDTO = BeanUtil.copyProperties(sub, BillFormFieldSubConfigDTO.class);
                    dto.setCreateMember(memberService.getById(dto.getCreateBy()));
                    dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
                    dto.getFieldSubConfigList().add(subDTO);
                }
            }
            resList.add(dto);
        }
        return new ReturnParam<List<BillFormFieldConfigDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(resList);
    }

    /**
     * 获取单据配置的表单字段和子项信息
     */
    @ApiOperation(value = "获取单据配置的表单字段子项信息", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 8)
    @RequestMapping(value = "getBillFormFieldSub/{formFieldId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<List<BillFormFieldSubConfigDTO>> getBillFormFieldSub(@PathVariable(name = "formFieldId") Long formFieldId) {
        List<BillFormFieldSubConfig> formFieldConfigList = billFormFieldSubConfigService.searchBillFormFieldSubConfigList(formFieldId);
        List<BillFormFieldSubConfigDTO> resList = new ArrayList<>(formFieldConfigList.size());
        for(BillFormFieldSubConfig config : formFieldConfigList){
            BillFormFieldSubConfigDTO dto = new BillFormFieldSubConfigDTO();
            BeanUtil.copyProperties(config, dto);
            dto.setCreateMember(memberService.getById(dto.getCreateBy()));
            dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
            resList.add(dto);
        }
        return new ReturnParam<List<BillFormFieldSubConfigDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(resList);
    }

    /**
     * 获取单据制定的审核流程列表
     */
    @ApiOperation(value = "获取单据制定的审核流程列表", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 9)
    @RequestMapping(value = "getBillReplyReady/{billId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<List<BillDetailReplyReadyDTO>> getBillReplyReady(@PathVariable(name = "billId") Long billId) {
        BillReplyReadySearchListDTO formDTO = new BillReplyReadySearchListDTO();
        formDTO.setBillListId(billId);
        SearchResoultDTO<BillReplyReady> replyReadySearchList = billReplyReadyService.searchBillReplyReadyList(formDTO);
        if(CollUtil.isNotEmpty(replyReadySearchList.getList())) {
            List<BillReplyReady> replyReadyList = new ArrayList<>();
            List<Long> flowStepIdList = new ArrayList<>();
            for(BillReplyReady brr : replyReadySearchList.getList()){
                if(!flowStepIdList.contains(brr.getFlowStepId())) {
                    replyReadyList.add(brr);
                    flowStepIdList.add(brr.getFlowStepId());
                }
            }
            flowStepIdList.clear();

            //单据所有审核流程对应的人
            BillReplyReadyUserSearchListDTO readyUserSearchDTO = new BillReplyReadyUserSearchListDTO();
            readyUserSearchDTO.setReplyReadyIdList(replyReadyList.stream().map(BillReplyReady::getId).collect(Collectors.toList()));
            SearchResoultDTO<BillReplyReadyUser> replyReadyUserSearchList = billReplyReadyUserService.searchBillReplyReadyUserList(readyUserSearchDTO);
            boolean hasReadyUser = replyReadyUserSearchList.getList().size() > 0;

            List<FlowStep> stepList = this.flowStepService.listByIds(replyReadyList.stream().map(BillReplyReady::getFlowStepId).collect(Collectors.toList()));
            List<BillDetailReplyReadyDTO> replyReadyListDTO = new ArrayList<>(replyReadyList.size());
            for(BillReplyReady replyReady : replyReadyList) {
                BillDetailReplyReadyDTO replyDTO = new BillDetailReplyReadyDTO();
                BeanUtil.copyProperties(replyReady, replyDTO);
                Optional<FlowStep> stepOpt = stepList.stream().filter(obj -> obj.getId().equals(replyReady.getFlowStepId())).findAny();
                stepOpt.ifPresent(flowStep -> replyDTO.setFlowStepName(flowStep.getName()));
                if(hasReadyUser) {
                    replyDTO.setUserList(replyReadyUserSearchList.getList().stream()
                            .filter(obj -> obj.getReplyReadyId().equals(replyReady.getId()))
                            .map(BillReplyReadyUser::getUserId).collect(Collectors.toList()));
                }
                replyReadyListDTO.add(replyDTO);
            }
            return new ReturnParam<List<BillDetailReplyReadyDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(replyReadyListDTO);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("尚未制定审核流程");
    }

    /**
     * 获取审核流程配置
     */
    @ApiOperation(value = "获取审核流程配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 10)
    @RequestMapping(value = "getFlowConfig", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<SearchResoultDTO<FlowConfigDTO>> getFlowConfig(FlowConfigSearchListDTO formDTO) {
        SearchResoultDTO<FlowConfig> flowConfigSearchDTO = flowConfigService.searchFlowConfigList(formDTO);
        SearchResoultDTO<FlowConfigDTO> resoultDTO = new SearchResoultDTO<>();
        resoultDTO.setPage(flowConfigSearchDTO.isPage());
        resoultDTO.setPageNum(flowConfigSearchDTO.getPageNum());
        resoultDTO.setPageSize(flowConfigSearchDTO.getPageSize());
        resoultDTO.setRowCount(flowConfigSearchDTO.getRowCount());
        resoultDTO.setList(new ArrayList<>(flowConfigSearchDTO.getList().size()));
        if(flowConfigSearchDTO.getList().size() > 0){
            FlowStepSearchListDTO flowStepFormDTO = new FlowStepSearchListDTO();
            flowStepFormDTO.setFlowConfigIdList(flowConfigSearchDTO.getList().stream().map(FlowConfig::getId).collect(Collectors.toList()));
            SearchResoultDTO<FlowStep> flowStepSearchDTO = this.flowStepService.searchFlowStepList(flowStepFormDTO);
            boolean hasStep = flowStepSearchDTO.getList().size() > 0;
            for(FlowConfig fc : flowConfigSearchDTO.getList()){
                FlowConfigDTO dto = new FlowConfigDTO();
                BeanUtil.copyProperties(fc, dto);
                dto.setCreateMember(memberService.getById(dto.getCreateBy()));
                dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
                if(hasStep) {
                    dto.setFlowStepList(new ArrayList<>());
                    for(FlowStep fs : flowStepSearchDTO.getList()) {
                        if(fs.getFlowConfigId().equals(fc.getId())) {
                            FlowStepDTO stepDTO = new FlowStepDTO();
                            BeanUtil.copyProperties(fs, stepDTO);
                            stepDTO.setCreateMember(memberService.getById(stepDTO.getCreateBy()));
                            stepDTO.setUpdateMember(memberService.getById(stepDTO.getUpdateBy()));
                            dto.getFlowStepList().add(stepDTO);
                        }
                    }
                }
                resoultDTO.getList().add(dto);
            }
            return new ReturnParam<SearchResoultDTO<FlowConfigDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(resoultDTO);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("未找到审核流程");
    }

    /**
     * 获取审核流程配置
     */
    @ApiOperation(value = "获取审核流程配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 11)
    @RequestMapping(value = "getFlowStep/{flowConfigId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<List<FlowStepDTO>> getFlowStep(@PathVariable(name = "flowConfigId") Long flowConfigId) {
        List<FlowStep> formFieldConfigList = flowStepService.searchFlowStepList(flowConfigId);
        List<FlowStepDTO> resList = new ArrayList<>(formFieldConfigList.size());
        for(FlowStep step : formFieldConfigList){
            FlowStepDTO dto = new FlowStepDTO();
            BeanUtil.copyProperties(step, dto);
            dto.setCreateMember(memberService.getById(dto.getCreateBy()));
            dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
            resList.add(dto);
        }
        return new ReturnParam<List<FlowStepDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(resList);
    }

    /**
     * 外部系统的配置
     */
    @ApiOperation(value = "获取外部系统的配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 12)
    @RequestMapping(value = "getOutsideConfig", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<OutsideConfigDTO> getOutsideConfig() {
        OutsideConfig config = outsideConfigService.getConfig();
        if(null == config){
            config = new OutsideConfig();
            config.setMemberSyncMode(MemberSyncModeEnum.不同步);
        }
        OutsideConfigDTO dto = BeanUtil.copyProperties(config, OutsideConfigDTO.class);
        return new ReturnParam<OutsideConfigDTO>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(dto);
    }

}
