package top.shiningon.dynamic.forms.rest.admin;

import cn.hutool.core.bean.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.constant.BillStatusEnum;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dto.admin.BillListDTO;
import top.shiningon.dynamic.forms.dto.admin.BillListSearchDTO;
import top.shiningon.dynamic.forms.dto.request.BillListSearchListDTO;
import top.shiningon.dynamic.forms.entity.BillConfig;
import top.shiningon.dynamic.forms.entity.BillList;
import top.shiningon.dynamic.forms.entity.FlowStep;
import top.shiningon.dynamic.forms.service.BillConfigService;
import top.shiningon.dynamic.forms.service.BillListService;
import top.shiningon.dynamic.forms.service.FlowStepService;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller("top.shiningon.dynamic.forms.rest.admin.BillListController")
@RequestMapping(value = "/admin/bill/list")
public class BillListController {

    @Autowired
    BillConfigService billConfigService;

    @Autowired
    FlowStepService flowStepService;

    @Autowired
    BillListService billListService;

    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("billStatusEnum", BillStatusEnum.values());
        return "admin/bill/list/index";
    }
    
    /**
     * 查找单据配置列表
     */
    @RequestMapping(value = "list", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam<List<BillListDTO>> list(@Validated BillListSearchDTO formDTO) {
        BillListSearchListDTO searchDTO = new BillListSearchListDTO();
        if(null != formDTO.getBillConfigId()) {
            searchDTO.setBillConfigIdList(Collections.singletonList(formDTO.getBillConfigId()));
        }
        searchDTO.setTitle(formDTO.getTitle());
        searchDTO.setStatus(formDTO.getStatus());
        searchDTO.setCreateBy(formDTO.getCreateBy());
        searchDTO.setBeginCreateTime(formDTO.getBeginCreateTime());
        searchDTO.setEndCreateTime(formDTO.getEndCreateTime());
        searchDTO.setUpdateBy(formDTO.getUpdateBy());
        searchDTO.setBeginUpdateTime(formDTO.getBeginUpdateTime());
        searchDTO.setEndUpdateTime(formDTO.getEndUpdateTime());
        searchDTO.setDelflag(null);
        searchDTO.setPageNum(formDTO.getPage());
        searchDTO.setPageSize(formDTO.getRows());
        SearchResoultDTO<BillList> resDTO = billListService.searchBillListList(searchDTO);
        if(!CollectionUtils.isEmpty(resDTO.getList())) {
            List<BillConfig> configList = billConfigService.listByIds(resDTO.getList().stream().map(BillList::getBillConfigId).collect(Collectors.toList()));
            List<FlowStep> stepList = flowStepService.listByIds(resDTO.getList().stream().map(BillList::getNowFlowStepId).collect(Collectors.toList()));
            List<BillListDTO> dtoList = new ArrayList<>(resDTO.getList().size());
            for(BillList bill : resDTO.getList()) {
                BillListDTO dto = new BillListDTO();
                BeanUtil.copyProperties(bill, dto);
                Optional<BillConfig> configOpt = configList.stream().filter(obj -> obj.getId().equals(bill.getBillConfigId())).findAny();
                configOpt.ifPresent(billConfig -> dto.setBillConfigName(billConfig.getTitle()));
                Optional<FlowStep> stepOpt = stepList.stream().filter(obj -> obj.getId().equals(bill.getNowFlowStepId())).findAny();
                stepOpt.ifPresent(flowStep -> dto.setNowFlowStepName(flowStep.getName()));
                dto.setStatusCode(bill.getStatus().getKey());
                dtoList.add(dto);
            }
            return new ReturnParam<List<BillListDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(dtoList);
        }
        return new ReturnParam<List<BillListDTO>>(ErrorCodeEnum.GLOBAL_ERROR).setMessage("没有记录").setContent(new ArrayList<>(0));
    }

}
