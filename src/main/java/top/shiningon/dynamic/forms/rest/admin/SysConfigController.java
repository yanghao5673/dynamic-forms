package top.shiningon.dynamic.forms.rest.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.dto.admin.SysConfigFormDTO;
import top.shiningon.dynamic.forms.entity.SysConfig;
import top.shiningon.dynamic.forms.service.SysConfigService;

import javax.servlet.http.HttpServletRequest;

@Controller("top.shiningon.dynamic.forms.rest.admin.SysConfigController")
@RequestMapping(value = "/admin/manager/sysConfig")
public class SysConfigController {

    @Autowired
    SysConfigService sysConfigService;

    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String index(Model model) {
        SysConfig config = sysConfigService.getSysConfig();
        model.addAttribute("sysConfig", config);
        return "admin/manager/sysConfig/index";
    }

    @RequestMapping(value = "updateLoginInfo", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam updateLoginInfo(SysConfigFormDTO formDTO, HttpServletRequest request) {
        return this.sysConfigService.updateLoginInfo(formDTO);
    }

}
