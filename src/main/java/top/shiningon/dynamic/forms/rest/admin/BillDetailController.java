package top.shiningon.dynamic.forms.rest.admin;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dto.admin.BillDetailDTO;
import top.shiningon.dynamic.forms.dto.admin.detail.BillDetailBillListDTO;
import top.shiningon.dynamic.forms.dto.admin.detail.BillDetailDetailDTO;
import top.shiningon.dynamic.forms.dto.admin.detail.BillDetailReplyDTO;
import top.shiningon.dynamic.forms.dto.admin.detail.BillDetailReplyReadyDTO;
import top.shiningon.dynamic.forms.dto.process.bill.request.BillReplySearchListDTO;
import top.shiningon.dynamic.forms.dto.request.BillDetailSearchListDTO;
import top.shiningon.dynamic.forms.dto.request.BillReplyReadySearchListDTO;
import top.shiningon.dynamic.forms.dto.request.BillReplyReadyUserSearchListDTO;
import top.shiningon.dynamic.forms.entity.*;
import top.shiningon.dynamic.forms.service.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller("top.shiningon.dynamic.forms.rest.admin.BillDetailController")
@RequestMapping(value = "/admin/bill/detail")
public class BillDetailController {

    @Autowired
    private FlowStepService flowStepService;

    @Autowired
    private BillListService billListService;

    @Autowired
    private BillDetailService billDetailService;

    @Autowired
    private BillReplyService billReplyService;

    @Autowired
    private BillReplyReadyService billReplyReadyService;

    @Autowired
    private BillReplyReadyUserService billReplyReadyUserService;

    @RequestMapping(value = "index/{billId}", method = RequestMethod.GET)
    public String index(@PathVariable(name = "billId") Long billId, Model model) {
        BillDetailDTO detailDTO = new BillDetailDTO();
        //单据主体
        BillList bill = billListService.searchBillListSingle(billId);
        if(null == bill){
            model.addAttribute("errorMessage", "单据未找到");
            return "admin/bill/detail/index";
        }
        BillDetailBillListDTO billDTO = new BillDetailBillListDTO();
        BeanUtil.copyProperties(bill, billDTO);
        FlowStep billFlowStep = this.flowStepService.searchFlowStepSingle(bill.getNowFlowStepId());
        if(null != billFlowStep) {
            billDTO.setNowFlowStepName(billFlowStep.getName());
        }
        detailDTO.setBill(billDTO);

        //单据表单
        BillDetailSearchListDTO detailSearchDTO = new BillDetailSearchListDTO();
        detailSearchDTO.setBillListIdList(Arrays.asList(billId));
        SearchResoultDTO<BillDetail> detailSearchList = billDetailService.searchBillDetailList(detailSearchDTO);
        if(CollUtil.isNotEmpty(detailSearchList.getList())){
            List<BillDetailDetailDTO> formInputListDTO = new ArrayList<>(detailSearchList.getList().size());
            for(BillDetail detail : detailSearchList.getList()) {
                BillDetailDetailDTO formInputDTO = new BillDetailDetailDTO();
                BeanUtil.copyProperties(detail, formInputDTO);
                formInputListDTO.add(formInputDTO);
            }
            detailDTO.setFormInputList(formInputListDTO);
        }

        //单据已审核流程
        BillReplySearchListDTO replySearchDTO = new BillReplySearchListDTO();
        replySearchDTO.setBillListId(billId);
        SearchResoultDTO<BillReply> replySearchList = billReplyService.searchBillReplyList(replySearchDTO);
        if(CollUtil.isNotEmpty(detailSearchList.getList())){
            List<FlowStep> stepList = this.flowStepService.listByIds(replySearchList.getList().stream().map(BillReply::getFlowStepId).collect(Collectors.toList()));
            List<BillDetailReplyDTO> replyListDTO = new ArrayList<>(replySearchList.getList().size());
            for(BillReply reply : replySearchList.getList()) {
                BillDetailReplyDTO replyDTO = new BillDetailReplyDTO();
                BeanUtil.copyProperties(reply, replyDTO);
                Optional<FlowStep> opt = stepList.stream().filter(obj -> {return obj.getId().equals(reply.getFlowStepId());}).findAny();
                if(opt.isPresent()) {
                    replyDTO.setFlowStepName(opt.get().getName());
                }
                replyListDTO.add(replyDTO);
            }
            detailDTO.setReplyList(replyListDTO);
        }

        //单据所有审核流程
        BillReplyReadySearchListDTO replyReadySearchDTO = new BillReplyReadySearchListDTO();
        replyReadySearchDTO.setBillListId(billId);
        SearchResoultDTO<BillReplyReady> replyReadySearchList = billReplyReadyService.searchBillReplyReadyList(replyReadySearchDTO);
        if(CollUtil.isNotEmpty(replyReadySearchList.getList())) {
            List<BillReplyReady> replyReadyList = new ArrayList<>();
            List<Long> flowStepIdList = new ArrayList<>();
            for(BillReplyReady brr : replyReadySearchList.getList()){
                if(!flowStepIdList.contains(brr.getFlowStepId())) {
                    replyReadyList.add(brr);
                    flowStepIdList.add(brr.getFlowStepId());
                }
            }
            flowStepIdList.clear();

            //单据所有审核流程对应的人
            BillReplyReadyUserSearchListDTO readyUserSearchDTO = new BillReplyReadyUserSearchListDTO();
            readyUserSearchDTO.setReplyReadyIdList(replyReadyList.stream().map(BillReplyReady::getId).collect(Collectors.toList()));
            SearchResoultDTO<BillReplyReadyUser> replyReadyUserSearchList = billReplyReadyUserService.searchBillReplyReadyUserList(readyUserSearchDTO);
            boolean hasReadyUser = replyReadyUserSearchList.getList().size() > 0;

            List<FlowStep> stepList = this.flowStepService.listByIds(replyReadyList.stream().map(BillReplyReady::getFlowStepId).collect(Collectors.toList()));
            List<BillDetailReplyReadyDTO> replyReadyListDTO = new ArrayList<>(replyReadyList.size());
            for(BillReplyReady replyReady : replyReadyList) {
                BillDetailReplyReadyDTO replyDTO = new BillDetailReplyReadyDTO();
                BeanUtil.copyProperties(replyReady, replyDTO);
                Optional<FlowStep> stepOpt = stepList.stream().filter(obj -> {return obj.getId().equals(replyReady.getFlowStepId());}).findAny();
                if(stepOpt.isPresent()) {
                    replyDTO.setFlowStepName(stepOpt.get().getName());
                }
                if(hasReadyUser) {
                    replyDTO.setUserList(replyReadyUserSearchList.getList().stream()
                            .filter(obj -> {return obj.getReplyReadyId().equals(replyReady.getId());})
                            .map(BillReplyReadyUser::getUserId).collect(Collectors.toList()));
                }
                replyReadyListDTO.add(replyDTO);
            }
            detailDTO.setReplyReadyList(replyReadyListDTO);
        }
        model.addAttribute("entity", detailDTO);
        return "admin/bill/detail/index";
    }

}
