package top.shiningon.dynamic.forms.rest.admin;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.constant.FlowConfigStatusEnum;
import top.shiningon.dynamic.forms.dto.admin.FlowConfigDTO;
import top.shiningon.dynamic.forms.dto.admin.FlowConfigFormDTO;
import top.shiningon.dynamic.forms.dto.admin.FlowConfigSearchDTO;
import top.shiningon.dynamic.forms.dto.admin.base.ComboBoxOptionDTO;
import top.shiningon.dynamic.forms.dto.process.base.request.FlowConfigSearchListDTO;
import top.shiningon.dynamic.forms.entity.FlowConfig;
import top.shiningon.dynamic.forms.service.FlowConfigService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("top.shiningon.dynamic.forms.rest.admin.FlowConfigController")
@RequestMapping(value = "/admin/flow/config")
public class FlowConfigController {

    @Autowired
    FlowConfigService flowConfigService;

    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String index(Model model) {
        model.addAttribute("flowConfigStatusEnum", FlowConfigStatusEnum.values());
        return "admin/flow/config/index";
    }
    
    /**
     * 查找单据配置列表
     */
    @RequestMapping(value = "list", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam<List<FlowConfigDTO>> list(@Validated FlowConfigSearchDTO formDTO) {
        FlowConfigSearchListDTO searchFormDTO = new FlowConfigSearchListDTO();
        searchFormDTO.setName(formDTO.getName());
        searchFormDTO.setStatus(formDTO.getStatus());
        searchFormDTO.setPageNum(formDTO.getPage());
        searchFormDTO.setPageSize(formDTO.getRows());
        SearchResoultDTO<FlowConfig> resDTO = flowConfigService.searchFlowConfigList(searchFormDTO);
        if(!CollectionUtils.isEmpty(resDTO.getList())) {
            List<FlowConfigDTO> dtoList = new ArrayList<>(resDTO.getList().size());
            for(FlowConfig flowConfig : resDTO.getList()){
                FlowConfigDTO dto = new FlowConfigDTO();
                BeanUtil.copyProperties(flowConfig, dto);
                dtoList.add(dto);
            }
            return new ReturnParam<List<FlowConfigDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(dtoList);
        }
        return new ReturnParam<List<FlowConfigDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setMessage("没有记录").setContent(new ArrayList<>(0));
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("entity", new FlowConfigDTO());
        model.addAttribute("flowConfigStatusEnum", FlowConfigStatusEnum.values());
        return "admin/flow/config/addOrEdit";
    }

    @RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id") Long id, Model model) {
        FlowConfig flowConfig = this.flowConfigService.searchFlowConfigSingle(id);
        FlowConfigDTO dto = new FlowConfigDTO();
        BeanUtil.copyProperties(flowConfig, dto);
        model.addAttribute("entity", dto);
        model.addAttribute("flowConfigStatusEnum", FlowConfigStatusEnum.values());
        return "admin/flow/config/addOrEdit";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public Object save(FlowConfigFormDTO formDTO, HttpServletRequest request) {
        top.shiningon.dynamic.forms.dto.config.FlowConfigFormDTO saveFormDTO = new top.shiningon.dynamic.forms.dto.config.FlowConfigFormDTO();
        saveFormDTO.setId(formDTO.getId());
        saveFormDTO.setName(formDTO.getName());
        saveFormDTO.setStatus(formDTO.getStatus());
        saveFormDTO.setOperBy( request.getSession().getAttribute("user").toString() );
        saveFormDTO.setOperTime(new Date());
        saveFormDTO.setDelflag(null);
        return flowConfigService.saveFlowConfigForm(saveFormDTO);
    }

    @RequestMapping(value = "del/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Object del(@PathVariable(name = "id") Long id, HttpServletRequest request) {
        if( flowConfigService.delFlowConfig(id, request.getSession().getAttribute("user").toString()) ){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS).setMessage("删除成功");
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("删除失败");
    }

    @RequestMapping(value = "selectOptionList")
    @ResponseBody
    public Object selectOptionList() {
        LambdaQueryWrapper<FlowConfig> query = new LambdaQueryWrapper<>();
        query.eq(FlowConfig::getDelflag, false);
        List<FlowConfig> clList = flowConfigService.list(query);
        List<ComboBoxOptionDTO> dtoList = new ArrayList<>(clList.size());
        if(CollUtil.isNotEmpty(clList)){
            for(FlowConfig flowConfig : clList){
                ComboBoxOptionDTO dto = new ComboBoxOptionDTO();
                dto.setId(flowConfig.getId().toString());
                dto.setName(flowConfig.getName());
                dto.setText(flowConfig.getName());
                dto.setDesc(flowConfig.getName());
                dtoList.add(dto);
            }
        }
        return dtoList;
    }

}
