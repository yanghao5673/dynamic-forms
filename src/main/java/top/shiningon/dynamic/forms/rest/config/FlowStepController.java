package top.shiningon.dynamic.forms.rest.config;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dto.config.FlowStepFormDTO;
import top.shiningon.dynamic.forms.service.FlowStepService;

@Api(tags = "流程步骤-更新操作")
@ApiSupport(order = 2)
@Controller("top.shiningon.dynamic.forms.rest.bill.flow.FlowStepController")
@RequestMapping(value = "/config/flow/step")
public class FlowStepController {

    @Autowired
    FlowStepService flowStepService;
    
    /**
     * 更新单个流程步骤
     */
    @ApiOperation(value = "新增、更新单个流程步骤", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 1)
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam update(@RequestBody @Validated FlowStepFormDTO formDTO) {
        return flowStepService.saveFlowStepForm(formDTO);
    }

    /**
     * 删除单个流程步骤
     */
    @ApiOperation(value = "删除单个流程步骤", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 2)
    @RequestMapping(value = "del/{id}/{operId}", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam del(@PathVariable(name = "id") Long id, @PathVariable(name = "operId") String operId) {
        if(flowStepService.delFlowStep(id,  operId)){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR);
    }

}
