package top.shiningon.dynamic.forms.rest.admin;

import cn.hutool.core.util.StrUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.entity.SysConfig;
import top.shiningon.dynamic.forms.service.SysConfigService;

import javax.servlet.http.HttpServletRequest;

@Controller("top.shiningon.dynamic.forms.rest.admin")
@RequestMapping(value = "/admin")
public class IndexController {

    private static final String LOGIN_PAGE = "admin/login";
    private static final String INDEX_PAGE = "admin/index";

    @Autowired
    SysConfigService sysConfigService;

    @RequestMapping(value = {"", "index", "login"}, method = RequestMethod.GET)
    public String index(HttpServletRequest request) {
        Object obj = request.getSession().getAttribute("user");
        if(null != obj && StrUtil.isNotBlank(obj.toString())) {
            return IndexController.INDEX_PAGE;
        }else{
            return IndexController.LOGIN_PAGE;
        }
    }

    /**
     * 登录
     */
    @RequestMapping(value = "login", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam login(String username, String password, HttpServletRequest request) {
        request.getSession().removeAttribute("user");
        SysConfig sysConfig = sysConfigService.getSysConfig();
        if(null == sysConfig){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("系统未设置登录功能");
        }
        if(sysConfig.getLoginAble()){
            if(username.equals(sysConfig.getLoginName()) && password.equals(sysConfig.getLoginPassword())){
                request.getSession().setAttribute("user", username);
                return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS).setMessage("登录成功");
            }
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("账号密码错误");
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("登录功能已关闭");
    }

    /**
     * 登出
     */
    @RequestMapping(value = "logout")
    public String logout(HttpServletRequest request) {
        request.getSession().removeAttribute("user");
        return IndexController.LOGIN_PAGE;
    }

}
