package top.shiningon.dynamic.forms.rest.admin;

import cn.hutool.core.bean.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.constant.InputTypeEnum;
import top.shiningon.dynamic.forms.dto.admin.BillFieldDTO;
import top.shiningon.dynamic.forms.dto.admin.BillFieldFormDTO;
import top.shiningon.dynamic.forms.dto.config.BillFormFieldConfigFormDTO;
import top.shiningon.dynamic.forms.entity.BillConfig;
import top.shiningon.dynamic.forms.entity.BillFormFieldConfig;
import top.shiningon.dynamic.forms.service.BillConfigService;
import top.shiningon.dynamic.forms.service.BillFormFieldConfigService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller("top.shiningon.dynamic.forms.rest.admin.BillFieldController")
@RequestMapping(value = "/admin/bill/field")
public class BillFieldController {

    @Autowired
    BillConfigService billConfigService;
    @Autowired
    BillFormFieldConfigService billFormFieldConfigService;

    @RequestMapping(value = "index/{billConfigId}", method = RequestMethod.GET)
    public String index(@PathVariable(name = "billConfigId") Long billConfigId, Model model) {
        model.addAttribute("billConfigId", billConfigId);
        BillConfig billConfig = billConfigService.searchBillConfigSingle(billConfigId);
        model.addAttribute("billConfigTitle", billConfig.getTitle());
        model.addAttribute("billConfigSubTitle", billConfig.getSubTitle());
        return "admin/bill/field/index";
    }
    
    /**
     * 查找单据配置列表
     */
    @RequestMapping(value = "list/{billConfigId}", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam<List<BillFieldDTO>> list(@PathVariable(name = "billConfigId") Long billConfigId) {
        SearchResoultDTO<BillFormFieldConfig> fieldConfigSearchList = billFormFieldConfigService.searchBillFormFieldConfigList(billConfigId);
        if(!CollectionUtils.isEmpty(fieldConfigSearchList.getList())) {
            List<BillFieldDTO> dtoList = new ArrayList<>(fieldConfigSearchList.getList().size());
            List<BillConfig> billConfigList = billConfigService.listByIds(fieldConfigSearchList.getList().stream().map(dto -> dto.getBillConfigId()).collect(Collectors.toList()));
            for(BillFormFieldConfig fieldConfig : fieldConfigSearchList.getList()){
                BillFieldDTO dto = new BillFieldDTO();
                BeanUtil.copyProperties(fieldConfig, dto);
                Optional<BillConfig> opt = billConfigList.stream().filter(obj -> obj.getId().equals(fieldConfig.getBillConfigId())).findAny();
                opt.ifPresent(billConfig -> dto.setBillConfigName(billConfig.getTitle()));
                dto.setInputTypeCode(fieldConfig.getInputType().getKey());
                dtoList.add(dto);
            }
            return new ReturnParam<List<BillFieldDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(dtoList);
        }
        return new ReturnParam<List<BillFieldDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setMessage("没有记录").setContent(new ArrayList<>(0));
    }

    @RequestMapping(value = "add/{billConfigId}", method = RequestMethod.GET)
    public String add(@PathVariable(name = "billConfigId") Long billConfigId, Model model) {
        BillFieldDTO dto = new BillFieldDTO();
        BillConfig billConfig = billConfigService.searchBillConfigSingle(billConfigId);
        dto.setBillConfigId(billConfig.getId());
        dto.setBillConfigName(billConfig.getTitle());
        model.addAttribute("entity", dto);
        model.addAttribute("inputTypeEnum", InputTypeEnum.values());
        return "admin/bill/field/addOrEdit";
    }

    @RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id") Long id, Model model) {
        BillFormFieldConfig billFieldConfig = this.billFormFieldConfigService.searchBillFormFieldConfigSingle(id);
        BillFieldDTO dto = new BillFieldDTO();
        BeanUtil.copyProperties(billFieldConfig, dto);
        BillConfig billConfig = billConfigService.searchBillConfigSingle(billFieldConfig.getBillConfigId());
        if(null != billConfig) {
            dto.setBillConfigName(billConfig.getTitle());
        }
        dto.setInputTypeCode(billFieldConfig.getInputType().getKey());
        model.addAttribute("entity", dto);
        model.addAttribute("inputTypeEnum", InputTypeEnum.values());
        return "admin/bill/field/addOrEdit";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public Object save(BillFieldFormDTO formDTO, HttpServletRequest request) {
        BillFormFieldConfigFormDTO saveFormDTO = new BillFormFieldConfigFormDTO();
        saveFormDTO.setId(formDTO.getId());
        saveFormDTO.setBillConfigId(formDTO.getBillConfigId());
        saveFormDTO.setInputName(formDTO.getInputName());
        saveFormDTO.setInputType(formDTO.getInputType());
        saveFormDTO.setVaildType(formDTO.getVaildType());
        saveFormDTO.setIndex(formDTO.getIndex());
        saveFormDTO.setVaildMax(formDTO.getVaildMax());
        saveFormDTO.setVaildMin(formDTO.getVaildMin());
        saveFormDTO.setIsRequired(formDTO.getIsRequired());
        saveFormDTO.setPlaceholder(formDTO.getPlaceholder());
        saveFormDTO.setDefVal(formDTO.getDefVal());
        saveFormDTO.setOperBy( request.getSession().getAttribute("user").toString() );
        saveFormDTO.setOperTime(new Date());
        saveFormDTO.setDelflag(null);
        return billFormFieldConfigService.saveBillFormFieldConfig(saveFormDTO);
    }

    @RequestMapping(value = "del/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam del(@PathVariable(name = "id") Long id, HttpServletRequest request) {
        if( billFormFieldConfigService.delBillFormFieldConfig(id, request.getSession().getAttribute("user").toString()) ){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS).setMessage("删除成功");
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("删除失败");
    }

}
