package top.shiningon.dynamic.forms.rest.process;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.log4j.Log4j2;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;
import top.shiningon.dynamic.forms.base.MyBizErrorException;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;
import top.shiningon.dynamic.forms.dto.process.bill.request.BillReplySearchListDTO;
import top.shiningon.dynamic.forms.dto.process.bill.request.StartBillFormDTO;
import top.shiningon.dynamic.forms.dto.process.bill.request.SubmitBillFormDTO;
import top.shiningon.dynamic.forms.dto.process.bill.request.SubmitReplyFormDTO;
import top.shiningon.dynamic.forms.dto.process.bill.response.BillListDTO;
import top.shiningon.dynamic.forms.dto.process.bill.response.BillReplyReadyFormDTO;
import top.shiningon.dynamic.forms.dto.process.bill.response.DetailBillFormDTO;
import top.shiningon.dynamic.forms.dto.process.bill.response.detail.BillDetailBillListDTO;
import top.shiningon.dynamic.forms.dto.process.bill.response.detail.BillDetailDetailDTO;
import top.shiningon.dynamic.forms.dto.process.bill.response.detail.BillDetailReplyDTO;
import top.shiningon.dynamic.forms.dto.process.bill.response.detail.BillDetailReplyReadyDTO;
import top.shiningon.dynamic.forms.dto.request.BillDetailSearchListDTO;
import top.shiningon.dynamic.forms.dto.request.BillListSearchListDTO;
import top.shiningon.dynamic.forms.dto.request.BillReplyReadySearchListDTO;
import top.shiningon.dynamic.forms.dto.request.BillReplyReadyUserSearchListDTO;
import top.shiningon.dynamic.forms.entity.*;
import top.shiningon.dynamic.forms.service.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Log4j2
@Api(tags = "单据处理")
@ApiSupport(order = 7)
@Controller("top.shiningon.dynamic.forms.rest.pub.BillController")
@RequestMapping(value = "/process/bill")
public class BillController {

    @Autowired
    BillConfigService billConfigService;
    @Autowired
    FlowConfigService flowConfigService;
    @Autowired
    FlowStepService flowStepService;
    @Autowired
    BillListService billListService;
    @Autowired
    BillDetailService billDetailService;
    @Autowired
    BillFormFieldConfigService billFormFieldConfigService;
    @Autowired
    BillFormFieldSubConfigService billFormFieldSubConfigService;
    @Autowired
    BillReplyService billReplyService;
    @Autowired
    BillReplyReadyService billReplyReadyService;
    @Autowired
    BillReplyReadyUserService billReplyReadyUserService;
    @Autowired
    MemberService memberService;

    /**
     * 保存并创建新单据
     */
    @ApiOperation(value = "保存并创建新单据", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 1)
    @RequestMapping(value = "createBill", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam<Long> createBill(@RequestBody SubmitBillFormDTO formDTO) {
        try {
            return billListService.createBill(formDTO);
        }catch (MyBizErrorException bizErrorException){
            log.error(bizErrorException.getReturnParam().getMsg(), bizErrorException);
            return bizErrorException.getReturnParam();
        }
    }

    /**
     * 用户提交表单，进入审核流程
     */
    @ApiOperation(value = "用户提交表单，进入审核流程", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 2)
    @RequestMapping(value = "startBill", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam startBill(@RequestBody StartBillFormDTO formDTO) {
        try {
            return billListService.startBill(formDTO);
        }catch (MyBizErrorException bizErrorException){
            log.error(bizErrorException.getReturnParam().getMsg(), bizErrorException);
            return bizErrorException.getReturnParam();
        }
    }

    /**
     * 用户查询审核表单列表（我的单据）
     */
    @ApiOperation(value = "用户查询审核表单列表（我的单据）", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 3)
    @RequestMapping(value = "searchMyBill/{operId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<List<BillListDTO>> searchMyBill(@PathVariable(name = "operId") String operId, PageFormDTO formDTO) {
        BillListSearchListDTO searchDTO = new BillListSearchListDTO();
        searchDTO.setPageNum(formDTO.getPageNum());
        searchDTO.setPageSize(formDTO.getPageSize());
        searchDTO.setCreateBy(operId);
        SearchResoultDTO<BillList> resDTO = billListService.searchBillListList(searchDTO);
        if(!CollectionUtils.isEmpty(resDTO.getList())) {
            List<BillConfig> configList = billConfigService.listByIds(resDTO.getList().stream().map(BillList::getBillConfigId).collect(Collectors.toList()));
            List<FlowStep> stepList = flowStepService.listByIds(resDTO.getList().stream().map(BillList::getNowFlowStepId).collect(Collectors.toList()));
            List<BillListDTO> dtoList = new ArrayList<>(resDTO.getList().size());
            for(BillList bill : resDTO.getList()) {
                BillListDTO dto = new BillListDTO();
                BeanUtil.copyProperties(bill, dto);
                dto.setCreateMember(memberService.getById(dto.getCreateBy()));
                dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
                Optional<BillConfig> configOpt = configList.stream().filter(obj -> obj.getId().equals(bill.getBillConfigId())).findAny();
                configOpt.ifPresent(billConfig -> dto.setBillConfigName(billConfig.getTitle()));
                Optional<FlowStep> stepOpt = stepList.stream().filter(obj -> obj.getId().equals(bill.getNowFlowStepId())).findAny();
                stepOpt.ifPresent(flowStep -> dto.setNowFlowStepName(flowStep.getName()));
                dto.setStatusCode(bill.getStatus().getKey());
                dtoList.add(dto);
            }
            return new ReturnParam<List<BillListDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(dtoList);
        }
        return new ReturnParam<List<BillListDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setMessage("没有记录").setContent(new ArrayList<>(0));
    }

    /**
     * 用户查询审核表单列表（待审核）
     */
    @ApiOperation(value = "用户查询审核表单列表（待审核）", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 4)
    @RequestMapping(value = "searchReply/{operId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<SearchResoultDTO<BillReplyReadyFormDTO>> searchReply(@PathVariable(name = "operId") String operId, PageFormDTO formDTO) {
        SearchResoultDTO<BillReplyReadyFormDTO> resoultDTO = new SearchResoultDTO<>();
        SearchResoultDTO<BillReplyReady> replyReadyListDTO = billReplyReadyService.searchUserReplyReadyList(operId, formDTO, true);
        //封装DTO
        if(CollUtil.isNotEmpty(replyReadyListDTO.getList())){
            resoultDTO.setPage(replyReadyListDTO.isPage());
            resoultDTO.setPageNum(replyReadyListDTO.getPageNum());
            resoultDTO.setPageSize(replyReadyListDTO.getPageSize());
            resoultDTO.setRowCount(replyReadyListDTO.getRowCount());
            resoultDTO.setList(new ArrayList<>());
            List<BillList> billList = billListService.listByIds(replyReadyListDTO.getList().stream().map(BillReplyReady::getBillListId).collect(Collectors.toList()));
            List<FlowStep> stepList = flowStepService.listByIds(replyReadyListDTO.getList().stream().map(BillReplyReady::getFlowStepId).collect(Collectors.toList()));
            for(BillReplyReady ready : replyReadyListDTO.getList()){
                BillReplyReadyFormDTO dto = new BillReplyReadyFormDTO();
                BeanUtil.copyProperties(ready, dto);
                dto.setCreateMember(memberService.getById(dto.getCreateBy()));
                dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
                Optional<BillList> billOpt = billList.stream().filter(obj -> obj.getId().equals(ready.getBillListId())).findAny();
                if(billOpt.isPresent()) {
                    dto.setBillTitle(billOpt.get().getTitle());
                    dto.setBillSubTitle(billOpt.get().getSubTitle());
                    dto.setCreateBy(billOpt.get().getCreateBy());
                    dto.setCreateTime(billOpt.get().getCreateTime());
                }
                Optional<FlowStep> stepOpt = stepList.stream().filter(obj -> obj.getId().equals(ready.getFlowStepId())).findAny();
                stepOpt.ifPresent(flowStep -> dto.setFlowStepName(flowStep.getName()));
                resoultDTO.getList().add(dto);
            }
        }
        return new ReturnParam<SearchResoultDTO<BillReplyReadyFormDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(resoultDTO);
    }

    /**
     * 用户查询审核表单列表（已审核）
     */
    @ApiOperation(value = "用户查询审核表单列表（已审核）", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 5)
    @RequestMapping(value = "searchReplyHis/{operId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<SearchResoultDTO<BillListDTO>> searchReplyHis(@PathVariable(name = "operId") String operId, BillReplySearchListDTO formDTO) {
        if(StrUtil.isBlank(formDTO.getCreateBy())) {
            formDTO.setCreateBy(operId);
        }
        SearchResoultDTO<BillListDTO> resoultDTO = new SearchResoultDTO<>();
        List<Long> billListId = billReplyService.searchUserReplyBillListId(operId, new PageFormDTO(formDTO.getPageNum(), formDTO.getPageSize()));
        if(CollUtil.isNotEmpty(billListId)) {
            resoultDTO.setList(new ArrayList<>(billListId.size()));
            List<BillList> billLst = this.billListService.listByIds(billListId);
            List<BillConfig> configList = billConfigService.listByIds(billLst.stream().map(BillList::getBillConfigId).collect(Collectors.toList()));
            List<FlowStep> stepList = flowStepService.listByIds(billLst.stream().map(BillList::getNowFlowStepId).collect(Collectors.toList()));
            for(BillList bill : billLst){
                BillListDTO dto = BeanUtil.copyProperties(bill, BillListDTO.class);
                dto.setCreateMember(memberService.getById(dto.getCreateBy()));
                dto.setUpdateMember(memberService.getById(dto.getUpdateBy()));
                Optional<BillConfig> configOpt = configList.stream().filter(obj -> obj.getId().equals(bill.getBillConfigId())).findAny();
                configOpt.ifPresent(billConfig -> dto.setBillConfigName(billConfig.getTitle()));
                Optional<FlowStep> stepOpt = stepList.stream().filter(obj -> obj.getId().equals(bill.getNowFlowStepId())).findAny();
                stepOpt.ifPresent(flowStep -> dto.setNowFlowStepName(flowStep.getName()));
                dto.setStatusCode(bill.getStatus().getKey());
                resoultDTO.getList().add(dto);
            }
        }else {
            resoultDTO.putResoult(new ArrayList<>(0));
        }
        return new ReturnParam<SearchResoultDTO<BillListDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setMessage("无记录").setContent(resoultDTO);
    }

    /**
     * 用户审核表单，查询展示表单详细信息
     */
    @ApiOperation(value = "用户审核表单，展示表单详细信息", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 6)
    @RequestMapping(value = "beginReply/{billId}/{operId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<DetailBillFormDTO> beginReply(@PathVariable(name = "billId") Long billId, @PathVariable(name = "operId") String operId) {
        //确认是否是用户审核，或者历史审核的。
        //查replyReady
        if(this.billReplyReadyService.searchUserIsToReplyByBill(billId, operId)){
            //获取单据信息
            return this.detailBill(billId);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("暂无权限查看");
    }

    /**
     * 用户提交审核信息
     */
    @ApiOperation(value = "用户提交审核信息", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 7)
    @RequestMapping(value = "reply", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam reply(@RequestBody SubmitReplyFormDTO formDTO) {
        try {
            return billListService.replyBill(formDTO);
        }catch (MyBizErrorException bizErrorException){
            log.error(bizErrorException.getReturnParam().getMsg(), bizErrorException);
            return bizErrorException.getReturnParam();
        }
    }

    /**
     * 查看单据当前信息
     */
    @ApiOperation(value = "查看单据当前信息", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 8)
    @RequestMapping(value = "detailBill/{billListId}", method = RequestMethod.GET)
    @ResponseBody
    public ReturnParam<DetailBillFormDTO> detailBill(@PathVariable(name = "billListId") Long billListId) {
        DetailBillFormDTO detailDTO = new DetailBillFormDTO();
        //单据主体
        BillList bill = billListService.searchBillListSingle(billListId);
        if(null == bill){
            return new ReturnParam<DetailBillFormDTO>(ErrorCodeEnum.GLOBAL_SUCCESS).setMessage("单据未找到");
        }
        BillDetailBillListDTO billDTO = new BillDetailBillListDTO();
        BeanUtil.copyProperties(bill, billDTO);
        billDTO.setCreateMember(memberService.getById(billDTO.getCreateBy()));
        FlowStep billFlowStep = this.flowStepService.searchFlowStepSingle(bill.getNowFlowStepId());
        if(null != billFlowStep) {
            billDTO.setNowFlowStepName(billFlowStep.getName());
        }
        detailDTO.setBill(billDTO);

        //单据表单
        BillDetailSearchListDTO detailSearchDTO = new BillDetailSearchListDTO();
        detailSearchDTO.setBillListIdList(Arrays.asList(billListId));
        SearchResoultDTO<BillDetail> detailSearchList = billDetailService.searchBillDetailList(detailSearchDTO);
        if(CollUtil.isNotEmpty(detailSearchList.getList())){
            List<BillDetailDetailDTO> formInputListDTO = new ArrayList<>(detailSearchList.getList().size());
            for(BillDetail detail : detailSearchList.getList()) {
                BillDetailDetailDTO formInputDTO = new BillDetailDetailDTO();
                BeanUtil.copyProperties(detail, formInputDTO);
                formInputListDTO.add(formInputDTO);
            }
            detailDTO.setFormInputList(formInputListDTO);
        }

        //单据已审核流程
        BillReplySearchListDTO replySearchDTO = new BillReplySearchListDTO();
        replySearchDTO.setBillListId(billListId);
        SearchResoultDTO<BillReply> replySearchList = billReplyService.searchBillReplyList(replySearchDTO);
        if(CollUtil.isNotEmpty(detailSearchList.getList())){
            List<Long> flowStepIdList = replySearchList.getList().stream().map(BillReply::getFlowStepId).collect(Collectors.toList());
            List<FlowStep> stepList;
            if(CollUtil.isNotEmpty(flowStepIdList)){
                stepList = this.flowStepService.listByIds(flowStepIdList);
            }else{
                stepList = new ArrayList<>(0);
            }
            List<BillDetailReplyDTO> replyListDTO = new ArrayList<>(replySearchList.getList().size());
            for(BillReply reply : replySearchList.getList()) {
                BillDetailReplyDTO replyDTO = new BillDetailReplyDTO();
                BeanUtil.copyProperties(reply, replyDTO);
                replyDTO.setCreateMember(memberService.getById(replyDTO.getCreateBy()));
                Optional<FlowStep> opt = stepList.stream().filter(obj -> {return obj.getId().equals(reply.getFlowStepId());}).findAny();
                if(opt.isPresent()) {
                    replyDTO.setFlowStepName(opt.get().getName());
                }
                replyListDTO.add(replyDTO);
            }
            detailDTO.setReplyList(replyListDTO);
        }

        //单据所有审核流程
        BillReplyReadySearchListDTO replyReadySearchDTO = new BillReplyReadySearchListDTO();
        replyReadySearchDTO.setBillListId(billListId);
        SearchResoultDTO<BillReplyReady> replyReadySearchList = billReplyReadyService.searchBillReplyReadyList(replyReadySearchDTO);
        if(CollUtil.isNotEmpty(replyReadySearchList.getList())) {
            List<BillReplyReady> replyReadyList = new ArrayList<>();
            List<Long> flowStepIdList = new ArrayList<>();
            for(BillReplyReady brr : replyReadySearchList.getList()){
                if(!flowStepIdList.contains(brr.getFlowStepId())) {
                    replyReadyList.add(brr);
                    flowStepIdList.add(brr.getFlowStepId());
                }
            }
            flowStepIdList.clear();
            //单据所有审核流程对应的人
            BillReplyReadyUserSearchListDTO readyUserSearchDTO = new BillReplyReadyUserSearchListDTO();
            readyUserSearchDTO.setReplyReadyIdList(replyReadyList.stream().map(BillReplyReady::getId).collect(Collectors.toList()));
            SearchResoultDTO<BillReplyReadyUser> replyReadyUserSearchList = billReplyReadyUserService.searchBillReplyReadyUserList(readyUserSearchDTO);
            boolean hasReadyUser = replyReadyUserSearchList.getList().size() > 0;

            List<FlowStep> stepList = this.flowStepService.listByIds(replyReadyList.stream().map(BillReplyReady::getFlowStepId).collect(Collectors.toList()));
            List<BillDetailReplyReadyDTO> replyReadyListDTO = new ArrayList<>(replyReadyList.size());
            for(BillReplyReady replyReady : replyReadyList) {
                BillDetailReplyReadyDTO replyDTO = new BillDetailReplyReadyDTO();
                BeanUtil.copyProperties(replyReady, replyDTO);
                Optional<FlowStep> stepOpt = stepList.stream().filter(obj -> {return obj.getId().equals(replyReady.getFlowStepId());}).findAny();
                if(stepOpt.isPresent()) {
                    replyDTO.setFlowStepName(stepOpt.get().getName());
                }
                if(hasReadyUser) {
                    replyDTO.setUserList(replyReadyUserSearchList.getList().stream()
                            .filter(obj -> {return obj.getReplyReadyId().equals(replyReady.getId());})
                            .map(BillReplyReadyUser::getUserId).collect(Collectors.toList()));
                }
                replyReadyListDTO.add(replyDTO);
            }
            detailDTO.setReplyReadyList(replyReadyListDTO);
        }
        return new ReturnParam<DetailBillFormDTO>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(detailDTO);
    }

}
