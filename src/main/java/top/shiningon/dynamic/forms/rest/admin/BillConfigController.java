package top.shiningon.dynamic.forms.rest.admin;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dto.admin.BillConfigDTO;
import top.shiningon.dynamic.forms.dto.admin.BillConfigSearchDTO;
import top.shiningon.dynamic.forms.dto.admin.base.ComboBoxOptionDTO;
import top.shiningon.dynamic.forms.dto.config.BillConfigFormDTO;
import top.shiningon.dynamic.forms.dto.process.base.request.BillConfigSearchListDTO;
import top.shiningon.dynamic.forms.entity.BillConfig;
import top.shiningon.dynamic.forms.entity.FlowConfig;
import top.shiningon.dynamic.forms.service.BillConfigService;
import top.shiningon.dynamic.forms.service.FlowConfigService;

import javax.servlet.http.HttpServletRequest;
import java.util.*;
import java.util.stream.Collectors;

@Controller("top.shiningon.dynamic.forms.rest.admin.BillConfigController")
@RequestMapping(value = "/admin/bill/config")
public class BillConfigController {

    @Autowired
    BillConfigService billConfigService;
    @Autowired
    FlowConfigService flowConfigService;

    @RequestMapping(value = "index", method = RequestMethod.GET)
    public String index(Model model) {
        return "admin/bill/config/index";
    }
    
    /**
     * 查找单据配置列表
     */
    @RequestMapping(value = "list", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam<List<BillConfigDTO>> list(@Validated BillConfigSearchDTO formDTO) {
        BillConfigSearchListDTO searchDTO = new BillConfigSearchListDTO();
        if(null != formDTO.getFlowConfigId()) {
            searchDTO.setWorkFlowIdList(Collections.singletonList(formDTO.getFlowConfigId()));
        }
        searchDTO.setCode(formDTO.getCode());
        searchDTO.setTitle(formDTO.getTitle());
        SearchResoultDTO<BillConfig> resDTO = billConfigService.searchBillConfigList(searchDTO);
        if(!CollectionUtils.isEmpty(resDTO.getList())) {
            List<BillConfigDTO> dtoList = new ArrayList<>(resDTO.getList().size());
            List<FlowConfig> fcList = flowConfigService.listByIds(resDTO.getList().stream().map(BillConfig::getFlowConfigId).collect(Collectors.toList()));
            for(BillConfig billConfig : resDTO.getList()){
                BillConfigDTO dto = new BillConfigDTO();
                BeanUtil.copyProperties(billConfig, dto);
                Optional<FlowConfig> opt = fcList.stream().filter(obj -> obj.getId().equals(billConfig.getFlowConfigId())).findAny();
                opt.ifPresent(flowConfig -> dto.setFlowConfigName(flowConfig.getName()));
                dtoList.add(dto);
            }
            return new ReturnParam<List<BillConfigDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(dtoList);
        }
        return new ReturnParam<List<BillConfigDTO>>(ErrorCodeEnum.GLOBAL_ERROR).setMessage("没有记录").setContent(new ArrayList<>(0));
    }

    @RequestMapping(value = "add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("entity", new BillConfigDTO());
        return "admin/bill/config/addOrEdit";
    }

    @RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id") Long id, Model model) {
        BillConfig billConfig = this.billConfigService.searchBillConfigSingle(id);
        BillConfigDTO dto = new BillConfigDTO();
        BeanUtil.copyProperties(billConfig, dto);
        FlowConfig flowConfig = flowConfigService.searchFlowConfigSingle(billConfig.getFlowConfigId());
        dto.setFlowConfigName(flowConfig.getName());
        model.addAttribute("entity", dto);
        return "admin/bill/config/addOrEdit";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam save(BillConfigFormDTO formDTO, HttpServletRequest request) {
        BillConfigFormDTO saveFormDTO = new BillConfigFormDTO();
        saveFormDTO.setId(formDTO.getId());
        saveFormDTO.setFlowConfigId(formDTO.getFlowConfigId());
        saveFormDTO.setCode(formDTO.getCode());
        saveFormDTO.setTitle(formDTO.getTitle());
        saveFormDTO.setSubTitle(formDTO.getSubTitle());
        saveFormDTO.setOperBy( request.getSession().getAttribute("user").toString() );
        saveFormDTO.setOperTime(new Date());
        saveFormDTO.setDelflag(null);
        return billConfigService.saveBillConfigForm(saveFormDTO);
    }

    @RequestMapping(value = "del/{id}", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam del(@PathVariable(name = "id") Long id, HttpServletRequest request) {
        if( billConfigService.delBillConfig(id, request.getSession().getAttribute("user").toString()) ){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS).setMessage("删除成功");
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("删除失败");
    }

    @RequestMapping(value = "selectOptionList")
    @ResponseBody
    public List<ComboBoxOptionDTO> selectOptionList() {
        LambdaQueryWrapper<BillConfig> query = new LambdaQueryWrapper<>();
        query.eq(BillConfig::getDelflag, false);
        List<BillConfig> clList = billConfigService.list(query);
        List<ComboBoxOptionDTO> dtoList = new ArrayList<>(clList.size());
        if(CollUtil.isNotEmpty(clList)){
            for(BillConfig billConfig : clList){
                ComboBoxOptionDTO dto = new ComboBoxOptionDTO();
                dto.setId(billConfig.getId().toString());
                dto.setName(billConfig.getTitle());
                dto.setText(billConfig.getTitle());
                dto.setDesc(billConfig.getCode());
                dtoList.add(dto);
            }
        }
        return dtoList;
    }

}
