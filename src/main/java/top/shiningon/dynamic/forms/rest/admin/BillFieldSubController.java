package top.shiningon.dynamic.forms.rest.admin;

import cn.hutool.core.bean.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dto.admin.BillFieldSubDTO;
import top.shiningon.dynamic.forms.dto.admin.BillFieldSubFormDTO;
import top.shiningon.dynamic.forms.dto.config.BillFormFieldSubConfigFormDTO;
import top.shiningon.dynamic.forms.entity.BillFormFieldConfig;
import top.shiningon.dynamic.forms.entity.BillFormFieldSubConfig;
import top.shiningon.dynamic.forms.service.BillFormFieldConfigService;
import top.shiningon.dynamic.forms.service.BillFormFieldSubConfigService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Controller("top.shiningon.dynamic.forms.rest.admin.BillFieldSubController")
@RequestMapping(value = "/admin/bill/field/sub")
public class BillFieldSubController {

    @Autowired
    BillFormFieldConfigService billFormFieldConfigService;
    @Autowired
    BillFormFieldSubConfigService billFormFieldSubConfigService;

    @RequestMapping(value = "index/{billFieldId}", method = RequestMethod.GET)
    public String index(@PathVariable(name = "billFieldId") Long billFieldId, Model model) {
        model.addAttribute("billFieldId", billFieldId);
        BillFormFieldConfig billField = billFormFieldConfigService.searchBillFormFieldConfigSingle(billFieldId);
        model.addAttribute("billFieldInputName", billField.getInputName());
        model.addAttribute("billFieldInputType", billField.getInputType());
        return "admin/bill/field/sub/index";
    }
    
    /**
     * 查找单据配置列表
     */
    @RequestMapping(value = "list/{billFieldId}", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam<List<BillFieldSubDTO>> list(@PathVariable(name = "billFieldId") Long billFieldId) {
        List<BillFormFieldSubConfig> fieldSubConfigList = billFormFieldSubConfigService.searchBillFormFieldSubConfigList(billFieldId);
        if(!CollectionUtils.isEmpty(fieldSubConfigList)) {
            List<BillFieldSubDTO> dtoList = new ArrayList<>(fieldSubConfigList.size());
            List<BillFormFieldConfig> fieldConfigList = billFormFieldConfigService.listByIds(fieldSubConfigList.stream().map(BillFormFieldSubConfig::getFormFieldId).collect(Collectors.toList()));
            for(BillFormFieldSubConfig subConfig : fieldSubConfigList){
                BillFieldSubDTO dto = new BillFieldSubDTO();
                BeanUtil.copyProperties(subConfig, dto);
                Optional<BillFormFieldConfig> opt = fieldConfigList.stream().filter(obj -> obj.getId().equals(subConfig.getFormFieldId())).findAny();
                opt.ifPresent(billFormFieldConfig -> dto.setFormFieldName(billFormFieldConfig.getInputName()));
                dtoList.add(dto);
            }
            return new ReturnParam<List<BillFieldSubDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(dtoList);
        }
        return new ReturnParam<List<BillFieldSubDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setMessage("没有记录").setContent(new ArrayList<>(0));
    }

    @RequestMapping(value = "add/{billFieldId}", method = RequestMethod.GET)
    public String add(@PathVariable(name = "billFieldId") Long billFieldId, Model model) {
        BillFieldSubDTO dto = new BillFieldSubDTO();
        BillFormFieldConfig fieldConfig = billFormFieldConfigService.searchBillFormFieldConfigSingle(billFieldId);
        dto.setFormFieldId(fieldConfig.getId());
        dto.setFormFieldName(fieldConfig.getInputName());
        model.addAttribute("entity", dto);
        return "admin/bill/field/sub/addOrEdit";
    }

    @RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id") Long id, Model model) {
        BillFormFieldSubConfig fieldSubConfig = this.billFormFieldSubConfigService.searchBillFormFieldSubConfigSingle(id);
        BillFieldSubDTO dto = new BillFieldSubDTO();
        BeanUtil.copyProperties(fieldSubConfig, dto);
        BillFormFieldConfig fieldConfig = billFormFieldConfigService.searchBillFormFieldConfigSingle(fieldSubConfig.getFormFieldId());
        dto.setFormFieldId(fieldConfig.getId());
        dto.setFormFieldName(fieldConfig.getInputName());
        model.addAttribute("entity", dto);
        return "admin/bill/field/sub/addOrEdit";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public Object save(BillFieldSubFormDTO formDTO, HttpServletRequest request) {
        BillFormFieldSubConfigFormDTO saveFormDTO = new BillFormFieldSubConfigFormDTO();
        saveFormDTO.setId(formDTO.getId());
        saveFormDTO.setFormFieldId(formDTO.getFormFieldId());
        saveFormDTO.setInputVal(formDTO.getInputVal());
        saveFormDTO.setIndex(formDTO.getIndex());
        saveFormDTO.setDefSelect(formDTO.getDefSelect());
        saveFormDTO.setOperBy( request.getSession().getAttribute("user").toString() );
        saveFormDTO.setOperTime(new Date());
        saveFormDTO.setDelflag(null);
        return billFormFieldSubConfigService.saveBillFormFieldSubConfig(saveFormDTO);
    }

    @RequestMapping(value = "del/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Object del(@PathVariable(name = "id") Long id, HttpServletRequest request) {
        if( billFormFieldSubConfigService.delBillFormFieldSubConfig(id, request.getSession().getAttribute("user").toString()) ){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS).setMessage("删除成功");
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("删除失败");
    }

}
