package top.shiningon.dynamic.forms.rest.config;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dto.config.BillFormFieldSubConfigFormDTO;
import top.shiningon.dynamic.forms.service.BillFormFieldSubConfigService;

@Api(tags = "表单字段子项配置-更新操作")
@ApiSupport(order = 5)
@Controller("top.shiningon.dynamic.forms.rest.bill.config.BillFormFieldSubConfigController")
@RequestMapping(value = "/config/bill/form/field/sub/config")
public class BillFormFieldSubConfigController {

    @Autowired
    BillFormFieldSubConfigService billFormFieldSubConfigService;
    
    /**
     * 更新单个表单字段子项配置
     */
    @ApiOperation(value = "新增、更新单个表单字段子项配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 1)
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam update(@RequestBody @Validated BillFormFieldSubConfigFormDTO formDTO) {
        //判断新增还是更新
        return billFormFieldSubConfigService.saveBillFormFieldSubConfig(formDTO);
    }

    /**
     * 删除单个表单字段子项配置
     */
    @ApiOperation(value = "删除单个表单字段子项配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 2)
    @RequestMapping(value = "del/{id}/{operId}", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam del(@PathVariable(name = "id") Long id, @PathVariable(name = "operId") String operId) {
        if(billFormFieldSubConfigService.delBillFormFieldSubConfig(id,  operId)){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR);
    }

}
