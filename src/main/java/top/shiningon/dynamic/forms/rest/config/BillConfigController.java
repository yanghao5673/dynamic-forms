package top.shiningon.dynamic.forms.rest.config;

import com.github.xiaoymin.knife4j.annotations.ApiOperationSupport;
import com.github.xiaoymin.knife4j.annotations.ApiSupport;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dto.config.BillConfigFormDTO;
import top.shiningon.dynamic.forms.service.BillConfigService;

@Api(tags = "单据配置-更新操作")
@ApiSupport(order = 3)
@Controller("top.shiningon.dynamic.forms.rest.bill.config.BillConfigController")
@RequestMapping(value = "/config/bill/config")
public class BillConfigController {

    @Autowired
    BillConfigService billConfigService;

    /**
     * 更新单个单据配置
     */
    @ApiOperation(value = "新增、更新单个单据配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 1)
    @RequestMapping(value = "update", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam update(@RequestBody @Validated BillConfigFormDTO formDTO) {
        //判断新增还是更新
        return billConfigService.saveBillConfigForm(formDTO);
    }

    /**
     * 删除单个单据配置
     */
    @ApiOperation(value = "删除单个单据配置", response = ReturnParam.class)
    @ApiOperationSupport(author = "yanghao", order = 2)
    @RequestMapping(value = "del/{id}/{operId}", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam del(@PathVariable(name = "id") Long id, @PathVariable(name = "operId") String operId) {
        if(billConfigService.delBillConfig(id,  operId)){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR);
    }

}
