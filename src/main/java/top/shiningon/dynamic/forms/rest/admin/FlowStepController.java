package top.shiningon.dynamic.forms.rest.admin;

import cn.hutool.core.bean.BeanUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.constant.FlowStepReplyTypeEnum;
import top.shiningon.dynamic.forms.constant.FlowStepReplyUserTypeEnum;
import top.shiningon.dynamic.forms.dto.admin.FlowStepDTO;
import top.shiningon.dynamic.forms.dto.admin.FlowStepFormDTO;
import top.shiningon.dynamic.forms.entity.FlowConfig;
import top.shiningon.dynamic.forms.entity.FlowStep;
import top.shiningon.dynamic.forms.service.FlowConfigService;
import top.shiningon.dynamic.forms.service.FlowStepService;

import javax.servlet.http.HttpServletRequest;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Controller("top.shiningon.dynamic.forms.rest.admin.FlowStepController")
@RequestMapping(value = "/admin/flow/step")
public class FlowStepController {

    @Autowired
    FlowConfigService flowConfigService;
    @Autowired
    FlowStepService flowStepService;

    @RequestMapping(value = "index/{configId}", method = RequestMethod.GET)
    public String index(@PathVariable(name = "configId") Long configId, Model model) {
        model.addAttribute("configId", configId);
        FlowConfig flowConfig = flowConfigService.searchFlowConfigSingle(configId);
        model.addAttribute("flowConfigName", flowConfig.getName());
        return "admin/flow/step/index";
    }
    
    /**
     * 查找单据配置列表
     */
    @RequestMapping(value = "list/{configId}", method = RequestMethod.POST)
    @ResponseBody
    public ReturnParam<List<FlowStepDTO>> list(@PathVariable(name = "configId") Long configId) {
        List<FlowStep> stepList = flowStepService.searchFlowStepList(configId);
        if(!CollectionUtils.isEmpty(stepList)) {
            List<FlowStepDTO> dtoList = new ArrayList<>(stepList.size());
            for(FlowStep flowStep : stepList){
                FlowStepDTO dto = new FlowStepDTO();
                BeanUtil.copyProperties(flowStep, dto);
                dtoList.add(dto);
            }
            return new ReturnParam<List<FlowStepDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(dtoList);
        }
        return new ReturnParam<List<FlowStepDTO>>(ErrorCodeEnum.GLOBAL_SUCCESS).setMessage("没有记录").setContent(new ArrayList<>(0));
    }

    @RequestMapping(value = "add/{configId}", method = RequestMethod.GET)
    public String add(@PathVariable(name = "configId") Long configId, Model model) {
        FlowStepDTO dto = new FlowStepDTO();
        FlowConfig flowConfig = flowConfigService.searchFlowConfigSingle(configId);
        dto.setFlowConfigId(flowConfig.getId());
        dto.setFlowConfigName(flowConfig.getName());
        model.addAttribute("entity", dto);
        model.addAttribute("flowStepReplyTypeEnum", FlowStepReplyTypeEnum.values());
        model.addAttribute("flowStepReplyUserTypeEnum", FlowStepReplyUserTypeEnum.values());
        return "admin/flow/step/addOrEdit";
    }

    @RequestMapping(value = "edit/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable(name = "id") Long id, Model model) {
        FlowStep flowStep = this.flowStepService.searchFlowStepSingle(id);
        FlowStepDTO dto = new FlowStepDTO();
        BeanUtil.copyProperties(flowStep, dto);
        FlowConfig config = flowConfigService.searchFlowConfigSingle(flowStep.getFlowConfigId());
        if(null != config) {
            dto.setFlowConfigName(config.getName());
        }
        model.addAttribute("entity", dto);
        model.addAttribute("flowStepReplyTypeEnum", FlowStepReplyTypeEnum.values());
        model.addAttribute("flowStepReplyUserTypeEnum", FlowStepReplyUserTypeEnum.values());
        return "admin/flow/step/addOrEdit";
    }

    @RequestMapping(value = "save", method = RequestMethod.POST)
    @ResponseBody
    public Object save(FlowStepFormDTO formDTO, HttpServletRequest request) {
        top.shiningon.dynamic.forms.dto.config.FlowStepFormDTO saveFormDTO = new top.shiningon.dynamic.forms.dto.config.FlowStepFormDTO();
        saveFormDTO.setId(formDTO.getId());
        saveFormDTO.setFlowConfigId(formDTO.getFlowConfigId());
        saveFormDTO.setName(formDTO.getName());
        saveFormDTO.setIndex(formDTO.getIndex());
        saveFormDTO.setReplyType(formDTO.getReplyType());
        saveFormDTO.setReplyUserType(formDTO.getReplyUserType());
        saveFormDTO.setUserTypeIds(formDTO.getUserTypeIds());
        saveFormDTO.setOperBy( request.getSession().getAttribute("user").toString() );
        saveFormDTO.setOperTime(new Date());
        saveFormDTO.setDelflag(null);
        return flowStepService.saveFlowStepForm(saveFormDTO);
    }

    @RequestMapping(value = "del/{id}", method = RequestMethod.POST)
    @ResponseBody
    public Object del(@PathVariable(name = "id") Long id, HttpServletRequest request) {
        if( this.flowStepService.delFlowStep(id, request.getSession().getAttribute("user").toString()) ){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS).setMessage("删除成功");
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("删除失败");
    }

}
