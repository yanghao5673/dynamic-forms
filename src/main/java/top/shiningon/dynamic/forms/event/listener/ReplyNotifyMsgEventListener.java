package top.shiningon.dynamic.forms.event.listener;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import cn.hutool.json.JSONUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;
import top.shiningon.dynamic.forms.constant.BillStatusEnum;
import top.shiningon.dynamic.forms.dto.callback.response.ReplyNotifyFormDTO;
import top.shiningon.dynamic.forms.dto.process.bill.response.detail.BillDetailDetailDTO;
import top.shiningon.dynamic.forms.entity.*;
import top.shiningon.dynamic.forms.event.ReplyNotifyMsgEvent;
import top.shiningon.dynamic.forms.service.*;

import java.util.ArrayList;
import java.util.List;

@Service("top.shiningon.dynamic.forms.event.listener.ReplyNotifyMsgEventListener")
public class ReplyNotifyMsgEventListener {

    @Autowired
    BillListService billListService;
    @Autowired
    BillDetailService billDetailService;
    @Autowired
    BillConfigService billConfigService;
    @Autowired
    BillReplyService billReplyService;
    @Autowired
    MemberService memberService;
    @Autowired
    OutsideConfigService outsideConfigService;
    @Autowired
    HttpClientService httpClientService;

    /**
     * 监听事件
     * 异步执行
     */
    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT, classes = ReplyNotifyMsgEvent.class, fallbackExecution = true)
    public void onApplicationEvent(ReplyNotifyMsgEvent msgEvent) {
        //判断有没有设置回调地址。
        OutsideConfig config = outsideConfigService.getConfig();
        if(null == config || StrUtil.isBlank(config.getReplyCallBackUrl())){
            return;
        }
        //处理具体事务
        BillList bill = billListService.searchBillListSingle(msgEvent.getBillId());
        BillConfig billConfig = billConfigService.searchBillConfigSingle(bill.getBillConfigId());
        BillReply reply = billReplyService.searchBillReplySingle(msgEvent.getBillReplyId());
        ReplyNotifyFormDTO dto = new ReplyNotifyFormDTO();
        dto.setId(bill.getId());
        dto.setTitle(bill.getTitle());
        dto.setSubTitle(bill.getSubTitle());
        dto.setBillConfigCode(billConfig.getCode());
        dto.setReplyStatus(reply.getReplyStatus());
        dto.setBillIsOver( BillStatusEnum.完成.equals(bill.getStatus()) );
        dto.setCreateBy(bill.getCreateBy());
        dto.setCreateMember(memberService.getById(bill.getCreateBy()));
        dto.setCreateTime(bill.getCreateTime());
        dto.setOperBy(reply.getCreateBy());
        dto.setOperMember(memberService.getById(reply.getCreateBy()));
        dto.setOperTime(reply.getCreateTime());
        List<BillDetail> detailList = billDetailService.searchBillDetailListByBillId(bill.getId());
        if(CollUtil.isNotEmpty(detailList)){
            dto.setFormInputList(new ArrayList<>(detailList.size()));
            for(BillDetail detail : detailList){
                BillDetailDetailDTO detailDTO = new BillDetailDetailDTO();
                detailDTO.setId(detail.getId());
                detailDTO.setIndex(detail.getIndex());
                detailDTO.setInputName(detail.getInputName());
                detailDTO.setInputType(detail.getInputType());
                detailDTO.setInputVal(detail.getInputVal());
                dto.getFormInputList().add(detailDTO);
            }
        }
        //消息通知
        String response = httpClientService.doPost(config.getReplyCallBackUrl(), JSONUtil.toJsonStr(dto));
        if(null == response){
            //TODO 请求报错，后续处理
        }
    }

}
