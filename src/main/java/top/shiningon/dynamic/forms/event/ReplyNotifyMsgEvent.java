package top.shiningon.dynamic.forms.event;

import org.springframework.context.ApplicationEvent;

public class ReplyNotifyMsgEvent extends ApplicationEvent{

    private Long billId;

    private Long billReplyId;

    public ReplyNotifyMsgEvent(Object source, Long billId, Long billReplyId){
        super(source);
        this.billId = billId;
        this.billReplyId = billReplyId;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public Long getBillReplyId() {
        return billReplyId;
    }

    public void setBillReplyId(Long billReplyId) {
        this.billReplyId = billReplyId;
    }
}
