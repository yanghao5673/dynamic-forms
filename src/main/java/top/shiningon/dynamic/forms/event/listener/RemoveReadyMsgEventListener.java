package top.shiningon.dynamic.forms.event.listener;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;
import top.shiningon.dynamic.forms.constant.BillStatusEnum;
import top.shiningon.dynamic.forms.entity.BillList;
import top.shiningon.dynamic.forms.entity.BillReplyReady;
import top.shiningon.dynamic.forms.event.RemoveReadyMsgEvent;
import top.shiningon.dynamic.forms.service.BillListService;
import top.shiningon.dynamic.forms.service.BillReplyReadyService;
import top.shiningon.dynamic.forms.service.BillReplyReadyUserService;

import java.util.List;
import java.util.stream.Collectors;

@Service("top.shiningon.dynamic.forms.event.listener.RemoveReadyMsgEventListener")
public class RemoveReadyMsgEventListener {

    @Autowired
    BillListService billListService;
    @Autowired
    BillReplyReadyService billReplyReadyService;
    @Autowired
    BillReplyReadyUserService billReplyReadyUserService;

    /**
     * 监听事件
     * 异步执行
     */
    @Async
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMMIT, classes = RemoveReadyMsgEvent.class, fallbackExecution = true)
    public void onApplicationEvent(RemoveReadyMsgEvent msgEvent) {
        BillList billList = billListService.searchBillListSingle(msgEvent.getBillId());
        if( BillStatusEnum.审批驳回.equals(billList.getStatus()) || BillStatusEnum.完成.equals(billList.getStatus()) ) {
            //获取当前的ready
            BillReplyReady nowReady = this.billReplyReadyService.getNowReady(msgEvent.getBillId());
            //拿到后续的ready的list
            List<BillReplyReady> afterReadyList = this.billReplyReadyService.getAfterReadyList(msgEvent.getBillId(), nowReady.getIndex());
            //删除list相关的readyUser
            List<Long> readyIdList = afterReadyList.stream().map(BillReplyReady::getId).collect(Collectors.toList());
            this.billReplyReadyUserService.delBillReplyReadyUserByReadyIdList(readyIdList);
            //删除ready的list
            this.billReplyReadyService.removeBatchByIds(afterReadyList);
        }
    }

}
