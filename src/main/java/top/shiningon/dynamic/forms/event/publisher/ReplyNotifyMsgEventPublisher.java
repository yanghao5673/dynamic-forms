package top.shiningon.dynamic.forms.event.publisher;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;
import top.shiningon.dynamic.forms.event.ReplyNotifyMsgEvent;

@Service("top.shiningon.dynamic.forms.event.publisher.ReplyNotifyMsgEventPublisher")
public class ReplyNotifyMsgEventPublisher implements ApplicationEventPublisherAware {

    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    /**
     * 连接事件
     */
    public void sendReplyNotify(Long billId, Long billReplyId) {
        applicationEventPublisher.publishEvent(new ReplyNotifyMsgEvent(this, billId, billReplyId));
    }

}
