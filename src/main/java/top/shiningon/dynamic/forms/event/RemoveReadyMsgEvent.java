package top.shiningon.dynamic.forms.event;

import org.springframework.context.ApplicationEvent;

public class RemoveReadyMsgEvent extends ApplicationEvent{

    private Long billId;

    private Long currentStepId;

    public RemoveReadyMsgEvent(Object source, Long billId, Long currentStepId){
        super(source);
        this.billId = billId;
        this.currentStepId = currentStepId;
    }

    public Long getBillId() {
        return billId;
    }

    public void setBillId(Long billId) {
        this.billId = billId;
    }

    public Long getCurrentStepId() {
        return currentStepId;
    }

    public void setCurrentStepId(Long currentStepId) {
        this.currentStepId = currentStepId;
    }
}
