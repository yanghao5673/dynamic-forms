package top.shiningon.dynamic.forms.event.publisher;

import org.springframework.context.ApplicationEventPublisher;
import org.springframework.context.ApplicationEventPublisherAware;
import org.springframework.stereotype.Service;
import top.shiningon.dynamic.forms.event.RemoveReadyMsgEvent;

@Service("top.shiningon.dynamic.forms.event.publisher.RemoveReadyMsgEventPublisher")
public class RemoveReadyMsgEventPublisher implements ApplicationEventPublisherAware {

    private ApplicationEventPublisher applicationEventPublisher;

    @Override
    public void setApplicationEventPublisher(ApplicationEventPublisher applicationEventPublisher) {
        this.applicationEventPublisher = applicationEventPublisher;
    }

    /**
     * 连接事件
     */
    public void removeReadyMsg(Long billId, Long currentStepId) {
        applicationEventPublisher.publishEvent(new RemoveReadyMsgEvent(this, billId, currentStepId));
    }

}
