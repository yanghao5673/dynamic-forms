package top.shiningon.dynamic.forms.base;

import com.baomidou.mybatisplus.core.metadata.IPage;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.util.List;

@Data
@ApiModel(value="SearchResoultDTO对象",description="统一查询返回对象")
public class SearchResoultDTO<T> {

    @ApiModelProperty(position=1, name = "isPage", required = true, notes="是否是分页查询", example = "T")
    private boolean isPage;

    @ApiModelProperty(position=2, name = "list", required = true, notes="查询结果列表", example = "T")
    private List<T> list;

    @ApiModelProperty(position=3, name = "pageNum", required = true, notes="页码", example = "T")
    private long pageNum = 0L;

    @ApiModelProperty(position=4, name = "pageSize", required = true, notes="每页记录数", example = "T")
    private long pageSize = 0L;

    @ApiModelProperty(position=5, name = "rowCount", required = true, notes="总记录数", example = "T")
    private long rowCount = 0L;

    public void putResoult(List<T> list){
        this.isPage = false;
        this.list = list;
    }

    public void putResoult(IPage<T> page){
        this.isPage = true;
        this.list = page.getRecords();
        this.pageNum = page.getCurrent();
        this.pageSize = page.getSize();
        this.rowCount = page.getTotal();
    }

}
