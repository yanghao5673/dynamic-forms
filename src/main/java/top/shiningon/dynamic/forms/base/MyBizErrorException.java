package top.shiningon.dynamic.forms.base;

/**
 * @author yanghao
 * 2019年5月15日
 */
public class MyBizErrorException extends RuntimeException{
	
	private static final long serialVersionUID = -3564692205017126073L;
	
	private ReturnParam returnParam;
	
	public MyBizErrorException(ReturnParam returnParam) {
        super("runtime error : " + returnParam.toString());
        this.returnParam = returnParam;
    }

	public ReturnParam getReturnParam() {
		return returnParam;
	}

	public void setReturnParam(ReturnParam returnParam) {
		this.returnParam = returnParam;
	}
	
}
