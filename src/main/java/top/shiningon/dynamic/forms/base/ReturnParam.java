package top.shiningon.dynamic.forms.base;

import cn.hutool.core.util.StrUtil;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AccessLevel;
import lombok.Data;
import lombok.Setter;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * 返回信息封装类
 * @author winder.yang
 *
 */
@Data
@ApiModel(value="ReturnParam对象",description="统一返回对象")
public class ReturnParam<T> {

	@ApiModelProperty(position=1, name = "result", required = true, notes="T or F", example = "T")
	@Setter(AccessLevel.NONE)
	private String result;
	@ApiModelProperty(position=2, name = "code", required = true, notes="200 或其他异常编码", example = "200")
	@Setter(AccessLevel.NONE)
	private Integer code;
	@ApiModelProperty(position=3, name = "msg", required = true, notes="操作结果描述", example = "操作成功")
	@Setter(AccessLevel.NONE)
	private String msg;
	@ApiModelProperty(position=4, name = "content", notes="返回的对象")
	@Setter(AccessLevel.NONE)
	private T content;

	public ReturnParam() {}

	public ReturnParam(ErrorCodeEnum errorCodeEnum) {
		this.setReturnInfo(ErrorCodeEnum.GLOBAL_SUCCESS.equals(errorCodeEnum), errorCodeEnum.getCode(), errorCodeEnum.getMsg());
	}

	public ReturnParam<T> setResult(String result) {
		this.result = result;
		return this;
	}

	public ReturnParam<T> setCode(Integer code){
		this.code = code;
		return this;
	}

	public ReturnParam<T> setMsg(String msg){
		this.msg = msg;
		return this;
	}

	public ReturnParam<T> setMessage(String message){
		this.msg = message;
		return this;
	}

	public ReturnParam<T> setContent(T content){
		this.content = content;
		return this;
	}

	/**
	 * 检查对象中返回的结果，是否是成功。
	 */
	public boolean checkIsSuccess(){
		return StrUtil.isNotBlank(this.result) && "T".equals(this.result);
	}

	/**
	 * 设定操作成功与否，返回对象
	 */
	public ReturnParam<T> setIsSuccess(boolean isSuccess){
		this.result = (isSuccess?"T":"F");
		return this;
	}

	/**
	 * 设置成功与否，状态code，以及操作信息
	 */
	public ReturnParam<T> setReturnInfo(boolean isSuccess, int code, String message){
		return this.setIsSuccess(isSuccess).setMessage(message).setCode(code);
	}

	public Map<String,Object> toMap(){
		Map<String,Object> retMap = new ConcurrentHashMap<>(4);
		retMap.put("result", result);
		if(null != code) {
			retMap.put("code", code);
		}else{
			if( this.checkIsSuccess() ){
				retMap.put("code", 200);
			}else{
				retMap.put("code", 500);
			}
		}
		retMap.put("msg", msg);
		if(null != content) {
			retMap.put("content", content);
		}
		return retMap;
	}

	public String toJson(){
		return "{\"result\":\"" + this.result + "\",\"code\":" + this.getCode() + ",\"msg\":\"" + this.getMsg() + "\"}";
	}

	@Override
	public String toString() {
		String str = "result:" + result + " code:" + code + " msg:" + msg;
		if(null != this.content) {
			str += " " + this.content.toString();
		}
		return str;
	}

}
