package top.shiningon.dynamic.forms;

import lombok.extern.log4j.Log4j2;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

@EnableAsync
@EnableWebMvc
@SpringBootApplication
@Log4j2
public class DynamicFormsApplication {

    public static void main(String[] args) {
        SpringApplication.run(DynamicFormsApplication.class, args);
    }

}
