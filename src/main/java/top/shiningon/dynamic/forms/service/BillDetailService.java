package top.shiningon.dynamic.forms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dto.request.BillDetailFormDTO;
import top.shiningon.dynamic.forms.dto.request.BillDetailSearchListDTO;
import top.shiningon.dynamic.forms.entity.BillDetail;

import java.util.List;

public interface BillDetailService extends IService<BillDetail> {

    public List<BillDetail> searchBillDetailListByBillId(Long billId);

    /**
     * 查询单据字段列表
     */
    SearchResoultDTO<BillDetail> searchBillDetailList(BillDetailSearchListDTO formDTO);

    /**
     * 查询单个单据字段
     */
    BillDetail searchBillDetailSingle(Long id);

    /**
     * 创建单据字段
     */
    boolean createBillDetail(BillDetailFormDTO formDTO);

    /**
     * 修改单据字段
     */
    boolean updateBillDetail(BillDetailFormDTO formDTO);

    /**
     * 删除单据字段
     */
    boolean delBillDetail(Long id, String operId);

    boolean batchSaveOrUpdate(List<BillDetail> detailList, String operId);

}
