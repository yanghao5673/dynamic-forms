package top.shiningon.dynamic.forms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.dto.sync.request.MemberSyncDTO;
import top.shiningon.dynamic.forms.entity.Member;

import java.util.List;

public interface MemberService extends IService<Member> {

    Member getById(String id);

    ReturnParam saveMember(MemberSyncDTO dto);

    ReturnParam saveBatchMember(List<MemberSyncDTO> dtoList);

    ReturnParam delMember(String id);

    ReturnParam delBatchMember(List<String> idList);

}
