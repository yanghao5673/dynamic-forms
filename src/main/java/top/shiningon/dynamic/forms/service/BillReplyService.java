package top.shiningon.dynamic.forms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;
import top.shiningon.dynamic.forms.dto.process.bill.request.BillReplySearchListDTO;
import top.shiningon.dynamic.forms.dto.request.BillReplyFormDTO;
import top.shiningon.dynamic.forms.entity.BillReply;

import java.util.List;

public interface BillReplyService extends IService<BillReply> {

    public List<Long> searchUserReplyBillListId(String oprId, PageFormDTO formDTO);

    /**
     * 查询流程步骤列表
     */
    SearchResoultDTO<BillReply> searchBillReplyList(BillReplySearchListDTO formDTO);

    /**
     * 查询单个流程步骤
     */
    BillReply searchBillReplySingle(Long id);

    /**
     * 创建流程步骤
     */
    boolean createBillReply(BillReplyFormDTO formDTO);

    /**
     * 修改流程步骤
     */
    boolean updateBillReply(BillReplyFormDTO formDTO);

    /**
     * 删除流程步骤
     */
    boolean delBillReply(Long id, String operId);

    BillReply getLastReplyByBill(Long billId);

}
