package top.shiningon.dynamic.forms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.benmanes.caffeine.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.constant.Cache1HKey;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dao.FlowConfigDao;
import top.shiningon.dynamic.forms.dto.config.FlowConfigFormDTO;
import top.shiningon.dynamic.forms.dto.process.base.request.FlowConfigSearchListDTO;
import top.shiningon.dynamic.forms.entity.FlowConfig;
import top.shiningon.dynamic.forms.service.FlowConfigService;
import top.shiningon.dynamic.forms.service.FlowStepService;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Component("top.shiningon.dynamic.forms.service.impl.FlowConfigServiceImpl")
public class FlowConfigServiceImpl extends ServiceImpl<FlowConfigDao, FlowConfig> implements FlowConfigService {

    @Autowired
    FlowConfigDao flowConfigDao;
    @Autowired
    FlowStepService flowStepService;
    @Autowired
    Cache<String, Object> caffeineCache1H;

    /**
     * 查询流程配置列表
     */
    public SearchResoultDTO<FlowConfig> searchFlowConfigList(FlowConfigSearchListDTO formDTO){
        SearchResoultDTO<FlowConfig> resDTO = new SearchResoultDTO<>();
        LambdaQueryWrapper<FlowConfig> wrapper = new LambdaQueryWrapper<>();
        //查询条件组装
        if(StrUtil.isNotBlank(formDTO.getName())){
            wrapper.like(FlowConfig::getName, formDTO.getName());
        }
        if(null != formDTO.getStatus()){
            wrapper.eq(FlowConfig::getStatus, formDTO.getStatus());
        }
        if(null != formDTO.getDelflag()){
            wrapper.eq(FlowConfig::getDelflag, formDTO.getDelflag());
        }
        if(formDTO.isPageSearch()) {
            IPage<FlowConfig> page = flowConfigDao.selectPage(new Page<>(formDTO.getPageNum(), formDTO.getPageSize()), wrapper);
            resDTO.putResoult(page);
        }else{
            //不分页，进行以下
            resDTO.putResoult(flowConfigDao.selectList(wrapper));
        }
        return resDTO;
    }

    /**
     * 查询单个流程配置
     */
    public FlowConfig searchFlowConfigSingle(Long id){
        Object obj = caffeineCache1H.getIfPresent(Cache1HKey.FLOW_CONFIG_BY_ID + id);
        if(null == obj) {
            LambdaQueryWrapper<FlowConfig> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(FlowConfig::getId, id);
            wrapper.eq(FlowConfig::getDelflag, false);
            FlowConfig config = this.flowConfigDao.selectOne(wrapper);
            if(null != config) {
                caffeineCache1H.put(Cache1HKey.FLOW_CONFIG_BY_ID + id, config);
            }else{
                caffeineCache1H.put(Cache1HKey.FLOW_CONFIG_BY_ID + id, new FlowConfig());
            }
            return config;
        }
        return (FlowConfig)obj;
    }

    public ReturnParam saveFlowConfigForm(@NotNull FlowConfigFormDTO formDTO){
        //必填校验
        if(StrUtil.isBlank(formDTO.getName())){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("流程名称未填写");
        }
        if(null == formDTO.getStatus()){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("状态未填写");
        }
        //创建对象
        FlowConfig flowConfig = new FlowConfig();
        if(null != formDTO.getId()) {
            flowConfig.setId(formDTO.getId());
            flowConfig.setUpdateBy(formDTO.getOperBy());
            flowConfig.setUpdateTime(formDTO.getOperTime());
            if(null != formDTO.getDelflag()) {
                flowConfig.setDelflag(formDTO.getDelflag());
            }
        }else{
            flowConfig.setCreateBy(formDTO.getOperBy());
            flowConfig.setCreateTime(formDTO.getOperTime());
            flowConfig.setDelflag(false);
        }
        flowConfig.setName(formDTO.getName());
        flowConfig.setStatus(formDTO.getStatus());
        if( this.saveOrUpdate(flowConfig) ) {
            caffeineCache1H.invalidate(Cache1HKey.FLOW_CONFIG_BY_ID + flowConfig.getId());
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }else{
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("保存失败");
        }
    }

    /**
     * 删除流程配置
     */
    public boolean delFlowConfig(Long id, String operId){
        FlowConfig config = this.flowConfigDao.selectById(id);
        config.setDelflag(true);
        config.setUpdateBy(operId);
        config.setUpdateTime(new Date());
        int num = this.flowConfigDao.updateById(config);
        caffeineCache1H.invalidate(Cache1HKey.FLOW_CONFIG_BY_ID + config.getId());
        return num == 1;
    }

}
