package top.shiningon.dynamic.forms.service.impl;

import cn.hutool.core.lang.TypeReference;
import cn.hutool.http.HttpResponse;
import cn.hutool.http.HttpUtil;
import cn.hutool.json.JSONUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.benmanes.caffeine.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.constant.Cache10MKey;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.constant.MemberSyncModeEnum;
import top.shiningon.dynamic.forms.dao.OutsideConfigDao;
import top.shiningon.dynamic.forms.dto.admin.OutsideMemberSyncFormDTO;
import top.shiningon.dynamic.forms.dto.rank.ReplyRankSearchDTO;
import top.shiningon.dynamic.forms.dto.rank.ReplyRankSearchResponseDTO;
import top.shiningon.dynamic.forms.entity.OutsideConfig;
import top.shiningon.dynamic.forms.service.HttpClientService;
import top.shiningon.dynamic.forms.service.OutsideConfigService;

import java.util.Date;
import java.util.List;

/**
 * 配置信息写入内存进行缓存，提供外部接口进行手动刷新
 */
@Component("top.shiningon.dynamic.forms.service.impl.OutsideConfigServiceImpl")
public class OutsideConfigServiceImpl implements OutsideConfigService {

    @Autowired
    OutsideConfigDao outsideConfigDao;
    @Autowired
    Cache<String, Object> caffeineCache10M;
    @Autowired
    HttpClientService httpClientService;

    public List<ReplyRankSearchResponseDTO> getBillRankUserIdList(List<ReplyRankSearchDTO> searchDTOList){
        OutsideConfig config = this.getConfig();
        String response = httpClientService.doPost(config.getUrlExpression(), JSONUtil.toJsonStr(searchDTOList));
        if(null != response) {
            return JSONUtil.parseObj(response).toBean(new TypeReference<ReturnParam<List<ReplyRankSearchResponseDTO>>>() {}).getContent();
        }
        return null;
    }

    public String getMemberTaskCron(){
        OutsideConfig config = this.getConfig();
        return null != config.getMemberSyncCron() ? config.getMemberSyncCron() : null;
    }

    public OutsideConfig getConfig(){
        Object obj = caffeineCache10M.getIfPresent(Cache10MKey.CONFIG_CACHE);
        if(null == obj) {
            LambdaQueryWrapper<OutsideConfig> query = new LambdaQueryWrapper<>();
            query.eq(OutsideConfig::getDelflag, false);
            OutsideConfig config = outsideConfigDao.selectOne(query);
            if(null == config) {
                config = new OutsideConfig();
                config.setMemberSyncMode(MemberSyncModeEnum.不同步);
            }
            caffeineCache10M.put(Cache10MKey.CONFIG_CACHE, config);
            return config;
        }
        return (OutsideConfig)obj;
    }

    public ReturnParam updateUrlExpression(String urlExpression, String replyCallBackUrl, String userId){
        boolean isSuccess = false;
        OutsideConfig config = this.getConfig();
        if(null == config){
            config = new OutsideConfig();
        }
        config.setUrlExpression(urlExpression);
        config.setReplyCallBackUrl(replyCallBackUrl);
        if(null == config.getId()) {
            config.setCreateBy(userId);
            config.setCreateTime(new Date());
            config.setDelflag(false);
            isSuccess = this.outsideConfigDao.insert(config) == 1;
        }else {
            config.setUpdateBy(userId);
            config.setUpdateTime(new Date());
            isSuccess = this.outsideConfigDao.updateById(config) == 1;
        }
        if(isSuccess) {
            caffeineCache10M.invalidate(Cache10MKey.CONFIG_CACHE);
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR);
    }

    public ReturnParam updateMemberSync(OutsideMemberSyncFormDTO formDTO, String userId){
        boolean isSuccess = false;
        OutsideConfig config = this.getConfig();
        if(null == config){
            config = new OutsideConfig();
        }
        config.setMemberSyncMode(formDTO.getMemberSyncMode());
        config.setMemberSyncCron(formDTO.getMemberSyncCron());
        config.setMemberSyncUrl(formDTO.getMemberSyncUrl());
        if(null == config.getId()) {
            config.setCreateBy(userId);
            config.setCreateTime(new Date());
            config.setDelflag(false);
            isSuccess = this.outsideConfigDao.insert(config) == 1;
        }else {
            config.setUpdateBy(userId);
            config.setUpdateTime(new Date());
            isSuccess = this.outsideConfigDao.updateById(config) == 1;
        }
        if(isSuccess) {
            caffeineCache10M.invalidate(Cache10MKey.CONFIG_CACHE);
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR);
    }

}
