package top.shiningon.dynamic.forms.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.benmanes.caffeine.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.constant.Cache1HKey;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dao.FlowStepDao;
import top.shiningon.dynamic.forms.dto.config.FlowStepFormDTO;
import top.shiningon.dynamic.forms.dto.request.FlowStepSearchListDTO;
import top.shiningon.dynamic.forms.entity.FlowStep;
import top.shiningon.dynamic.forms.service.FlowStepService;

import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component("top.shiningon.dynamic.forms.service.impl.FlowStepServiceImpl")
public class FlowStepServiceImpl extends ServiceImpl<FlowStepDao, FlowStep> implements FlowStepService {

    @Autowired
    FlowStepDao flowStepDao;
    @Autowired
    Cache<String, Object> caffeineCache1H;

    public List<FlowStep> searchFlowStepList(Long flowConfigId){
        Object obj = caffeineCache1H.getIfPresent(Cache1HKey.FLOW_STEP_LIST_BY_FLOW_CONFIG_ID + flowConfigId);
        if(null == obj) {
            LambdaQueryWrapper<FlowStep> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(FlowStep::getFlowConfigId, flowConfigId);
            wrapper.eq(FlowStep::getDelflag, false);
            List<FlowStep> stepList = flowStepDao.selectList(wrapper);
            if(null != stepList) {
                caffeineCache1H.put(Cache1HKey.FLOW_STEP_LIST_BY_FLOW_CONFIG_ID + flowConfigId, stepList);
            }else{
                caffeineCache1H.put(Cache1HKey.FLOW_STEP_LIST_BY_FLOW_CONFIG_ID + flowConfigId, new ArrayList<FlowStep>(0));
            }
            return stepList;
        }
        return (List<FlowStep>)obj;
    }

    /**
     * 查询流程步骤列表
     */
    public SearchResoultDTO<FlowStep> searchFlowStepList(FlowStepSearchListDTO formDTO){
        SearchResoultDTO<FlowStep> resDTO = new SearchResoultDTO<>();
        if(CollUtil.isEmpty(formDTO.getFlowConfigIdList()) && StrUtil.isBlank(formDTO.getName())){
            return resDTO;
        }
        LambdaQueryWrapper<FlowStep> wrapper = new LambdaQueryWrapper<>();
        //查询条件组装
        if( !CollectionUtils.isEmpty(formDTO.getFlowConfigIdList()) ){
            if(formDTO.getFlowConfigIdList().size() > 1){
                wrapper.in(FlowStep::getFlowConfigId, formDTO.getFlowConfigIdList());
            }else{
                wrapper.eq(FlowStep::getFlowConfigId, formDTO.getFlowConfigIdList().get(0));
            }
        }
        if(StrUtil.isNotBlank(formDTO.getName())){
            wrapper.like(FlowStep::getName, formDTO.getName());
        }
        if(null != formDTO.getDelflag()){
            wrapper.eq(FlowStep::getDelflag, formDTO.getDelflag());
        }else{
            wrapper.eq(FlowStep::getDelflag, false);
        }
        //排序
        this.initOrderBy(wrapper, formDTO.getOrderType(), formDTO.getOrderCol());
        List<FlowStep> list;
        if(formDTO.isPageSearch()) {
            IPage<FlowStep> page = flowStepDao.selectPage(new Page<>(formDTO.getPageNum(), formDTO.getPageSize()), wrapper);
            list = page.getRecords();
            resDTO.setPage(true);
            resDTO.setList(list);
            resDTO.setPageNum(formDTO.getPageNum());
            resDTO.setPageSize(formDTO.getPageSize());
            resDTO.setRowCount(page.getTotal());
        }else{
            //不分页，进行以下
            list = flowStepDao.selectList(wrapper);
            resDTO.setPage(false);
            resDTO.setList(list);
        }
        return resDTO;
    }

    /**
     * 查询单个流程步骤
     */
    public FlowStep searchFlowStepSingle(Long id){
        if(id <= 0L){ return null; }
        Object obj = caffeineCache1H.getIfPresent(Cache1HKey.FLOW_STEP_BY_ID + id);
        if(null == obj) {
            LambdaQueryWrapper<FlowStep> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(FlowStep::getId, id);
            wrapper.eq(FlowStep::getDelflag, false);
            FlowStep step = this.flowStepDao.selectOne(wrapper);
            if(null != step) {
                caffeineCache1H.put(Cache1HKey.FLOW_STEP_BY_ID + id, step);
            }else{
                caffeineCache1H.put(Cache1HKey.FLOW_STEP_BY_ID + id, new FlowStep());
            }
            return step;
        }
        return (FlowStep)obj;
    }

    public FlowStep searchNextFlowStepSingle(Long flowConfigId, Integer nowStepIndex){
        if(null == nowStepIndex){
            nowStepIndex = -1;
        }
        Object obj = caffeineCache1H.getIfPresent(Cache1HKey.NEXT_FLOW_STEP_BY_FLOW_CONFIG + flowConfigId + ":" + nowStepIndex);
        if(null == obj) {
            FlowStep step = null;
            LambdaQueryWrapper<FlowStep> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(FlowStep::getFlowConfigId, flowConfigId);
            wrapper.eq(FlowStep::getDelflag, false);
            wrapper.gt(FlowStep::getIndex, nowStepIndex);
            wrapper.orderByAsc(FlowStep::getIndex);
            List<FlowStep> stepList = this.flowStepDao.selectList(wrapper);
            if (CollUtil.isNotEmpty(stepList)) {
                step = stepList.get(0);
            }
            if(null != step) {
                caffeineCache1H.put(Cache1HKey.NEXT_FLOW_STEP_BY_FLOW_CONFIG + flowConfigId + ":" + nowStepIndex, step);
            }else{
                caffeineCache1H.put(Cache1HKey.NEXT_FLOW_STEP_BY_FLOW_CONFIG + flowConfigId + ":" + nowStepIndex, new FlowStep());
            }
            return step;
        }
        return (FlowStep)obj;
    }

    public ReturnParam saveFlowStepForm(@NotNull FlowStepFormDTO formDTO){
        //必填校验
        if(null == formDTO.getFlowConfigId()){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("流程配置未填写");
        }
        if(StrUtil.isBlank(formDTO.getName())){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("流程步骤未填写");
        }
        if(null == formDTO.getIndex()){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("步骤顺序未填写");
        }
        if(null == formDTO.getReplyType()){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("审批方式未填写");
        }
        if(null == formDTO.getReplyUserType()){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("审批级别未填写");
        }
        if(StrUtil.isBlank(formDTO.getUserTypeIds())){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("级别主键未填写");
        }
        //检查步骤顺序是否重复
        List<FlowStep> stepList = this.searchFlowStepList(formDTO.getFlowConfigId());
        if(CollUtil.isNotEmpty(stepList)){
            for(FlowStep fs : stepList){
                if(fs.getId() != formDTO.getId() && fs.getIndex() == formDTO.getIndex()){
                    return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("步骤顺序重复不可用");
                }
            }
        }

        //创建对象
        FlowStep flowStep = new FlowStep();
        if(null != formDTO.getId()) {
            flowStep.setId(formDTO.getId());
            flowStep.setUpdateBy(formDTO.getOperBy());
            flowStep.setUpdateTime(formDTO.getOperTime());
            if(null != formDTO.getDelflag()) {
                flowStep.setDelflag(formDTO.getDelflag());
            }
        }else{
            flowStep.setFlowConfigId(formDTO.getFlowConfigId());
            flowStep.setCreateBy(formDTO.getOperBy());
            flowStep.setCreateTime(formDTO.getOperTime());
            flowStep.setDelflag(false);
        }
        flowStep.setName(formDTO.getName());
        flowStep.setIndex(formDTO.getIndex());
        flowStep.setReplyType(formDTO.getReplyType());
        flowStep.setReplyUserType(formDTO.getReplyUserType());
        flowStep.setUserTypeIds(formDTO.getUserTypeIds());
        if( this.saveOrUpdate(flowStep) ) {
            caffeineCache1H.invalidate(Cache1HKey.FLOW_STEP_LIST_BY_FLOW_CONFIG_ID + flowStep.getFlowConfigId());
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }else{
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("保存失败");
        }
    }

    /**
     * 删除流程步骤
     */
    public boolean delFlowStep(Long id, String operId){
        FlowStep step = this.flowStepDao.selectById(id);
        step.setDelflag(true);
        step.setUpdateBy(operId);
        step.setUpdateTime(new Date());
        caffeineCache1H.invalidate(Cache1HKey.FLOW_STEP_LIST_BY_FLOW_CONFIG_ID + step.getFlowConfigId());
        int num = this.flowStepDao.updateById(step);
        return num == 1;
    }

    private LambdaQueryWrapper<FlowStep> initOrderBy(LambdaQueryWrapper<FlowStep> wrapper, String orderType, String orderCol){
        if( StrUtil.isNotBlank(orderType) ){
            if("ASC".equalsIgnoreCase(orderCol)){
                wrapper.orderByAsc(FlowStep::getIndex);
            }
            if("DESC".equalsIgnoreCase(orderCol)){
                wrapper.orderByDesc(FlowStep::getIndex);
            }
        }else{
            //默认按照index顺序
            wrapper.orderByAsc(FlowStep::getIndex);
        }
        return wrapper;
    }

}
