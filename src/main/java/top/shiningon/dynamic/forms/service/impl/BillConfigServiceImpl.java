package top.shiningon.dynamic.forms.service.impl;

import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.benmanes.caffeine.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.constant.Cache1HKey;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dao.BillConfigDao;
import top.shiningon.dynamic.forms.dto.config.BillConfigFormDTO;
import top.shiningon.dynamic.forms.dto.process.base.request.BillConfigSearchListDTO;
import top.shiningon.dynamic.forms.entity.BillConfig;
import top.shiningon.dynamic.forms.service.BillConfigService;
import top.shiningon.dynamic.forms.service.BillFormFieldConfigService;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Component("top.shiningon.dynamic.forms.service.impl.BillConfigServiceImpl")
public class BillConfigServiceImpl extends ServiceImpl<BillConfigDao, BillConfig> implements BillConfigService {

    @Autowired
    BillConfigDao billConfigDao;
    @Autowired
    BillFormFieldConfigService billFormFieldConfigService;
    @Autowired
    Cache<String, Object> caffeineCache1H;

    /**
     * 查询单据配置列表
     */
    public SearchResoultDTO<BillConfig> searchBillConfigList(BillConfigSearchListDTO formDTO){
        SearchResoultDTO<BillConfig> resDTO = new SearchResoultDTO<>();
        LambdaQueryWrapper<BillConfig> wrapper = new LambdaQueryWrapper<>();
        //查询条件组装
        if( !CollectionUtils.isEmpty(formDTO.getWorkFlowIdList()) ){
            if(formDTO.getWorkFlowIdList().size() > 1){
                wrapper.in(BillConfig::getFlowConfigId, formDTO.getWorkFlowIdList());
            }else{
                wrapper.eq(BillConfig::getFlowConfigId, formDTO.getWorkFlowIdList().get(0));
            }
        }
        if(StringUtils.hasText(formDTO.getCode())){
            wrapper.like(BillConfig::getCode, formDTO.getCode());
        }
        if(StringUtils.hasText(formDTO.getTitle())){
            wrapper.like(BillConfig::getTitle, formDTO.getTitle());
        }
        if(null != formDTO.getDelflag()){
            wrapper.eq(BillConfig::getDelflag, formDTO.getDelflag());
        }else{
            wrapper.eq(BillConfig::getDelflag, false);
        }
        if(formDTO.isPageSearch()) {
            IPage<BillConfig> page = billConfigDao.selectPage(new Page<>(formDTO.getPageNum(), formDTO.getPageSize()), wrapper);
            resDTO.putResoult(page);
        }else{
            //不分页，进行以下
            resDTO.putResoult(billConfigDao.selectList(wrapper));
        }
        return resDTO;
    }

    /**
     * 查询单个单据配置
     */
    public BillConfig searchBillConfigSingle(Long id){
        Object obj = caffeineCache1H.getIfPresent(Cache1HKey.BILL_CONFIG_BY_ID + id);
        if(null == obj) {
            LambdaQueryWrapper<BillConfig> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(BillConfig::getId, id);
            wrapper.eq(BillConfig::getDelflag, false);
            BillConfig config = this.billConfigDao.selectOne(wrapper);
            if(null != config) {
                caffeineCache1H.put(Cache1HKey.BILL_CONFIG_BY_ID + id, config);
            }else{
                caffeineCache1H.put(Cache1HKey.BILL_CONFIG_BY_ID + id, new BillConfig());
            }
            return config;
        }
        return (BillConfig)obj;
    }

    public ReturnParam saveBillConfigForm(@NotNull BillConfigFormDTO formDTO){
        //必填校验
        if(null == formDTO.getFlowConfigId()){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("流程名称未填写");
        }
        if(StrUtil.isBlank(formDTO.getCode())){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("流程名称未填写");
        }
        if(StrUtil.isBlank(formDTO.getTitle())){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("流程名称未填写");
        }
        if(StrUtil.isBlank(formDTO.getSubTitle())){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("流程名称未填写");
        }
        //创建对象
        BillConfig billConfig = new BillConfig();
        if(null != formDTO.getId()) {
            billConfig.setId(formDTO.getId());
            billConfig.setUpdateBy(formDTO.getOperBy());
            billConfig.setUpdateTime(formDTO.getOperTime());
            if(null != formDTO.getDelflag()) {
                billConfig.setDelflag(formDTO.getDelflag());
            }
        }else{
            billConfig.setCreateBy(formDTO.getOperBy());
            billConfig.setCreateTime(formDTO.getOperTime());
            billConfig.setDelflag(false);
        }
        billConfig.setFlowConfigId(formDTO.getFlowConfigId());
        billConfig.setCode(formDTO.getCode());
        billConfig.setTitle(formDTO.getTitle());
        billConfig.setSubTitle(formDTO.getSubTitle());
        if( this.saveOrUpdate(billConfig) ) {
            caffeineCache1H.invalidate(Cache1HKey.BILL_CONFIG_BY_ID + billConfig.getId());
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }else{
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("保存失败");
        }
    }

    /**
     * 删除单据配置
     */
    public boolean delBillConfig(Long id, String operId){
        BillConfig bill = this.billConfigDao.selectById(id);
        bill.setId(id);
        bill.setDelflag(true);
        bill.setUpdateBy(operId);
        bill.setUpdateTime(new Date());
        caffeineCache1H.invalidate(Cache1HKey.BILL_CONFIG_BY_ID + bill.getId());
        int num = this.billConfigDao.updateById(bill);
        return num == 1;
    }

}
