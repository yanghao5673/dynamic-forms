package top.shiningon.dynamic.forms.service;

import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.dto.admin.OutsideMemberSyncFormDTO;
import top.shiningon.dynamic.forms.dto.rank.ReplyRankSearchDTO;
import top.shiningon.dynamic.forms.dto.rank.ReplyRankSearchResponseDTO;
import top.shiningon.dynamic.forms.entity.OutsideConfig;

import java.util.List;

public interface OutsideConfigService {

    List<ReplyRankSearchResponseDTO> getBillRankUserIdList(List<ReplyRankSearchDTO> searchDTOList);

    public String getMemberTaskCron();

    OutsideConfig getConfig();

    ReturnParam updateUrlExpression(String urlExpression, String replyCallBackUrl, String userId);

    ReturnParam updateMemberSync(OutsideMemberSyncFormDTO formDTO, String userId);

}
