package top.shiningon.dynamic.forms.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.github.benmanes.caffeine.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.constant.Cache1HKey;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dao.SysConfigDao;
import top.shiningon.dynamic.forms.dto.admin.SysConfigFormDTO;
import top.shiningon.dynamic.forms.entity.SysConfig;
import top.shiningon.dynamic.forms.service.SysConfigService;

import java.util.List;

/**
 * 配置信息写入内存进行缓存，提供外部接口进行手动刷新
 */
@Component("top.shiningon.dynamic.forms.service.impl.SysConfigServiceImpl")
public class SysConfigServiceImpl implements SysConfigService {

    @Autowired
    SysConfigDao sysConfigDao;
    @Autowired
    Cache<String, Object> caffeineCache1H;

    public SysConfig getSysConfig(){
        Object obj = caffeineCache1H.getIfPresent(Cache1HKey.SYS_CONFIG);
        if(null == obj) {
            SysConfig sysConfig;
            List<SysConfig> configList = sysConfigDao.selectList(new LambdaQueryWrapper<>());
            if(CollUtil.isNotEmpty(configList)) {
                caffeineCache1H.put(Cache1HKey.SYS_CONFIG, configList.get(0));
                sysConfig = configList.get(0);
            }else{
                sysConfig = new SysConfig();
                sysConfig.setLoginAble(false);
                caffeineCache1H.put(Cache1HKey.SYS_CONFIG, sysConfig);
            }
            return sysConfig;
        }
        return (SysConfig)obj;
    }

    public ReturnParam updateLoginInfo(SysConfigFormDTO formDTO){
        boolean isSuccess = false;
        SysConfig sysConfig = this.getSysConfig();
        if(null == sysConfig){
            sysConfig = new SysConfig();
        }
        sysConfig.setLoginAble(formDTO.getLoginAble());
        sysConfig.setLoginName(formDTO.getLoginName());
        sysConfig.setLoginPassword(formDTO.getLoginPassword());
        if(null == sysConfig.getId()) {
            isSuccess = this.sysConfigDao.insert(sysConfig) == 1;
        }else {
            isSuccess = this.sysConfigDao.updateById(sysConfig) == 1;
        }
        if(isSuccess) {
            caffeineCache1H.invalidate(Cache1HKey.SYS_CONFIG);
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR);
    }

}
