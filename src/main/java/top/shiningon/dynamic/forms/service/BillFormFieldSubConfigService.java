package top.shiningon.dynamic.forms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dto.config.BillFormFieldSubConfigFormDTO;
import top.shiningon.dynamic.forms.dto.request.BillFormFieldSubConfigSearchListDTO;
import top.shiningon.dynamic.forms.entity.BillFormFieldSubConfig;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface BillFormFieldSubConfigService extends IService<BillFormFieldSubConfig> {

    List<BillFormFieldSubConfig> searchBillFormFieldSubConfigList(Long fieldConfigId);

    SearchResoultDTO<BillFormFieldSubConfig> searchBillFormFieldSubConfigList(List<Long> fieldConfigIdList);

    /**
     * 查询流程步骤列表
     */
    SearchResoultDTO<BillFormFieldSubConfig> searchBillFormFieldSubConfigList(BillFormFieldSubConfigSearchListDTO formDTO);

    /**
     * 查询单个流程步骤
     */
    BillFormFieldSubConfig searchBillFormFieldSubConfigSingle(Long id);

    ReturnParam saveBillFormFieldSubConfig(@NotNull BillFormFieldSubConfigFormDTO formDTO);

    /**
     * 删除流程步骤
     */
    boolean delBillFormFieldSubConfig(Long id, String operId);

}
