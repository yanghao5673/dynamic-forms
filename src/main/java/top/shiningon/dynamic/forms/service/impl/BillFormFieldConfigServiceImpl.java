package top.shiningon.dynamic.forms.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.benmanes.caffeine.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.constant.Cache1HKey;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dao.BillFormFieldConfigDao;
import top.shiningon.dynamic.forms.dto.config.BillFormFieldConfigFormDTO;
import top.shiningon.dynamic.forms.dto.request.BillFormFieldConfigSearchListDTO;
import top.shiningon.dynamic.forms.entity.BillFormFieldConfig;
import top.shiningon.dynamic.forms.service.BillFormFieldConfigService;
import top.shiningon.dynamic.forms.service.BillFormFieldSubConfigService;

import javax.validation.constraints.NotNull;
import java.util.Date;

@Component("top.shiningon.dynamic.forms.service.impl.BillFormFieldConfigServiceImpl")
public class BillFormFieldConfigServiceImpl extends ServiceImpl<BillFormFieldConfigDao, BillFormFieldConfig> implements BillFormFieldConfigService {

    @Autowired
    BillFormFieldConfigDao billFormFieldConfigDao;
    @Autowired
    BillFormFieldSubConfigService billFormFieldSubConfigService;
    @Autowired
    Cache<String, Object> caffeineCache1H;

    public SearchResoultDTO<BillFormFieldConfig> searchBillFormFieldConfigList(Long billConfigId){
        Object obj = caffeineCache1H.getIfPresent(Cache1HKey.BILL_FORM_FIELD_CONFIG_BY_BILL_CONFIG_ID + billConfigId);
        if(null == obj) {
            BillFormFieldConfigSearchListDTO formDTO = new BillFormFieldConfigSearchListDTO();
            formDTO.setBillConfigId(billConfigId);
            SearchResoultDTO<BillFormFieldConfig> searchDTO = this.searchBillFormFieldConfigList(formDTO);
            if(null != searchDTO) {
                caffeineCache1H.put(Cache1HKey.BILL_FORM_FIELD_CONFIG_BY_BILL_CONFIG_ID + billConfigId, searchDTO);
            }else{
                caffeineCache1H.put(Cache1HKey.BILL_FORM_FIELD_CONFIG_BY_BILL_CONFIG_ID + billConfigId, new SearchResoultDTO<BillFormFieldConfig>());
            }
            return searchDTO;
        }
        return (SearchResoultDTO<BillFormFieldConfig>)obj;
    }

    /**
     * 查询流程步骤列表
     */
    public SearchResoultDTO<BillFormFieldConfig> searchBillFormFieldConfigList(BillFormFieldConfigSearchListDTO formDTO){
        SearchResoultDTO<BillFormFieldConfig> resDTO = new SearchResoultDTO<>();
        if(null == formDTO.getBillConfigId() && StrUtil.isBlank(formDTO.getInputName()) ){
            return resDTO;
        }
        LambdaQueryWrapper<BillFormFieldConfig> wrapper = new LambdaQueryWrapper<>();
        //查询条件组装
        if(null != formDTO.getBillConfigId()){
            wrapper.like(BillFormFieldConfig::getBillConfigId, formDTO.getBillConfigId());
        }
        if(StrUtil.isNotBlank(formDTO.getInputName())){
            wrapper.like(BillFormFieldConfig::getInputName, formDTO.getInputName());
        }
        if(null != formDTO.getDelflag()){
            wrapper.eq(BillFormFieldConfig::getDelflag, formDTO.getDelflag());
        }else{
            wrapper.eq(BillFormFieldConfig::getDelflag, false);
        }
        if(formDTO.isPageSearch()) {
            IPage<BillFormFieldConfig> page = billFormFieldConfigDao.selectPage(new Page<>(formDTO.getPageNum(), formDTO.getPageSize()), wrapper);
            resDTO.putResoult(page);
        }else{
            //不分页，进行以下
            resDTO.putResoult(billFormFieldConfigDao.selectList(wrapper));
        }
        return resDTO;
    }

    /**
     * 查询单个流程步骤
     */
    public BillFormFieldConfig searchBillFormFieldConfigSingle(Long id){
        Object obj = caffeineCache1H.getIfPresent(Cache1HKey.BILL_FORM_FIELD_CONFIG_BY_ID + id);
        if(null == obj) {
            LambdaQueryWrapper<BillFormFieldConfig> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(BillFormFieldConfig::getId, id);
            wrapper.eq(BillFormFieldConfig::getDelflag, false);
            BillFormFieldConfig config = this.billFormFieldConfigDao.selectOne(wrapper);
            if(null != config) {
                caffeineCache1H.put(Cache1HKey.BILL_FORM_FIELD_CONFIG_BY_ID + id, config);
            }else{
                caffeineCache1H.put(Cache1HKey.BILL_FORM_FIELD_CONFIG_BY_ID + id, new BillFormFieldConfig());
            }
            return config;
        }
        return (BillFormFieldConfig)obj;
    }

    public ReturnParam saveBillFormFieldConfig(@NotNull BillFormFieldConfigFormDTO formDTO){
        //必填校验
        if(null == formDTO.getBillConfigId()){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("所属单据配置未填写");
        }
        if(StrUtil.isBlank(formDTO.getInputName())){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("字段名称未填写");
        }
        if(null == formDTO.getInputType()){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("字段类型未填写");
        }
//        if(null == formDTO.getVaildType()){
//            return ReturnParam.initReturnInfo(ErrorCodeEnum.GLOBAL_ERROR).setMessage("字段校验类型未填写");
//        }
        if(null == formDTO.getIndex()){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("字段顺序未填写");
        }
        if(null == formDTO.getIsRequired()){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("字段是否必填未填写");
        }
        //检查步骤顺序是否重复
        SearchResoultDTO<BillFormFieldConfig> fieldConfigSearchList = this.searchBillFormFieldConfigList(formDTO.getBillConfigId());
        if(CollUtil.isNotEmpty(fieldConfigSearchList.getList())){
            for(BillFormFieldConfig fieldConfig : fieldConfigSearchList.getList()){
                if(fieldConfig.getId() != formDTO.getId() && fieldConfig.getIndex() == formDTO.getIndex()){
                    return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("步骤顺序重复不可用");
                }
            }
        }

        //创建对象
        BillFormFieldConfig fieldConfig = new BillFormFieldConfig();
        if(null != formDTO.getId()) {
            fieldConfig.setId(formDTO.getId());
            fieldConfig.setUpdateBy(formDTO.getOperBy());
            fieldConfig.setUpdateTime(formDTO.getOperTime());
            if(null != formDTO.getDelflag()) {
                fieldConfig.setDelflag(formDTO.getDelflag());
            }
        }else{
            fieldConfig.setBillConfigId(formDTO.getBillConfigId());
            fieldConfig.setCreateBy(formDTO.getOperBy());
            fieldConfig.setCreateTime(formDTO.getOperTime());
            fieldConfig.setDelflag(false);
        }
        fieldConfig.setInputName(formDTO.getInputName());
        fieldConfig.setInputType(formDTO.getInputType());
        fieldConfig.setVaildType(formDTO.getVaildType());
        fieldConfig.setIndex(formDTO.getIndex());
        fieldConfig.setVaildMax(formDTO.getVaildMax());
        fieldConfig.setVaildMin(formDTO.getVaildMin());
        fieldConfig.setIsRequired(formDTO.getIsRequired());
        fieldConfig.setPlaceholder(formDTO.getPlaceholder());
        fieldConfig.setDefVal(formDTO.getDefVal());
        if( this.saveOrUpdate(fieldConfig) ) {
            caffeineCache1H.invalidate(Cache1HKey.BILL_FORM_FIELD_CONFIG_BY_ID + fieldConfig.getId());
            caffeineCache1H.invalidate(Cache1HKey.BILL_FORM_FIELD_CONFIG_BY_BILL_CONFIG_ID + fieldConfig.getBillConfigId());
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }else{
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("保存失败");
        }
    }

    /**
     * 删除流程步骤
     */
    public boolean delBillFormFieldConfig(Long id, String operId){
        BillFormFieldConfig bill = this.billFormFieldConfigDao.selectById(id);
        bill.setId(id);
        bill.setDelflag(true);
        bill.setUpdateBy(operId);
        bill.setUpdateTime(new Date());
        int num = this.billFormFieldConfigDao.updateById(bill);
        caffeineCache1H.invalidate(Cache1HKey.BILL_FORM_FIELD_CONFIG_BY_ID + id);
        caffeineCache1H.invalidate(Cache1HKey.BILL_FORM_FIELD_CONFIG_BY_BILL_CONFIG_ID + bill.getBillConfigId());
        return num == 1;
    }

}
