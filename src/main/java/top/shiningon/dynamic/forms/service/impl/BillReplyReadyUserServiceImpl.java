package top.shiningon.dynamic.forms.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dao.BillReplyReadyUserDao;
import top.shiningon.dynamic.forms.dto.request.BillReplyReadyUserFormDTO;
import top.shiningon.dynamic.forms.dto.request.BillReplyReadyUserSearchListDTO;
import top.shiningon.dynamic.forms.entity.BillReplyReadyUser;
import top.shiningon.dynamic.forms.service.BillReplyReadyUserService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

@Component("top.shiningon.dynamic.forms.service.impl.BillReplyReadyUserServiceImpl")
public class BillReplyReadyUserServiceImpl extends ServiceImpl<BillReplyReadyUserDao, BillReplyReadyUser> implements BillReplyReadyUserService {

    @Autowired
    BillReplyReadyUserDao billReplyReadyUserDao;
//    @Autowired
//    OutsideConfigService outsideConfigService;

    public List<BillReplyReadyUser> searchByReplyReadyId(Long replyReadyId){
        LambdaQueryWrapper<BillReplyReadyUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BillReplyReadyUser::getReplyReadyId, replyReadyId);
        wrapper.eq(BillReplyReadyUser::getDelflag, false);
        return this.billReplyReadyUserDao.selectList(wrapper);
    }

    /**
     * 查询流程步骤列表
     */
    public SearchResoultDTO<BillReplyReadyUser> searchBillReplyReadyUserList(BillReplyReadyUserSearchListDTO formDTO){
        SearchResoultDTO<BillReplyReadyUser> resDTO = new SearchResoultDTO<>();
        LambdaQueryWrapper<BillReplyReadyUser> wrapper = new LambdaQueryWrapper<>();
        //查询条件组装
        if(null != formDTO.getReplyReadyId()){
            wrapper.like(BillReplyReadyUser::getReplyReadyId, formDTO.getReplyReadyId());
        }
        if( !CollectionUtils.isEmpty(formDTO.getReplyReadyIdList()) ){
            if(formDTO.getReplyReadyIdList().size() > 1){
                wrapper.in(BillReplyReadyUser::getReplyReadyId, formDTO.getReplyReadyIdList());
            }else{
                wrapper.eq(BillReplyReadyUser::getReplyReadyId, formDTO.getReplyReadyIdList().get(0));
            }
        }
        if(null != formDTO.getUserid()){
            wrapper.like(BillReplyReadyUser::getUserId, formDTO.getUserid());
        }
        if(null != formDTO.getDelflag()){
            wrapper.eq(BillReplyReadyUser::getDelflag, formDTO.getDelflag());
        }else{
            wrapper.eq(BillReplyReadyUser::getDelflag, false);
        }
        if(formDTO.isPageSearch()) {
            IPage<BillReplyReadyUser> page = billReplyReadyUserDao.selectPage(new Page<>(formDTO.getPageNum(), formDTO.getPageSize()), wrapper);
            resDTO.putResoult(page);
        }else{
            //不分页，进行以下
            resDTO.putResoult(billReplyReadyUserDao.selectList(wrapper));
        }
        return resDTO;
    }

    /**
     * 查询单个流程步骤
     */
    public BillReplyReadyUser searchBillReplyReadyUserSingle(Long id){
        LambdaQueryWrapper<BillReplyReadyUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BillReplyReadyUser::getId, id);
        wrapper.eq(BillReplyReadyUser::getDelflag, false);
        return this.billReplyReadyUserDao.selectOne(wrapper);
    }

    /**
     * 创建流程步骤
     */
    public boolean createBillReplyReadyUser(BillReplyReadyUserFormDTO formDTO){
        BillReplyReadyUser reply = new BillReplyReadyUser();
        reply.setReplyReadyId(formDTO.getReplyReadyId());
        reply.setUserId(formDTO.getUserId());
        reply.setCreateBy(formDTO.getOperBy());
        reply.setCreateTime(formDTO.getOperTime());
        reply.setDelflag(formDTO.getDelflag());
        int num = this.billReplyReadyUserDao.insert(reply);
        return num == 1;
    }

    /**
     * 外部依赖，根据职位名称，查询到用户id。如果指定了用户id，就不需要外部依赖。
     */
    public boolean createBillReplyReadyUser(Long readyId, String currentUserId, List<String> userIdList){
        try {
            if(CollUtil.isNotEmpty(userIdList)){
                List<BillReplyReadyUser> readyUserList = new ArrayList<>(userIdList.size());
                for(String userId : userIdList){
                    BillReplyReadyUser reply = new BillReplyReadyUser();
                    reply.setReplyReadyId(readyId);
                    reply.setUserId(userId);
                    reply.setCreateBy(currentUserId);
                    reply.setCreateTime(new Date());
                    reply.setDelflag(false);
                    readyUserList.add(reply);
                }
                return this.saveOrUpdateBatch(readyUserList);
            }
            return true;
        }catch (Exception e){
            e.printStackTrace();
            return false;
        }
    }

    /**
     * 修改流程步骤
     */
    public boolean updateBillReplyReadyUser(BillReplyReadyUserFormDTO formDTO){
        BillReplyReadyUser reply = new BillReplyReadyUser();
        reply.setId(formDTO.getId());
        reply.setReplyReadyId(formDTO.getReplyReadyId());
        reply.setUserId(formDTO.getUserId());
        reply.setUpdateBy(formDTO.getOperBy());
        reply.setUpdateTime(formDTO.getOperTime());
        reply.setDelflag(formDTO.getDelflag());
        int num = this.billReplyReadyUserDao.updateById(reply);
        return num == 1;
    }

    /**
     * 删除流程步骤
     */
    public boolean delBillReplyReadyUser(Long id, String operId){
        BillReplyReadyUser bill = new BillReplyReadyUser();
        bill.setId(id);
        bill.setDelflag(true);
        bill.setUpdateBy(operId);
        bill.setUpdateTime(new Date());
        int num = this.billReplyReadyUserDao.updateById(bill);
        return num == 1;
    }

    public boolean delBillReplyReadyUserByReadyIdList(List<Long> readyIdList){
        LambdaQueryWrapper<BillReplyReadyUser> wrapper = new LambdaQueryWrapper<>();
        wrapper.in(BillReplyReadyUser::getReplyReadyId, readyIdList);
        this.billReplyReadyUserDao.delete(wrapper);
        return true;
    }

}
