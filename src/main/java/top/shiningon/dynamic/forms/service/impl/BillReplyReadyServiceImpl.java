package top.shiningon.dynamic.forms.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.yulichang.wrapper.MPJLambdaWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dao.BillReplyReadyDao;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;
import top.shiningon.dynamic.forms.dto.request.BillReplyReadySearchListDTO;
import top.shiningon.dynamic.forms.entity.BillReplyReady;
import top.shiningon.dynamic.forms.entity.BillReplyReadyUser;
import top.shiningon.dynamic.forms.service.BillReplyReadyService;
import top.shiningon.dynamic.forms.service.BillReplyReadyUserService;

import java.util.Date;
import java.util.List;

@Component("top.shiningon.dynamic.forms.service.impl.BillReplyReadyServiceImpl")
public class BillReplyReadyServiceImpl extends ServiceImpl<BillReplyReadyDao, BillReplyReady> implements BillReplyReadyService {

    @Autowired
    BillReplyReadyDao billReplyReadyDao;
    @Autowired
    BillReplyReadyUserService billReplyReadyUserService;

    public boolean searchUserIsToReplyByBill(Long billId, String oprId){
        MPJLambdaWrapper<BillReplyReady> joinWrapper = new MPJLambdaWrapper<>();
        joinWrapper.leftJoin(BillReplyReadyUser.class, on -> on
                        .eq(BillReplyReadyUser::getReplyReadyId, BillReplyReady::getId))
                .eq(BillReplyReadyUser::getUserId, oprId)
                .eq(BillReplyReady::getBillListId, billId)
                .eq(BillReplyReady::getIsNow, true);
        return this.billReplyReadyDao.selectJoinCount(joinWrapper) > 0;
    }

    public SearchResoultDTO<BillReplyReady> searchUserReplyReadyList(String oprId, PageFormDTO formDTO, Boolean isNow){
        SearchResoultDTO<BillReplyReady> resDTO = new SearchResoultDTO<>();
        MPJLambdaWrapper<BillReplyReady> joinWrapper = new MPJLambdaWrapper<>();
        joinWrapper.selectAll(BillReplyReady.class)
                .leftJoin(BillReplyReadyUser.class, on -> on
                    .eq(BillReplyReadyUser::getReplyReadyId, BillReplyReady::getId))
                .eq(BillReplyReadyUser::getUserId, oprId);
        if(null != isNow){
            joinWrapper.eq(BillReplyReady::getIsNow, isNow);
        }
        joinWrapper.orderByAsc(BillReplyReady::getCreateBy);
        if(formDTO.isPageSearch()) {
            IPage<BillReplyReady> page = this.billReplyReadyDao.selectJoinPage(new Page<>(formDTO.getPageNum(), formDTO.getPageSize()), BillReplyReady.class, joinWrapper);
            resDTO.putResoult(page);
        }else{
            //不分页，进行以下
            resDTO.putResoult(this.billReplyReadyDao.selectJoinList(BillReplyReady.class, joinWrapper));
        }
        return resDTO;
    }

    /**
     * 查询单据流程审核信息
     */
    public SearchResoultDTO<BillReplyReady> searchBillReplyReadyList(BillReplyReadySearchListDTO formDTO){
        SearchResoultDTO<BillReplyReady> resDTO = new SearchResoultDTO<>();
        LambdaQueryWrapper<BillReplyReady> wrapper = new LambdaQueryWrapper<>();
        //查询条件组装
        if(null != formDTO.getBillListId()){
            wrapper.like(BillReplyReady::getBillListId, formDTO.getBillListId());
        }
        if(null != formDTO.getFlowStepId()){
            wrapper.like(BillReplyReady::getFlowStepId, formDTO.getFlowStepId());
        }
        if(null != formDTO.getIsNow()){
            wrapper.like(BillReplyReady::getIsNow, formDTO.getIsNow());
        }
        if(null != formDTO.getDelflag()){
            wrapper.eq(BillReplyReady::getDelflag, formDTO.getDelflag());
        }else{
            wrapper.eq(BillReplyReady::getDelflag, false);
        }
        //默认排序
        wrapper.orderByAsc(BillReplyReady::getIndex);
        List<BillReplyReady> list;
        if(formDTO.isPageSearch()) {
            IPage<BillReplyReady> page = billReplyReadyDao.selectPage(new Page<>(formDTO.getPageNum(), formDTO.getPageSize()), wrapper);
            list = page.getRecords();
            resDTO.setPage(true);
            resDTO.setList(list);
            resDTO.setPageNum(formDTO.getPageNum());
            resDTO.setPageSize(formDTO.getPageSize());
            resDTO.setRowCount(page.getTotal());
        }else{
            //不分页，进行以下
            list = billReplyReadyDao.selectList(wrapper);
            resDTO.setPage(false);
            resDTO.setList(list);
        }
        return resDTO;
    }

    public BillReplyReady getNowReady(Long billId){
        LambdaQueryWrapper<BillReplyReady> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BillReplyReady::getBillListId, billId);
        wrapper.eq(BillReplyReady::getIsNow, true);
        wrapper.eq(BillReplyReady::getDelflag, false);
        return this.billReplyReadyDao.selectOne(wrapper);
    }

    public BillReplyReady getNextReady(BillReplyReady nowReady){
        LambdaQueryWrapper<BillReplyReady> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BillReplyReady::getBillListId, nowReady.getBillListId());
        wrapper.eq(BillReplyReady::getDelflag, false);
        wrapper.gt(BillReplyReady::getIndex, nowReady.getIndex());
        wrapper.orderByAsc(BillReplyReady::getIndex);
        List<BillReplyReady> readyList = this.billReplyReadyDao.selectList(wrapper);
        if(CollUtil.isNotEmpty(readyList)){
            return readyList.get(0);
        }
        return null;
    }

    public List<BillReplyReady> getAfterReadyList(Long billId, Integer startIndex){
        LambdaQueryWrapper<BillReplyReady> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BillReplyReady::getBillListId, billId);
        wrapper.eq(BillReplyReady::getDelflag, false);
        wrapper.ge(BillReplyReady::getIndex, startIndex);
        wrapper.orderByAsc(BillReplyReady::getIndex);
        return this.billReplyReadyDao.selectList(wrapper);
    }

    /**
     * 创建流程步骤
     */
    public boolean createBillReplyReady(BillReplyReady reply){
        if(null == reply){
            return false;
        }
        if(null == reply.getCreateBy()){
            reply.setCreateBy("-1");
        }
        if(null == reply.getCreateTime()){
            reply.setCreateTime(new Date());
        }
        if(null == reply.getDelflag()){
            reply.setDelflag(false);
        }
        int num = this.billReplyReadyDao.insert(reply);
        return num == 1;
    }

    public boolean removeNowFlag(Long billListId){
        LambdaQueryWrapper<BillReplyReady> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BillReplyReady::getBillListId, billListId);
        wrapper.eq(BillReplyReady::getIsNow, true);
        List<BillReplyReady> readyList = this.billReplyReadyDao.selectList(wrapper);
        if(CollUtil.isNotEmpty(readyList)){
            for(BillReplyReady ready : readyList){
                ready.setIsNow(false);
                ready.setUpdateBy("-1");
                ready.setUpdateTime(new Date());
            }
            this.saveOrUpdateBatch(readyList);
        }
        return true;
    }

}
