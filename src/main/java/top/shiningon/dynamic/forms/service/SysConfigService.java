package top.shiningon.dynamic.forms.service;

import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.dto.admin.OutsideMemberSyncFormDTO;
import top.shiningon.dynamic.forms.dto.admin.SysConfigFormDTO;
import top.shiningon.dynamic.forms.dto.rank.ReplyRankSearchDTO;
import top.shiningon.dynamic.forms.dto.rank.ReplyRankSearchResponseDTO;
import top.shiningon.dynamic.forms.entity.OutsideConfig;
import top.shiningon.dynamic.forms.entity.SysConfig;

import java.util.List;

public interface SysConfigService {

    SysConfig getSysConfig();

    ReturnParam updateLoginInfo(SysConfigFormDTO formDTO);

}
