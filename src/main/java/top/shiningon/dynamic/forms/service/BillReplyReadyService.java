package top.shiningon.dynamic.forms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;
import top.shiningon.dynamic.forms.dto.request.BillReplyReadySearchListDTO;
import top.shiningon.dynamic.forms.entity.BillReplyReady;

import java.util.List;

public interface BillReplyReadyService extends IService<BillReplyReady> {

    boolean searchUserIsToReplyByBill(Long billId, String oprId);

    public SearchResoultDTO<BillReplyReady> searchUserReplyReadyList(String oprId, PageFormDTO formDTO, Boolean isNow);

    /**
     * 查询流程步骤列表
     */
    SearchResoultDTO<BillReplyReady> searchBillReplyReadyList(BillReplyReadySearchListDTO formDTO);

    BillReplyReady getNowReady(Long billId);

    BillReplyReady getNextReady(BillReplyReady nowReady);

    List<BillReplyReady> getAfterReadyList(Long billId, Integer startIndex);

    boolean createBillReplyReady(BillReplyReady reply);

    boolean removeNowFlag(Long billListId);

}
