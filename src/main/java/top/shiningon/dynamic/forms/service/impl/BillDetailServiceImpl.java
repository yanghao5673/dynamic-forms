package top.shiningon.dynamic.forms.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.benmanes.caffeine.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.constant.Cache1HKey;
import top.shiningon.dynamic.forms.dao.BillDetailDao;
import top.shiningon.dynamic.forms.dto.request.BillDetailFormDTO;
import top.shiningon.dynamic.forms.dto.request.BillDetailSearchListDTO;
import top.shiningon.dynamic.forms.entity.BillDetail;
import top.shiningon.dynamic.forms.service.BillDetailService;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component("top.shiningon.dynamic.forms.service.impl.BillDetailServiceImpl")
public class BillDetailServiceImpl extends ServiceImpl<BillDetailDao, BillDetail> implements BillDetailService{

    @Autowired
    BillDetailDao billDetailDao;
    @Autowired
    Cache<String, Object> caffeineCache1H;

    public List<BillDetail> searchBillDetailListByBillId(Long billId){
        LambdaQueryWrapper<BillDetail> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BillDetail::getBillListId, billId);
        wrapper.orderByAsc(BillDetail::getIndex);
        return billDetailDao.selectList(wrapper);
    }

    /**
     * 查询单据字段列表
     */
    public SearchResoultDTO<BillDetail> searchBillDetailList(BillDetailSearchListDTO formDTO){
        SearchResoultDTO<BillDetail> resDTO = new SearchResoultDTO<>();
        LambdaQueryWrapper<BillDetail> wrapper = new LambdaQueryWrapper<>();
        //查询条件组装
        if( !CollectionUtils.isEmpty(formDTO.getBillListIdList()) ){
            if(formDTO.getBillListIdList().size() > 1){
                wrapper.in(BillDetail::getBillListId, formDTO.getBillListIdList());
            }else{
                wrapper.eq(BillDetail::getBillListId, formDTO.getBillListIdList().get(0));
            }
        }
        if(null != formDTO.getDelflag()){
            wrapper.eq(BillDetail::getDelflag, formDTO.getDelflag());
        }else{
            wrapper.eq(BillDetail::getDelflag, false);
        }
        //默认排序
        wrapper.orderByAsc(BillDetail::getIndex);
        List<BillDetail> list;
        //不分页，进行以下
        list = billDetailDao.selectList(wrapper);
        resDTO.setPage(false);
        resDTO.setList(list);
        return resDTO;
    }

    /**
     * 查询单个单据字段
     */
    public BillDetail searchBillDetailSingle(Long id){
        Object obj = caffeineCache1H.getIfPresent(Cache1HKey.BILL_DETAIL_BY_ID + id);
        if(null != obj) {
            LambdaQueryWrapper<BillDetail> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(BillDetail::getId, id);
            wrapper.eq(BillDetail::getDelflag, false);
            BillDetail detail = this.billDetailDao.selectOne(wrapper);
            if(null != detail) {
                caffeineCache1H.put(Cache1HKey.BILL_DETAIL_BY_ID + id, detail);
            }else{
                caffeineCache1H.put(Cache1HKey.BILL_DETAIL_BY_ID + id, new BillDetail());
            }
            return detail;
        }
        return (BillDetail)obj;
    }

    /**
     * 创建单据字段
     */
    public boolean createBillDetail(BillDetailFormDTO formDTO){
        BillDetail bill = new BillDetail();
        bill.setBillListId(formDTO.getBillListId());
        bill.setBillFormFieldId(formDTO.getBillFormFieldId());
        bill.setInputName(formDTO.getInputName());
        bill.setInputType(formDTO.getInputType());
        bill.setInputVal(formDTO.getInputVal());
        bill.setCreateBy(formDTO.getOperBy());
        bill.setCreateTime(formDTO.getOperTime());
        bill.setDelflag(formDTO.getDelflag());
        int num = this.billDetailDao.insert(bill);
        return num == 1;
    }

    /**
     * 修改单据字段
     */
    public boolean updateBillDetail(BillDetailFormDTO formDTO){
        BillDetail bill = new BillDetail();
        bill.setId(formDTO.getId());
        bill.setBillListId(formDTO.getBillListId());
        bill.setBillFormFieldId(formDTO.getBillFormFieldId());
        bill.setInputName(formDTO.getInputName());
        bill.setInputType(formDTO.getInputType());
        bill.setInputVal(formDTO.getInputVal());
        bill.setUpdateBy(formDTO.getOperBy());
        bill.setUpdateTime(formDTO.getOperTime());
        bill.setDelflag(formDTO.getDelflag());
        int num = this.billDetailDao.updateById(bill);
        caffeineCache1H.invalidate(Cache1HKey.BILL_DETAIL_BY_ID + bill.getId());
        return num == 1;
    }

    /**
     * 删除单据字段
     */
    public boolean delBillDetail(Long id, String operId){
        BillDetail bill = new BillDetail();
        bill.setId(id);
        bill.setDelflag(true);
        bill.setUpdateBy(operId);
        bill.setUpdateTime(new Date());
        caffeineCache1H.invalidate(Cache1HKey.BILL_DETAIL_BY_ID + bill.getId());
        int num = this.billDetailDao.updateById(bill);
        return num == 1;
    }

    public boolean batchSaveOrUpdate(List<BillDetail> detailList, String operId){
        if(CollUtil.isEmpty(detailList)){
            return false;
        }
        List<String> cacheKey = new ArrayList<>();
        for(BillDetail detail : detailList){
            if( null == detail.getId() ){
                detail.setCreateBy(operId);
                detail.setCreateTime(new Date());
                detail.setDelflag(false);
            }else{
                detail.setUpdateBy(operId);
                detail.setUpdateTime(new Date());
                cacheKey.add(Cache1HKey.BILL_DETAIL_BY_ID + detail.getId());
            }
        }
        if(CollUtil.isNotEmpty(cacheKey)) {
            caffeineCache1H.invalidateAll(cacheKey);
        }
        return this.saveOrUpdateBatch(detailList);
    }

}
