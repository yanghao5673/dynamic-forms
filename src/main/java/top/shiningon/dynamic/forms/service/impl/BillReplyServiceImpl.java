package top.shiningon.dynamic.forms.service.impl;

import cn.hutool.core.collection.CollUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dao.BillReplyDao;
import top.shiningon.dynamic.forms.dto.process.PageFormDTO;
import top.shiningon.dynamic.forms.dto.process.bill.request.BillReplySearchListDTO;
import top.shiningon.dynamic.forms.dto.request.BillReplyFormDTO;
import top.shiningon.dynamic.forms.entity.BillReply;
import top.shiningon.dynamic.forms.event.publisher.RemoveReadyMsgEventPublisher;
import top.shiningon.dynamic.forms.event.publisher.ReplyNotifyMsgEventPublisher;
import top.shiningon.dynamic.forms.service.BillReplyService;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Component("top.shiningon.dynamic.forms.service.impl.BillReplyServiceImpl")
public class BillReplyServiceImpl extends ServiceImpl<BillReplyDao, BillReply> implements BillReplyService {

    @Autowired
    BillReplyDao billReplyDao;
    @Autowired
    ReplyNotifyMsgEventPublisher replyNotifyMsgEventPublisher;

    public List<Long> searchUserReplyBillListId(String oprId, PageFormDTO formDTO){
        LambdaQueryWrapper<BillReply> wrapper = new LambdaQueryWrapper<BillReply>();
        wrapper.select(BillReply::getBillListId);
        wrapper.eq(BillReply::getCreateBy, oprId);
        wrapper.eq(BillReply::getDelflag, false);
        wrapper.groupBy(BillReply::getBillListId);
        if(null != formDTO.getPageNum() && null != formDTO.getPageSize()) {
            IPage<BillReply> page = this.billReplyDao.selectPage(new Page<>(formDTO.getPageNum(), formDTO.getPageSize()), wrapper);
            if (page.getRecords().size() > 0) {
                return page.getRecords().stream().map(BillReply::getBillListId).collect(Collectors.toList());
            }
        }else{
            List<BillReply> replyList = this.billReplyDao.selectList(wrapper);
            if(CollUtil.isNotEmpty(replyList)){
                return replyList.stream().map(BillReply::getBillListId).collect(Collectors.toList());
            }
        }
        return null;
    }

    /**
     * 查询流程步骤列表
     */
    public SearchResoultDTO<BillReply> searchBillReplyList(BillReplySearchListDTO formDTO){
        SearchResoultDTO<BillReply> resDTO = new SearchResoultDTO<>();
        LambdaQueryWrapper<BillReply> wrapper = this.createBillReplySearchListDTOWrapper(formDTO);
        if(formDTO.isPageSearch()) {
            IPage<BillReply> page = billReplyDao.selectPage(new Page<>(formDTO.getPageNum(), formDTO.getPageSize()), wrapper);
            resDTO.putResoult(page);
        }else{
            //不分页，进行以下
            resDTO.putResoult(billReplyDao.selectList(wrapper));
        }
        return resDTO;
    }

    /**
     * 查询单个流程步骤
     */
    public BillReply searchBillReplySingle(Long id){
        LambdaQueryWrapper<BillReply> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BillReply::getId, id);
        wrapper.eq(BillReply::getDelflag, false);
        return this.billReplyDao.selectOne(wrapper);
    }

    /**
     * 创建流程步骤
     */
    public boolean createBillReply(BillReplyFormDTO formDTO){
        BillReply reply = new BillReply();
        reply.setBillListId(formDTO.getBillListId());
        reply.setFlowStepId(formDTO.getFlowStepId());
        reply.setReplyStatus(formDTO.getReplyStatus());
        reply.setIndex(formDTO.getIndex());
        reply.setRemark(formDTO.getRemark());
        reply.setCreateBy(formDTO.getOperBy());
        reply.setCreateTime(formDTO.getOperTime());
        reply.setDelflag(formDTO.getDelflag());
        int num = this.billReplyDao.insert(reply);
        replyNotifyMsgEventPublisher.sendReplyNotify(formDTO.getBillListId(), reply.getId());
        return num == 1;
    }

    /**
     * 修改流程步骤
     */
    public boolean updateBillReply(BillReplyFormDTO formDTO){
        BillReply reply = new BillReply();
        reply.setId(formDTO.getId());
        reply.setBillListId(formDTO.getBillListId());
        reply.setFlowStepId(formDTO.getFlowStepId());
        reply.setReplyStatus(formDTO.getReplyStatus());
        reply.setIndex(formDTO.getIndex());
        reply.setRemark(formDTO.getRemark());
        reply.setUpdateBy(formDTO.getOperBy());
        reply.setUpdateTime(formDTO.getOperTime());
        reply.setDelflag(formDTO.getDelflag());
        int num = this.billReplyDao.updateById(reply);
        return num == 1;
    }

    /**
     * 删除流程步骤
     */
    public boolean delBillReply(Long id, String operId){
        BillReply bill = new BillReply();
        bill.setId(id);
        bill.setDelflag(true);
        bill.setUpdateBy(operId);
        bill.setUpdateTime(new Date());
        int num = this.billReplyDao.updateById(bill);
        return num == 1;
    }

    public BillReply getLastReplyByBill(Long billId){
        LambdaQueryWrapper<BillReply> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BillReply::getBillListId, billId);
        wrapper.eq(BillReply::getDelflag, false);
        wrapper.orderByDesc(BillReply::getIndex);
        List<BillReply> replyList = this.billReplyDao.selectList(wrapper);
        if(CollUtil.isNotEmpty(replyList)){
            return replyList.get(0);
        }
        return null;
    }

    private LambdaQueryWrapper<BillReply> createBillReplySearchListDTOWrapper(BillReplySearchListDTO formDTO){
        LambdaQueryWrapper<BillReply> wrapper = new LambdaQueryWrapper<>();
        //查询条件组装
        if(null != formDTO.getBillListId()){
            wrapper.like(BillReply::getBillListId, formDTO.getBillListId());
        }
        if(null != formDTO.getFlowStepId()){
            wrapper.like(BillReply::getFlowStepId, formDTO.getFlowStepId());
        }
        if(null != formDTO.getReplyStatus()){
            wrapper.like(BillReply::getReplyStatus, formDTO.getReplyStatus());
        }
        if(null != formDTO.getCreateBy()){
            wrapper.eq(BillReply::getCreateBy, formDTO.getCreateBy());
        }
        if(null != formDTO.getDelflag()){
            wrapper.eq(BillReply::getDelflag, formDTO.getDelflag());
        }else{
            wrapper.eq(BillReply::getDelflag, false);
        }
        return wrapper;
    }

}
