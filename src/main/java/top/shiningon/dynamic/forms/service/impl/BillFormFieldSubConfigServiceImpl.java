package top.shiningon.dynamic.forms.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.benmanes.caffeine.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.constant.Cache1HKey;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dao.BillFormFieldSubConfigDao;
import top.shiningon.dynamic.forms.dto.config.BillFormFieldSubConfigFormDTO;
import top.shiningon.dynamic.forms.dto.request.BillFormFieldSubConfigSearchListDTO;
import top.shiningon.dynamic.forms.entity.BillFormFieldSubConfig;
import top.shiningon.dynamic.forms.service.BillFormFieldSubConfigService;

import javax.validation.constraints.NotNull;
import java.util.*;
import java.util.stream.Collectors;

@Component("top.shiningon.dynamic.forms.service.impl.BillFormFieldSubConfigServiceImpl")
public class BillFormFieldSubConfigServiceImpl extends ServiceImpl<BillFormFieldSubConfigDao, BillFormFieldSubConfig> implements BillFormFieldSubConfigService {

    @Autowired
    BillFormFieldSubConfigDao billFormFieldSubConfigDao;
    @Autowired
    Cache<String, Object> caffeineCache1H;

    public List<BillFormFieldSubConfig> searchBillFormFieldSubConfigList(Long fieldConfigId){
        Object obj = caffeineCache1H.getIfPresent(Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_LIST_BY_FIELD_CONFIG_ID + fieldConfigId);
        if(null == obj) {
            LambdaQueryWrapper<BillFormFieldSubConfig> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(BillFormFieldSubConfig::getFormFieldId, fieldConfigId);
            wrapper.eq(BillFormFieldSubConfig::getDelflag, false);
            List<BillFormFieldSubConfig> list = billFormFieldSubConfigDao.selectList(wrapper);
            if(null != list) {
                caffeineCache1H.put(Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_LIST_BY_FIELD_CONFIG_ID + fieldConfigId, list);
            }else{
                caffeineCache1H.put(Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_LIST_BY_FIELD_CONFIG_ID + fieldConfigId, new ArrayList<BillFormFieldSubConfig>(0));
            }
            return list;
        }
        return (List<BillFormFieldSubConfig>)obj;
    }

    public SearchResoultDTO<BillFormFieldSubConfig> searchBillFormFieldSubConfigList(List<Long> fieldConfigIdList){
        List<BillFormFieldSubConfig> resList = new ArrayList<>(fieldConfigIdList.size());
        //先尝试取缓存，然后读数据库
        List<String> cacheKeyList = fieldConfigIdList.stream().map(obj -> Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_LIST_BY_FIELD_CONFIG_ID + obj.toString()).collect(Collectors.toList());
        Map<String, Object> cacheObjList = caffeineCache1H.getAllPresent(cacheKeyList);
        if(CollUtil.isNotEmpty(cacheObjList)){
            for(Map.Entry<String, Object> entry : cacheObjList.entrySet()) {
                resList.addAll((List<BillFormFieldSubConfig>)entry.getValue());
                try {
                    fieldConfigIdList.remove(Long.parseLong(entry.getKey().replace(Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_LIST_BY_FIELD_CONFIG_ID, "")));
                }catch (Exception e){
                    e.printStackTrace();
                }
            }
        }
        if(CollUtil.isNotEmpty(fieldConfigIdList)) {
            BillFormFieldSubConfigSearchListDTO formDTO = new BillFormFieldSubConfigSearchListDTO();
            formDTO.setFormFieldIdList(fieldConfigIdList);
            SearchResoultDTO<BillFormFieldSubConfig> subConfigSearchResoultDTO = this.searchBillFormFieldSubConfigList(formDTO);
            if(CollUtil.isNotEmpty(subConfigSearchResoultDTO.getList())) {
                Map<Long, List<BillFormFieldSubConfig>> subMap = new HashMap<>();
                for (BillFormFieldSubConfig subConfig : subConfigSearchResoultDTO.getList()) {
                    if(!subMap.containsKey(subConfig.getFormFieldId())){
                        subMap.put(subConfig.getFormFieldId(), new ArrayList<>());
                    }
                    subMap.get(subConfig.getFormFieldId()).add(subConfig);
                }
                if(CollUtil.isNotEmpty(subMap)){
                    //写入缓存
                    for(Map.Entry<Long, List<BillFormFieldSubConfig>> entry : subMap.entrySet()) {
                        if(null != entry.getValue()) {
                            caffeineCache1H.put(Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_LIST_BY_FIELD_CONFIG_ID + entry.getKey(), entry.getValue());
                        }else{
                            caffeineCache1H.put(Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_LIST_BY_FIELD_CONFIG_ID + entry.getKey(), new ArrayList<BillFormFieldSubConfig>(0));
                        }
                    }
                }
                resList.addAll(subConfigSearchResoultDTO.getList());
            }
        }
        SearchResoultDTO<BillFormFieldSubConfig> res = new SearchResoultDTO<>();
        res.putResoult(resList);
        return res;
    }

    /**
     * 查询流程步骤列表
     */
    public SearchResoultDTO<BillFormFieldSubConfig> searchBillFormFieldSubConfigList(BillFormFieldSubConfigSearchListDTO formDTO){
        SearchResoultDTO<BillFormFieldSubConfig> resDTO = new SearchResoultDTO<>();
        if(CollUtil.isEmpty(formDTO.getFormFieldIdList())){
            return resDTO;
        }
        LambdaQueryWrapper<BillFormFieldSubConfig> wrapper = new LambdaQueryWrapper<>();
        //查询条件组装
        if( !CollectionUtils.isEmpty(formDTO.getFormFieldIdList()) ){
            if(formDTO.getFormFieldIdList().size() > 1){
                wrapper.in(BillFormFieldSubConfig::getFormFieldId, formDTO.getFormFieldIdList());
            }else{
                wrapper.eq(BillFormFieldSubConfig::getFormFieldId, formDTO.getFormFieldIdList().get(0));
            }
        }
        if(null != formDTO.getDelflag()){
            wrapper.eq(BillFormFieldSubConfig::getDelflag, formDTO.getDelflag());
        }else{
            wrapper.eq(BillFormFieldSubConfig::getDelflag, false);
        }
        if(formDTO.isPageSearch()) {
            IPage<BillFormFieldSubConfig> page = billFormFieldSubConfigDao.selectPage(new Page<>(formDTO.getPageNum(), formDTO.getPageSize()), wrapper);
            resDTO.putResoult(page);
        }else{
            //不分页，进行以下
            resDTO.putResoult(billFormFieldSubConfigDao.selectList(wrapper));
        }
        return resDTO;
    }

    /**
     * 查询单个流程步骤
     */
    public BillFormFieldSubConfig searchBillFormFieldSubConfigSingle(Long id){
        Object obj = caffeineCache1H.getIfPresent(Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_BY_ID + id);
        if(null == obj){
            LambdaQueryWrapper<BillFormFieldSubConfig> wrapper = new LambdaQueryWrapper<>();
            wrapper.eq(BillFormFieldSubConfig::getId, id);
            wrapper.eq(BillFormFieldSubConfig::getDelflag, false);
            BillFormFieldSubConfig subConfig = this.billFormFieldSubConfigDao.selectOne(wrapper);
            if(null != subConfig) {
                caffeineCache1H.put(Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_BY_ID + id, subConfig);
            }else{
                caffeineCache1H.put(Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_BY_ID + id, new BillFormFieldSubConfig());
            }
            return subConfig;
        }
        return (BillFormFieldSubConfig)obj;
    }

    public ReturnParam saveBillFormFieldSubConfig(@NotNull BillFormFieldSubConfigFormDTO formDTO){
        //必填校验
        if(StrUtil.isBlank(formDTO.getInputVal())){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("可选值未填写");
        }
        if(null == formDTO.getIndex()){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("顺序未填写");
        }
        if(null == formDTO.getDefSelect()){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("默认选中未填写");
        }
        //检查步骤顺序是否重复
        List<BillFormFieldSubConfig> fieldSubConfigList = this.searchBillFormFieldSubConfigList(formDTO.getFormFieldId());
        if(CollUtil.isNotEmpty(fieldSubConfigList)){
            for(BillFormFieldSubConfig fieldSubConfig : fieldSubConfigList){
                if(fieldSubConfig.getId() != formDTO.getId() && fieldSubConfig.getIndex() == formDTO.getIndex()){
                    return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("步骤顺序重复不可用");
                }
            }
        }

        //创建对象
        BillFormFieldSubConfig subConfig = new BillFormFieldSubConfig();
        if(null != formDTO.getId()) {
            subConfig.setId(formDTO.getId());
            subConfig.setUpdateBy(formDTO.getOperBy());
            subConfig.setUpdateTime(formDTO.getOperTime());
            if(null != formDTO.getDelflag()) {
                subConfig.setDelflag(formDTO.getDelflag());
            }
        }else{
            subConfig.setFormFieldId(formDTO.getFormFieldId());
            subConfig.setCreateBy(formDTO.getOperBy());
            subConfig.setCreateTime(formDTO.getOperTime());
            subConfig.setDelflag(false);
        }
        subConfig.setInputVal(formDTO.getInputVal());
        subConfig.setIndex(formDTO.getIndex());
        subConfig.setDefSelect(formDTO.getDefSelect());
        if( this.saveOrUpdate(subConfig) ) {
            caffeineCache1H.invalidate(Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_BY_ID + subConfig.getId());
            caffeineCache1H.invalidate(Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_LIST_BY_FIELD_CONFIG_ID + subConfig.getFormFieldId());
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }else{
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("保存失败");
        }
    }

    /**
     * 删除流程步骤
     */
    public boolean delBillFormFieldSubConfig(Long id, String operId){
        BillFormFieldSubConfig bill = this.billFormFieldSubConfigDao.selectById(id);
        bill.setDelflag(true);
        bill.setUpdateBy(operId);
        bill.setUpdateTime(new Date());
        caffeineCache1H.invalidate(Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_BY_ID + bill.getId());
        caffeineCache1H.invalidate(Cache1HKey.BILL_FORM_FIELD_SUB_CONFIG_LIST_BY_FIELD_CONFIG_ID + bill.getFormFieldId());
        int num = this.billFormFieldSubConfigDao.updateById(bill);
        return num == 1;
    }

}
