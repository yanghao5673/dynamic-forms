package top.shiningon.dynamic.forms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dto.config.BillConfigFormDTO;
import top.shiningon.dynamic.forms.dto.process.base.request.BillConfigSearchListDTO;
import top.shiningon.dynamic.forms.entity.BillConfig;

import javax.validation.constraints.NotNull;

public interface BillConfigService extends IService<BillConfig> {

    /**
     * 查询单据配置列表
     */
    SearchResoultDTO<BillConfig> searchBillConfigList(BillConfigSearchListDTO formDTO);

    /**
     * 查询单个单据配置
     */
    BillConfig searchBillConfigSingle(Long id);

    ReturnParam saveBillConfigForm(@NotNull BillConfigFormDTO formDTO);

    /**
     * 删除单据配置
     */
    boolean delBillConfig(Long id, String operId);

}
