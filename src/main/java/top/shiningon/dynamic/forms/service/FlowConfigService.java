package top.shiningon.dynamic.forms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dto.config.FlowConfigFormDTO;
import top.shiningon.dynamic.forms.dto.process.base.request.FlowConfigSearchListDTO;
import top.shiningon.dynamic.forms.entity.FlowConfig;

import javax.validation.constraints.NotNull;

public interface FlowConfigService extends IService<FlowConfig> {

    /**
     * 查询流程配置列表
     */
    SearchResoultDTO<FlowConfig> searchFlowConfigList(FlowConfigSearchListDTO formDTO);

    /**
     * 查询单个流程配置
     */
    FlowConfig searchFlowConfigSingle(Long id);

    /**
     * 创建、修改流程配置
     */
    ReturnParam saveFlowConfigForm(@NotNull FlowConfigFormDTO formDTO);

    /**
     * 删除流程配置
     */
    boolean delFlowConfig(Long id, String operId);

}
