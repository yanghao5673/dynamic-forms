package top.shiningon.dynamic.forms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dto.config.BillFormFieldConfigFormDTO;
import top.shiningon.dynamic.forms.dto.request.BillFormFieldConfigSearchListDTO;
import top.shiningon.dynamic.forms.entity.BillFormFieldConfig;

import javax.validation.constraints.NotNull;

public interface BillFormFieldConfigService extends IService<BillFormFieldConfig> {

    /**
     * 根据配置表id查询流程步骤列表
     */
    SearchResoultDTO<BillFormFieldConfig> searchBillFormFieldConfigList(Long billConfigId);

    /**
     * 查询流程步骤列表
     */
    SearchResoultDTO<BillFormFieldConfig> searchBillFormFieldConfigList(BillFormFieldConfigSearchListDTO formDTO);

    /**
     * 查询单个流程步骤
     */
    BillFormFieldConfig searchBillFormFieldConfigSingle(Long id);

    ReturnParam saveBillFormFieldConfig(@NotNull BillFormFieldConfigFormDTO formDTO);

    /**
     * 删除流程步骤
     */
    boolean delBillFormFieldConfig(Long id, String operId);

}
