package top.shiningon.dynamic.forms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dto.config.FlowStepFormDTO;
import top.shiningon.dynamic.forms.dto.request.FlowStepSearchListDTO;
import top.shiningon.dynamic.forms.entity.FlowStep;

import javax.validation.constraints.NotNull;
import java.util.List;

public interface FlowStepService extends IService<FlowStep> {

    List<FlowStep> searchFlowStepList(Long flowConfigId);

    /**
     * 查询流程步骤列表
     */
    SearchResoultDTO<FlowStep> searchFlowStepList(FlowStepSearchListDTO formDTO);

    /**
     * 查询单个流程步骤
     */
    FlowStep searchFlowStepSingle(Long id);

    FlowStep searchNextFlowStepSingle(Long flowConfigId, Integer nowStepIndex);

    ReturnParam saveFlowStepForm(@NotNull FlowStepFormDTO formDTO);

    /**
     * 删除流程步骤
     */
    boolean delFlowStep(Long id, String operId);

}
