package top.shiningon.dynamic.forms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dto.process.bill.request.StartBillFormDTO;
import top.shiningon.dynamic.forms.dto.process.bill.request.SubmitBillFormDTO;
import top.shiningon.dynamic.forms.dto.process.bill.request.SubmitReplyFormDTO;
import top.shiningon.dynamic.forms.dto.request.BillListSearchListDTO;
import top.shiningon.dynamic.forms.entity.BillList;

public interface BillListService extends IService<BillList> {

    /**
     * 查询单据字段列表
     */
    SearchResoultDTO<BillList> searchBillListList(BillListSearchListDTO formDTO);

    /**
     * 查询单个单据字段
     */
    BillList searchBillListSingle(Long id);

    /**
     * 创建单据的创建
     */
    ReturnParam createBill(SubmitBillFormDTO formDTO);

    ReturnParam startBill(StartBillFormDTO formDTO);

    ReturnParam replyBill(SubmitReplyFormDTO formDTO);

}
