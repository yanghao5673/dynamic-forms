package top.shiningon.dynamic.forms.service.impl;

import lombok.extern.log4j.Log4j2;
import org.apache.http.HttpStatus;
import org.apache.http.client.config.RequestConfig;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.util.EntityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.shiningon.dynamic.forms.service.HttpClientService;

import java.io.IOException;

@Log4j2
@Component("top.shiningon.dynamic.forms.service.impl.HttpClientServiceImpl")
public class HttpClientServiceImpl implements HttpClientService {

    @Autowired
    private CloseableHttpClient httpClient;
    @Autowired
    private RequestConfig requestConfig;

    public String doGet(String url, String charset) {
        CloseableHttpResponse response = null;
        try {
            //发送get请求
            HttpGet request = new HttpGet(url);
            request.setConfig(requestConfig);
            response = httpClient.execute(request);

            /**请求发送成功，并得到响应**/
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                /**读取服务器返回过来的json字符串数据**/
                String strResult = EntityUtils.toString(response.getEntity());
                strResult = new String(strResult.getBytes(charset), "utf-8");
                return strResult;
            }
        } catch (IOException e) {
            log.error("http调用出错", e);
        } finally {
            try {
                if (response != null){
                    response.close();
                }
            }catch (IOException e){
                log.error("关闭链接出错", e);
            }
        }
        return null;
    }

    public String doPost(String url, String jsonStr) {
        CloseableHttpResponse response = null;
        try {
            //发送get请求
            HttpPost request = new HttpPost(url);
            request.setConfig(requestConfig);
            StringEntity stringEntity = new StringEntity(jsonStr,"UTF-8");
            request.addHeader("Content-Type", "application/json; charset=UTF-8");
            request.setEntity(stringEntity);
            response = httpClient.execute(request);

            /**请求发送成功，并得到响应**/
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                /**读取服务器返回过来的json字符串数据**/
                return EntityUtils.toString(response.getEntity());
            }
        } catch (IOException e) {
            log.error("http调用出错", e);
        } finally {
            try {
                if (response != null){
                    response.close();
                }
            }catch (IOException e){
                log.error("关闭链接出错", e);
            }
        }
        return null;
    }

}
