package top.shiningon.dynamic.forms.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.github.benmanes.caffeine.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.constant.Cache1HKey;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.dao.MemberDao;
import top.shiningon.dynamic.forms.dto.sync.request.MemberSyncDTO;
import top.shiningon.dynamic.forms.entity.Member;
import top.shiningon.dynamic.forms.service.MemberService;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * 操作人员信息映射表
 */
@Component("top.shiningon.dynamic.forms.service.impl.MemberServiceImpl")
public class MemberServiceImpl extends ServiceImpl<MemberDao, Member> implements MemberService {

    @Autowired
    MemberDao memberDao;
    @Autowired
    Cache<String, Object> caffeineCache1H;

    public Member getById(String id){
        if(StrUtil.isBlank(id)){
            return null;
        }
        Object obj = caffeineCache1H.getIfPresent(Cache1HKey.MEMBER_BY_ID + id);
        if(null == obj) {
            Member member = this.memberDao.selectById(id);
            if(null != member) {
                caffeineCache1H.put(Cache1HKey.MEMBER_BY_ID + id, member);
            }else{
                caffeineCache1H.put(Cache1HKey.MEMBER_BY_ID + id, new Member());
            }
            return member;
        }
        return (Member)obj;
    }

    public ReturnParam saveMember(MemberSyncDTO dto){
        boolean insertMethod = false;
        Member member = this.getById(dto.getId());
        if(null == member){
            member = new Member();
            insertMethod = true;
        }
        member.setId(dto.getId());
        member.setName(dto.getName());
        member.setPosition(dto.getPosition());
        member.setDepartment(dto.getDepartment());
        if(insertMethod) {
            this.memberDao.insert(member);
        }else{
            this.memberDao.updateById(member);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
    }

    public ReturnParam saveBatchMember(List<MemberSyncDTO> dtoList){
        if(CollUtil.isEmpty(dtoList)){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }
        List<Member> inserList = new ArrayList<>();
        List<Member> updateList = new ArrayList<>();
        List<Member> dbList = this.memberDao.selectBatchIds(dtoList.stream().map(MemberSyncDTO::getId).collect(Collectors.toList()));
        boolean hasDB = CollUtil.isNotEmpty(dbList);
        Member member;
        for(MemberSyncDTO dto : dtoList){
            if(StrUtil.isBlank(dto.getName())){
                continue;
            }
            if(hasDB){
                Optional<Member> opt = dbList.stream().filter(obj -> obj.getId().equals(dto.getId())).findAny();
                if(opt.isPresent()){
                    member = opt.get();
                    member.setName(dto.getName());
                    member.setPosition(dto.getPosition());
                    member.setDepartment(dto.getDepartment());
                    updateList.add(member);
                    continue;
                }else{
                    member = new Member();
                }
            }else{
                member = new Member();
            }
            member.setId(dto.getId());
            member.setName(dto.getName());
            member.setPosition(dto.getPosition());
            member.setDepartment(dto.getDepartment());
            inserList.add(member);
        }
        if(CollUtil.isNotEmpty(inserList)) {
            this.saveBatch(inserList, 100);
        }
        if(CollUtil.isNotEmpty(updateList)) {
            this.updateBatchById(updateList, 100);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
    }

    public ReturnParam delMember(String id){
        this.memberDao.deleteById(id);
        return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
    }

    public ReturnParam delBatchMember(List<String> idList){
        if(CollUtil.isNotEmpty(idList)) {
            this.memberDao.deleteBatchIds(idList);
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
    }

}
