package top.shiningon.dynamic.forms.service;

public interface HttpClientService {

    public String doGet(String url, String charset);

    public String doPost(String url, String jsonStr);

}
