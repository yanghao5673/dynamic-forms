package top.shiningon.dynamic.forms.service;

import com.baomidou.mybatisplus.extension.service.IService;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.dto.request.BillReplyReadyUserFormDTO;
import top.shiningon.dynamic.forms.dto.request.BillReplyReadyUserSearchListDTO;
import top.shiningon.dynamic.forms.entity.BillReplyReadyUser;

import java.util.List;

public interface BillReplyReadyUserService extends IService<BillReplyReadyUser> {

    public List<BillReplyReadyUser> searchByReplyReadyId(Long replyReadyId);

    /**
     * 查询流程步骤列表
     */
    SearchResoultDTO<BillReplyReadyUser> searchBillReplyReadyUserList(BillReplyReadyUserSearchListDTO formDTO);

    /**
     * 查询单个流程步骤
     */
    BillReplyReadyUser searchBillReplyReadyUserSingle(Long id);

    /**
     * 创建流程步骤
     */
    boolean createBillReplyReadyUser(BillReplyReadyUserFormDTO formDTO);

    boolean createBillReplyReadyUser(Long readyId, String currentUserId, List<String> userIdList);

    /**
     * 修改流程步骤
     */
    boolean updateBillReplyReadyUser(BillReplyReadyUserFormDTO formDTO);

    /**
     * 删除流程步骤
     */
    boolean delBillReplyReadyUser(Long id, String operId);

    boolean delBillReplyReadyUserByReadyIdList(List<Long> readyIdList);

}
