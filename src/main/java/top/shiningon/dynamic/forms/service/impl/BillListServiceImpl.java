package top.shiningon.dynamic.forms.service.impl;

import cn.hutool.core.collection.CollUtil;
import cn.hutool.core.date.DateUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;
import top.shiningon.dynamic.forms.base.MyBizErrorException;
import top.shiningon.dynamic.forms.base.ReturnParam;
import top.shiningon.dynamic.forms.base.SearchResoultDTO;
import top.shiningon.dynamic.forms.constant.BillReplyStatusEnum;
import top.shiningon.dynamic.forms.constant.BillStatusEnum;
import top.shiningon.dynamic.forms.constant.ErrorCodeEnum;
import top.shiningon.dynamic.forms.constant.FlowStepReplyTypeEnum;
import top.shiningon.dynamic.forms.dao.BillListDao;
import top.shiningon.dynamic.forms.dto.process.bill.request.*;
import top.shiningon.dynamic.forms.dto.rank.ReplyRankSearchDTO;
import top.shiningon.dynamic.forms.dto.rank.ReplyRankSearchResponseDTO;
import top.shiningon.dynamic.forms.dto.request.*;
import top.shiningon.dynamic.forms.entity.*;
import top.shiningon.dynamic.forms.event.publisher.RemoveReadyMsgEventPublisher;
import top.shiningon.dynamic.forms.service.*;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

@Component("top.shiningon.dynamic.forms.service.impl.BillListServiceImpl")
public class BillListServiceImpl extends ServiceImpl<BillListDao, BillList> implements BillListService {

    @Autowired
    BillListDao billListDao;
    @Autowired
    RemoveReadyMsgEventPublisher removeReadyMsgEventPublisher;
    @Autowired
    BillDetailService billDetailService;
    @Autowired
    BillFormFieldConfigService billFormFieldConfigService;
    @Autowired
    FlowConfigService flowConfigService;
    @Autowired
    FlowStepService flowStepService;
    @Autowired
    BillConfigService billConfigService;
    @Autowired
    BillReplyService billReplyService;
    @Autowired
    BillReplyReadyService billReplyReadyService;
    @Autowired
    BillReplyReadyUserService billReplyReadyUserService;
    @Autowired
    OutsideConfigService outsideConfigService;

    /**
     * 查询单据字段列表
     */
    public SearchResoultDTO<BillList> searchBillListList(BillListSearchListDTO formDTO){
        SearchResoultDTO<BillList> resDTO = new SearchResoultDTO<>();
        LambdaQueryWrapper<BillList> wrapper = new LambdaQueryWrapper<>();
        //查询条件组装
        if( CollUtil.isNotEmpty(formDTO.getBillConfigIdList()) ){
            if(formDTO.getBillConfigIdList().size() > 1){
                wrapper.in(BillList::getBillConfigId, formDTO.getBillConfigIdList());
            }else{
                wrapper.eq(BillList::getBillConfigId, formDTO.getBillConfigIdList().get(0));
            }
        }
        wrapper.like(StrUtil.isNotBlank(formDTO.getTitle()), BillList::getTitle, formDTO.getTitle());
        wrapper.eq(null != formDTO.getStatus(), BillList::getStatus, formDTO.getStatus());
        wrapper.eq(StrUtil.isNotBlank(formDTO.getCreateBy()), BillList::getCreateBy, formDTO.getCreateBy());
        wrapper.eq(StrUtil.isNotBlank(formDTO.getUpdateBy()), BillList::getUpdateBy, formDTO.getUpdateBy());
        wrapper.eq(BillList::getDelflag, null != formDTO.getDelflag() ? formDTO.getDelflag() : false);
        if(null != formDTO.getBeginCreateTime() && null != formDTO.getEndCreateTime()){
            if(DateUtil.compare(formDTO.getBeginCreateTime(), formDTO.getEndCreateTime()) <= 0){
                wrapper.between(BillList::getCreateTime, formDTO.getBeginCreateTime(), DateUtil.offsetDay(formDTO.getEndCreateTime(), 1));
            }else{
                return resDTO;
            }
        }else{
            wrapper.gt(null != formDTO.getBeginCreateTime(), BillList::getCreateTime, formDTO.getBeginCreateTime());
            wrapper.lt(null != formDTO.getEndCreateTime(), BillList::getCreateTime, formDTO.getEndCreateTime());
        }
        if(null != formDTO.getBeginUpdateTime() && null != formDTO.getEndUpdateTime()){
            if(DateUtil.compare(formDTO.getBeginUpdateTime(), formDTO.getEndUpdateTime()) <= 0){
                wrapper.between(BillList::getUpdateTime, formDTO.getBeginUpdateTime(), DateUtil.offsetDay(formDTO.getEndUpdateTime(), 1));
            }else{
                return resDTO;
            }
        }else{
            if(null != formDTO.getBeginUpdateTime()){
                wrapper.gt(BillList::getUpdateTime, formDTO.getBeginUpdateTime());
            }
            if(null != formDTO.getEndUpdateTime()){
                wrapper.lt(BillList::getUpdateTime, formDTO.getEndUpdateTime());
            }
        }
        if(formDTO.isPageSearch()) {
            IPage<BillList> page = billListDao.selectPage(new Page<>(formDTO.getPageNum(), formDTO.getPageSize()), wrapper);
            resDTO.putResoult(page);
        }else{
            //不分页，进行以下
            resDTO.putResoult(billListDao.selectList(wrapper));
        }
        return resDTO;
    }

    /**
     * 查询单个单据字段
     */
    public BillList searchBillListSingle(Long id){
        LambdaQueryWrapper<BillList> wrapper = new LambdaQueryWrapper<>();
        wrapper.eq(BillList::getId, id);
        wrapper.eq(BillList::getDelflag, false);
        return this.billListDao.selectOne(wrapper);
    }

    /**
     * 创建单据的创建
     */
    @Transactional
    public ReturnParam<Long> createBill(SubmitBillFormDTO formDTO) throws MyBizErrorException{
        if(null == formDTO || CollUtil.isEmpty(formDTO.getInputList())){
            throw new MyBizErrorException(new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("表单字段未提交"));
        }
        BillList bill = new BillList();
        bill.setBillConfigId(formDTO.getBillConfigId());
        BillConfig billConfig = this.billConfigService.getById(formDTO.getBillConfigId());
        bill.setTitle(billConfig.getTitle());
        bill.setSubTitle(billConfig.getSubTitle());
        FlowStep nextStep = this.flowStepService.searchNextFlowStepSingle(billConfig.getFlowConfigId(), null);
        if(null != nextStep) {
            bill.setNowFlowStepId(nextStep.getId());
        }else{
            throw new MyBizErrorException(new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("表单审核流程未配置"));
        }
        bill.setStatus(BillStatusEnum.未提交);
        bill.setCreateBy(formDTO.getOperBy());
        bill.setCreateTime(new Date());
        bill.setDelflag(false);
        if(this.billListDao.insert(bill) == 1){
            List<BillDetail> detailList = new ArrayList<>(formDTO.getInputList().size());
            //加载对应的单据字段列表
            SearchResoultDTO<BillFormFieldConfig> fieldConfigSearchList = billFormFieldConfigService.searchBillFormFieldConfigList(formDTO.getBillConfigId());
            for(BillDetailDTO dto : formDTO.getInputList()){
                BillDetail detail = new BillDetail();
                detail.setBillListId(bill.getId());
                //获取对应的字段对象
                Optional<BillFormFieldConfig> fieldConfigOpt = fieldConfigSearchList.getList().stream().filter(obj -> obj.getId().equals(dto.getFieldId())).findAny();
                if(!fieldConfigOpt.isPresent()){
                    throw new MyBizErrorException(new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("表单字段参数异常"));
                }
                BillFormFieldConfig fieldConfig = fieldConfigOpt.get();
                detail.setBillFormFieldId(fieldConfig.getId());
                detail.setIndex(fieldConfig.getIndex());
                detail.setInputName(fieldConfig.getInputName());
                detail.setInputType(fieldConfig.getInputType());
                detail.setInputVal(dto.getInputVal());
                detailList.add(detail);
            }
            if( this.billDetailService.batchSaveOrUpdate(detailList, formDTO.getOperBy()) ){
                return new ReturnParam<Long>(ErrorCodeEnum.GLOBAL_SUCCESS).setContent(bill.getId());
            }
            throw new MyBizErrorException(new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("表单字段保存失败"));
        }else{
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("表单创建失败");
        }
    }

    public ReturnParam startBill(StartBillFormDTO formDTO) throws MyBizErrorException{
        boolean isReStart = false;
        BillList bill = this.searchBillListSingle(formDTO.getBillId());
        //判断是不是驳回状态
        if(BillStatusEnum.审批驳回.equals(bill.getStatus())){
            isReStart = true;
        }
        if(!BillStatusEnum.未提交.equals(bill.getStatus()) && !BillStatusEnum.审批驳回.equals(bill.getStatus())){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("单据不可重复提交");
        }
        if(!bill.getCreateBy().equals(formDTO.getOperBy())){
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("单据仅支持本人发起提交");
        }
        BillConfig billConfig = this.billConfigService.searchBillConfigSingle(bill.getBillConfigId());
        FlowStep nextStep = this.flowStepService.searchNextFlowStepSingle(billConfig.getFlowConfigId(), null);
        if(null != nextStep) {
            bill.setNowFlowStepId(nextStep.getId());
        }else{
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("表单审核流程未配置");
        }
        bill.setStatus(BillStatusEnum.已提交);
        bill.setUpdateBy(formDTO.getOperBy());
        bill.setUpdateTime(new Date());
        int row = this.billListDao.updateById(bill);
        if(row != 1){
            throw new MyBizErrorException(new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("单据发起失败"));
        }
        //单据发起后，初始化审核流程信息
        //如果是驳回后发起,要先清理原先的流程信息
        if(isReStart){
            BillReplyReadyFormDTO updateDTO = new BillReplyReadyFormDTO();
            updateDTO.setBillListId(bill.getId());
            billReplyReadyService.removeNowFlag(bill.getId());
        }
        //获取所有流程步骤，预设审批流程
        BillConfig config = this.billConfigService.searchBillConfigSingle(bill.getBillConfigId());
        List<FlowStep> stepList = this.flowStepService.searchFlowStepList(config.getFlowConfigId());
        if(CollUtil.isEmpty(stepList)){
            throw new MyBizErrorException(new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("审核流程步骤不存在"));
        }
        //记录是否是第一个步骤，默认为当前步骤
        boolean isFirst = true;
        //远程获取对应的数据信息
        List<ReplyRankSearchDTO> searchRankList = new ArrayList<>();
        for(FlowStep step : stepList ){
            ReplyRankSearchDTO rank = new ReplyRankSearchDTO();
            rank.setId(step.getId());
            rank.setUserId(formDTO.getOperBy());
            rank.setReplyUserType(step.getReplyUserType());
            rank.setReplyId(step.getUserTypeIds());
            searchRankList.add(rank);
        }
        List<ReplyRankSearchResponseDTO> rankResList = outsideConfigService.getBillRankUserIdList(searchRankList);
        if(CollUtil.isEmpty(rankResList)){
            throw new MyBizErrorException(new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("远程接口异常"));
        }
        for(FlowStep step : stepList ){
            BillReplyReady ready = new BillReplyReady();
            ready.initBy(bill.getId(), step);
            ready.setIsNow(isFirst);
            isFirst = false;
            if(!this.billReplyReadyService.createBillReplyReady(ready)){
                throw new MyBizErrorException(new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("审核流程步骤初始化异常"));
            }
            List<List<String>> rankUser2List = rankResList.stream().filter(obj -> obj.getId().equals(step.getId())).map(ReplyRankSearchResponseDTO::getReplyUserId).collect(Collectors.toList());
            if(CollUtil.isNotEmpty(rankUser2List)) {
                List<String> userIdList = new ArrayList<>();
                for(List<String> list : rankUser2List){
                    userIdList.addAll(list);
                }
                if (!this.billReplyReadyUserService.createBillReplyReadyUser(ready.getId(), formDTO.getOperBy(), userIdList)) {
                    throw new MyBizErrorException(new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("审核流程步骤相关人员初始化异常"));
                }
            }else{
                throw new MyBizErrorException(new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("远程获取审核流程步骤相关人员异常"));
            }
        }
        return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
    }

    public ReturnParam replyBill(SubmitReplyFormDTO formDTO){
        //找到单据
        BillList bill = this.searchBillListSingle(formDTO.getBillListId());
        //确认单据当前的流程步骤是否和当前提交的一致。
        if(bill.getNowFlowStepId().equals(formDTO.getFlowStepId())) {
            //获取最近一次提交，得到审核的顺序号
            int index = 1;
            BillReply preReply = this.billReplyService.getLastReplyByBill(bill.getId());
            if(null != preReply){
                index = preReply.getIndex() + 1;
            }
            //根据审核状态，进入不同方法
            switch (formDTO.getReplyStatus()){
                case 审批通过:
                    return this.replyPass(bill, formDTO, index);
                case 审批驳回:
                    return this.replyReject(bill, formDTO, index);
                default:
                    throw new MyBizErrorException(new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("审批状态异常"));
            }
        }else{
            return new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("当前审核流程不正确");
        }
    }

    private ReturnParam replyPass(BillList bill, SubmitReplyFormDTO formDTO, Integer index){
        if(this.createReplyRecord(bill, formDTO.getReplyStatus(), formDTO.getRemark(), formDTO.getOperBy(), index)) {
            //通过
            //检查是否可以通过当前流程步骤
            FlowStep nowStep = this.flowStepService.searchFlowStepSingle(formDTO.getFlowStepId());
            if( checkReplyPass(bill.getId(), nowStep) ){
                //获取下一个step
                FlowStep nextStep = this.flowStepService.searchNextFlowStepSingle(nowStep.getFlowConfigId(), nowStep.getIndex());
                this.updateBillAndReadyToNext(bill, nextStep, formDTO);
            }
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }
        throw new MyBizErrorException(new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("审核信息保存失败"));
    }

    private ReturnParam replyNotice(BillList bill, SubmitReplyFormDTO formDTO, Long nowReadyId, FlowStep flowStep){
        //创建知会记录
        List<BillReplyReadyUser> readyUserList = this.billReplyReadyUserService.searchByReplyReadyId(nowReadyId);
        for(BillReplyReadyUser readyUser : readyUserList) {
            this.createReplyRecord(bill, BillReplyStatusEnum.知会, "", readyUser.getUserId(),flowStep.getIndex());
        }
        //检查有没有下一步
        FlowStep nowStep = this.flowStepService.searchFlowStepSingle(flowStep.getId());
        //获取下一个step
        FlowStep nextStep = this.flowStepService.searchNextFlowStepSingle(nowStep.getFlowConfigId(), nowStep.getIndex());
        this.updateBillAndReadyToNext(bill, nextStep, formDTO);
        return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
    }

    private ReturnParam replyReject(BillList bill, SubmitReplyFormDTO formDTO, Integer index){
        if(this.createReplyRecord(bill, formDTO.getReplyStatus(), formDTO.getRemark(), formDTO.getOperBy(), index)) {
            //驳回
            //异步进行：移除后续的ready信息
            //清理后续的流程预设数据
            removeReadyMsgEventPublisher.removeReadyMsg(bill.getId(), bill.getNowFlowStepId());
            bill.setNowFlowStepId(0L);
            bill.setStatus(BillStatusEnum.审批驳回);
            bill.setUpdateTime(new Date());
            bill.setUpdateBy(formDTO.getOperBy());
            this.billListDao.updateById(bill);
            this.billReplyReadyService.removeNowFlag(bill.getId());
            return new ReturnParam(ErrorCodeEnum.GLOBAL_SUCCESS);
        }
        throw new MyBizErrorException(new ReturnParam(ErrorCodeEnum.GLOBAL_ERROR).setMessage("审核信息保存失败"));
    }

    private void updateBillAndReadyToNext(BillList bill, FlowStep nextStep, SubmitReplyFormDTO formDTO){
        if (null == nextStep) {
            //单据结束
            //异步进行：移除后续的ready信息
            removeReadyMsgEventPublisher.removeReadyMsg(bill.getId(), bill.getNowFlowStepId());
            bill.setNowFlowStepId(-1L);
            bill.setStatus(BillStatusEnum.完成);
            bill.setUpdateTime(new Date());
            bill.setUpdateBy(formDTO.getOperBy());
            this.billListDao.updateById(bill);
        } else {
            //单据有下一步，设置bill记录的当前step，和ready的当前标签
            bill.setNowFlowStepId(nextStep.getId());
            bill.setStatus(BillStatusEnum.审批通过);
            bill.setUpdateTime(new Date());
            bill.setUpdateBy(formDTO.getOperBy());
            this.billListDao.updateById(bill);
            //判断下一步是不是知会
            BillReplyReady nowReady = this.billReplyReadyService.getNowReady(bill.getId());
            if (null != nowReady) {
                nowReady.setIsNow(false);
                nowReady.setUpdateBy(formDTO.getOperBy());
                nowReady.setUpdateTime(new Date());
                this.billReplyReadyService.updateById(nowReady);
                BillReplyReady nextReady = this.billReplyReadyService.getNextReady(nowReady);
                if (null != nextReady) {
                    nextReady.setIsNow(true);
                    nextReady.setUpdateBy(formDTO.getOperBy());
                    nextReady.setUpdateTime(new Date());
                    this.billReplyReadyService.updateById(nextReady);
                }
                if (null != nextStep && FlowStepReplyTypeEnum.知会.equals(nextStep.getReplyType())) {
                    this.replyNotice(bill, formDTO, nextReady.getId(), nextStep);
                }
            }
        }
    }

    private boolean createReplyRecord(BillList bill, BillReplyStatusEnum replyStatus, String remark, String operId,Integer index) {
        //记录审核回复
        BillReplyFormDTO replyFormDTO = new BillReplyFormDTO();
        replyFormDTO.setBillListId(bill.getId());
        replyFormDTO.setFlowStepId(bill.getNowFlowStepId());
        replyFormDTO.setReplyStatus(replyStatus);
        replyFormDTO.setIndex(index);
        replyFormDTO.setRemark(remark);
        replyFormDTO.setOperBy(operId);
        replyFormDTO.setOperTime(new Date());
        replyFormDTO.setDelflag(false);
        return billReplyService.createBillReply(replyFormDTO);
    }

    private boolean checkReplyPass(Long billId, FlowStep nowStep){
        switch (nowStep.getReplyType()){
            case 知会:
                return true;
            case 一票通过:
                return true;
            case 会签通过:
                //查同步骤下的所有人，是否都通过了
                BillReplySearchListDTO formDTO = new BillReplySearchListDTO();
                formDTO.setBillListId(billId);
                formDTO.setFlowStepId(nowStep.getId());
                formDTO.setReplyStatus(BillReplyStatusEnum.审批通过);
                SearchResoultDTO<BillReply> replyListDTO = this.billReplyService.searchBillReplyList(formDTO);
                if(CollUtil.isEmpty(replyListDTO.getList())){
                    return false;
                }
                //检查步骤中，用户是否匹配全了
                BillReplyReadySearchListDTO readySearchDTO = new BillReplyReadySearchListDTO();
                readySearchDTO.setBillListId(billId);
                readySearchDTO.setFlowStepId(nowStep.getId());
                readySearchDTO.setIsNow(true);
                SearchResoultDTO<BillReplyReady> readyListDTO = this.billReplyReadyService.searchBillReplyReadyList(readySearchDTO);
                if(CollUtil.isNotEmpty(readyListDTO.getList()) && readyListDTO.getList().size() == 1){
                    BillReplyReady nowReady = readyListDTO.getList().get(0);
                    BillReplyReadyUserSearchListDTO searchDTO = new BillReplyReadyUserSearchListDTO();
                    searchDTO.setReplyReadyId(nowReady.getId());
                    SearchResoultDTO<BillReplyReadyUser> readyUserListDTO = this.billReplyReadyUserService.searchBillReplyReadyUserList(searchDTO);
                    if(CollUtil.isNotEmpty(readyUserListDTO.getList())){
                        return replyListDTO.getList().size() == readyUserListDTO.getList().size();
                    }
                    return false;
                }
        }
        return false;
    }

}
